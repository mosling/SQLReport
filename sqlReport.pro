TARGET = sqlReport
TEMPLATE = app

QT += core gui xml sql printsupport qml widgets

CONFIG += c++20
CONFIG += sdk_no_version_check
CONFIG += lrelease
CONFIG += embed_translations
CONFIG += app_bundle

DEFINES += QT_USE_QSTRINGBUILDER

INCLUDEPATH += $$PWD/src
INCLUDEPATH += $$PWD/src/db
INCLUDEPATH += src

TRANSLATIONS = \
    resources/i18n/sqlreport_de.ts \
    resources/i18n/sqlreport_en.ts

RC_ICONS = resources/sqlreport.ico

# Define variables for substitution
SQLREPORT_VERSION = $$system(git describe)
SQLREPORT_DATE = $$system(git log -1 --format=%cs)

#Input template files
TEMPLATE_FILE = config.h.in
GENERATED_FILE = config.h

# Create the substitution configuration
QMAKE_SUBSTITUTES += config.h.in
config.h.in.input = $${TEMPLATE_FILE}
config.h.in.output = $${GENERATED_FILE}

# Define custom variables for substitution‚
config.h.in.vars += SQLREPORT_VERSION
config.h.in.vars += SQLREPORT_DATE

# Ensure the generated file is cleaned
QMAKE_CLEAN += $${GENERATED_FILE}

# Version for generated file
VERSION = $$SQLREPORT_VERSION

win32 {
    CONFIG += console
    VERSION ~= s/([0-9.]+)-(\d+)-g[a-f0-9]{6,}/\1.\2/
}

macx {
    INFO_PLIST_PATH = $$shell_quote($${OUT_PWD}/$${TARGET}.app/Contents/Info.plist)
    QMAKE_POST_LINK += /usr/libexec/PlistBuddy -c \"Delete :CFBundleShortVersionString\" $${INFO_PLIST_PATH};
    QMAKE_POST_LINK += /usr/libexec/PlistBuddy -c \"Add :CFBundleShortVersionString string $${VERSION}\" $${INFO_PLIST_PATH}
}

OTHER_FILES += \
    README.adoc \
    .gitlab-ci.yml \
    .gitignore \
    config.h.in

RESOURCES = resources/resources.qrc

FORMS += \
    src/about_dialog.ui \
    src/db/db_connection_form.ui \
    src/db/dbconnectionstatus.ui \
    src/editdialog.ui \
    src/editwidget.ui \
    src/parameter/parameterdialog.ui \
    src/sql_report.ui

HEADERS += \
    $${GENERATED_FILE} \
    src/commonjavascript.h \
    src/about_dialog.h \
    src/block_text_edit.h \
    src/common.h \
    src/db/db_connection.h \
    src/db/db_connection_form.h \
    src/db/db_connection_set.h \
    src/db/dbconnectionstatus.h \
    src/db/dbencoding.h \
    src/db/odbcsources.h \
    src/editdialog.h \
    src/editwidget.h \
    src/logmessage.h \
    src/lookuplist.h \
    src/parameter/parametercategorydelegate.h \
    src/parameter/parameterdialog.h \
    src/parameter/parameterentry.h \
    src/parameter/parametermodel.h \
    src/qtree_reporter.h \
    src/query_executor.h \
    src/query_set.h \
    src/query_set_entry.h \
    src/query_set_history.h \
    src/recentcombobox.h \
    src/resultset.h \
    src/resultsetlist.h \
    src/resultsetsingle.h \
    src/resultsetsql.h \
    src/sql_report.h \
    src/sql_report_highlighter.h \
    src/sqlreportcli.h

SOURCES += \
    src/commonjavascript.cpp \
    src/about_dialog.cpp \
    src/block_text_edit.cpp \
    src/common.cpp \
    src/db/db_connection.cpp \
    src/db/db_connection_form.cpp \
    src/db/db_connection_set.cpp \
    src/db/dbconnectionstatus.cpp \
    src/db/dbencoding.cpp \
    src/db/odbcsources.cpp \
    src/editdialog.cpp \
    src/editwidget.cpp \
    src/logmessage.cpp \
    src/lookuplist.cpp \
    src/main.cpp \
    src/parameter/parametercategorydelegate.cpp \
    src/parameter/parameterdialog.cpp \
    src/parameter/parameterentry.cpp \
    src/parameter/parametermodel.cpp \
    src/qtree_reporter.cpp \
    src/query_executor.cpp \
    src/query_set.cpp \
    src/query_set_entry.cpp \
    src/recentcombobox.cpp \
    src/resultset.cpp \
    src/resultsetlist.cpp \
    src/resultsetsingle.cpp \
    src/resultsetsql.cpp \
    src/sql_report.cpp \
    src/sql_report_highlighter.cpp \
    src/sqlreportcli.cpp

DISTFILES += \
    ../sqlreport/deployment/config/config.xml \
    deployment/packages/com.mosling.sqlreport/meta/license.txt \
    deployment/packages/com.mosling.sqlreport/meta/package.xml
