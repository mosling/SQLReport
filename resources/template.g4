grammar template;

block
    : (block_line NL)*
    ;

block_line
    : '::#' comment
    | '::<' include
    | '::' TXT
    | template_row
    ;

comment
    : (WS TXT)*
    ;

template_row
    : (string | command)+
    ;

command
    : '${' variable '}'
    | '#{' block_call '}'
    ;

variable
    : '?' string
    | string
    ;

block_call
    : string
    ;

include
    : TXT
    ;

string: (WS* TXT+ WS*)+;

TXT : [0-9a-z.A-Z_]+ ;
WS : [ \t]+ ;
NL : [\r\n]+ -> skip;
