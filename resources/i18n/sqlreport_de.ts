<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/about_dialog.ui" line="20"/>
        <source>About</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="../../src/about_dialog.ui" line="105"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Generate Output from SQL Sources&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;See &lt;a href=&quot;https://mosling.gitlab.io/sqlreportdocs/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Documentation&lt;/span&gt;&lt;/a&gt;&lt;br/&gt;Visit the GitLab Repository &lt;a href=&quot;https://gitlab.com/mosling/SQLReport&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;SQLReport&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Erzeuge Dokumente aus SQL-Datenbanken&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;a href=&quot;https://mosling.gitlab.io/sqlreportdocs/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Dokumentation&lt;/span&gt;&lt;/a&gt; anzeigen&lt;br/&gt;Besuche GitLab &lt;a href=&quot;https://gitlab.com/mosling/SQLReport&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;SQLReport&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/about_dialog.ui" line="127"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbConnection</name>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="152"/>
        <source>unknown encoding &apos;%1&apos; should be in (%2)</source>
        <translation>unbekannte Kodierung &apos;%1&apos; muss eine aus der Liste (%2) sein</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="195"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="195"/>
        <source>%1 -- </source>
        <translation>%1 -- </translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="303"/>
        <source>%2 [%1,%4] --&gt; &apos;%3&apos;</source>
        <translation>%2 [%1,%4] --&gt; &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="311"/>
        <source>connect database &apos;%1&apos;</source>
        <translation>verbinde mit Datenbank &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="327"/>
        <source>remove database because driver changed from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>remove database because driver changed from &apos;%1&apos; to &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="330"/>
        <source>adding database type connection %1 for %2</source>
        <translation>adding database type connection %1 for %2</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="355"/>
        <source>missing database file for &apos;%1&apos;</source>
        <translation>fehlende Datenbankdatei für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="379"/>
        <source>open database &apos;%1&apos;</source>
        <translation>open database &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="395"/>
        <source>close database %1</source>
        <translation>close database %1</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="428"/>
        <source>[%1] use special SQL to get tables /%2/ </source>
        <translation>[%1] verwende Datenbank spezifische Abfrage um die Tabellen zu ermitteln /%2/ </translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="511"/>
        <source>no foreign key readout method for database type %1</source>
        <translation>keine Methode zum Auslesen der Fremdschlüssel aus der Datenbank bekannt %1</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="582"/>
        <source>no referenced field readout method for database type %1</source>
        <translation>keine Methode zum Auslesen der Referenzfelder aus der Datenbank bekannt %1</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="715"/>
        <source>&lt;%1&gt;%2</source>
        <translation>&lt;%1&gt;%2</translation>
    </message>
</context>
<context>
    <name>DbConnectionForm</name>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="20"/>
        <source>Database Connection</source>
        <translation>Datenbankverbindung bearbeiten</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="38"/>
        <source>Connection Name</source>
        <translation>Verbindungsname</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="48"/>
        <source>Identifying name for data source connection.</source>
        <translation>Name dieser konkreten Datenbankverbindung.</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="55"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="71"/>
        <source>The database type, controlled by Qt plugins.</source>
        <translation>Der Typ der Datenbank, wird durch die Qt sqldriver Plugins bestimmt.</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="81"/>
        <source>Database Name</source>
        <translation>Datenbankname</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="118"/>
        <source>Name of database (filename or simple name using a database server)</source>
        <translation>Name der Datenbank oder einer Datei (z.B. Firebird)</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="128"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="138"/>
        <source>Database Parameter</source>
        <translation>Parameter</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Format&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; param1:=value1|param2:=value2...&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Usage&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ${?param1}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This parameter are used to set input parameter for the database, this values can be overwritten by the query (not recommended) and local parameter which are not stored in the query set file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Format&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; param1:=value1|param2:=value2...&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Verwendung&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ${?param1}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Diese Parameter werden verwendet um verbindungsspezifische Informationen bereitzustellen.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="155"/>
        <source>Database Encoding</source>
        <translation>Kodierung</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="175"/>
        <source>Host</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="185"/>
        <source>The hostname or IP address.</source>
        <translation>Der Name des Datenbankservers oder dessen IP-Adresse (ohne Port).</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="192"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="202"/>
        <source>The port for the database server.</source>
        <translation>Der Port des Datenbankservers.</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="209"/>
        <source>Username</source>
        <translation>Nutzername</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="219"/>
        <source>Username to login into database.</source>
        <translation>Der Nutzer, der für die Anmeldung verwendet wird.</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="226"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="269"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Password to access the data with the username, please be sure your password appears in the project file if the checkox is activated. We store it base64 encoded with some salt to prevent users to get a view on it. But this isn&apos;t secure in any way.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Passwort in Verbindung mit dem Nutzernamen zur Identifizierung am Datenbankserver.&lt;/p&gt;&lt;p&gt;Bei der Speicherung des Passwortes in der Datenabankdatei wird eine base64 Kodierung mit etwas Salz verwendet um das Abgreifen beim Ansehen zu verhindern.&lt;p&gt;&lt;b style=&quot;color:orangered&quot;&gt;Dies ist keine sichere Methode, das Passwort zu speichern!&lt;/b&gt;&lt;/p&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="294"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked the password is stored in the project file, be aware that this isn&apos;t secure way to save passwords.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Das Passwort wird in der Datenbankdatei gespeichert.&lt;p&gt;&lt;b style=&quot;color:orangered&quot;&gt;Dies ist keine sichere Methode, das Passwort zu speichern!&lt;/b&gt;&lt;/p&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="307"/>
        <source>Show</source>
        <translation>Anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="317"/>
        <source>Connection Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="324"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Option1|Option2|...&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Option1|Option2|...&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="331"/>
        <source>DB Name Template</source>
        <translation>Verbindungsmuster</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="341"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If this value is set, it is used to set the database name of the Qt connection (i.e. QSqlDatabase::setDatabase). The format ist the same as used for normal templates. The parameters dbname, dbhost, dbport, dbuser and dbpassword are available and can be used with ${dbname} and so on. If a file is involved the additional parameter &lt;span style=&quot; font-weight:700;&quot;&gt;${filename}&lt;/span&gt; contains the absolute path of the current system.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Falls ein Muster angegeben ist, wird es verwendet um die Verbindungszeichenkette (i.e. QSqlDatabase::setDatabase) zu erstellen. Als Format kommt der Syntax wie bei den Standard Templates zum Einsatz. Vorbelegte Parameter sind dbname, dbhost, dbport, dbuser, dbpassword und filename. Der filename Parameter enthält den absoluten Pfad im aktuellen System.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="348"/>
        <source>Connection String</source>
        <translation>Verbindung</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="405"/>
        <source>Test Connection</source>
        <translation>Teste Verbindung</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="437"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="453"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.cpp" line="88"/>
        <source>Select Connection</source>
        <translation>Auswahl einer Verbindung</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.cpp" line="89"/>
        <source>ODBC Datasource:</source>
        <translation>ODBC Datenquelle:</translation>
    </message>
</context>
<context>
    <name>DbConnectionSet</name>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="64"/>
        <source>ignore to read the same database again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="87"/>
        <source>wrong or empty database set XML file &apos;%1&apos;</source>
        <translation>falsche oder leere Datenbank XML Datei &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="93"/>
        <source>root node in XML must be &apos;SQLREPORT&apos; but is &apos;%1&apos;</source>
        <translation>XML Wurzelknoten muss &apos;SQLREPORT&apos; sein, gefunden wurde &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="101"/>
        <source>read database set file &apos;%1&apos; version %2</source>
        <translation>Datenbankdatei &apos;%1&apos; Version %2 eingelesen</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="109"/>
        <source>sqlreport child %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="122"/>
        <source>the database file has no DATABASESET section, possible you select a queryset XML file -- ignore this file</source>
        <translation>die Datenbankdatei hat keinen DATABASE Abschnitt, möglicherweise wurde eine Abfrageliste ausgewählt -- ignoriere die Datei</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="151"/>
        <source>Overwrite connection %1</source>
        <translation>Ersetze vorhandene Verbindung %1</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="178"/>
        <source>can&apos;t open database set file &apos;%1&apos; for writing</source>
        <translation>Datenbank Datei &apos;%1&apos; kann nicht geschrieben werden</translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="181"/>
        <source>write database set to &apos;%1&apos;</source>
        <translation>schreibe Datenbank-Datei &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>DbConnectionStatus</name>
    <message>
        <location filename="../../src/db/dbconnectionstatus.ui" line="17"/>
        <source>Dialog</source>
        <translation>Verbindungstest</translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.ui" line="66"/>
        <source>Icon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.ui" line="81"/>
        <source>Status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.cpp" line="15"/>
        <source>Successful</source>
        <translation>Erfolgreich</translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.cpp" line="23"/>
        <source>Connection Error</source>
        <translation>Verbindungsfehler</translation>
    </message>
</context>
<context>
    <name>DbEncoding</name>
    <message>
        <location filename="../../src/db/dbencoding.cpp" line="40"/>
        <source>encoding &apos;%1&apos; is not supported should be one of (%2) -- using system</source>
        <translation>Kodierung &apos;%1&apos; wird nicht unterstützt, mögliche Werte sind (%2) -- verwende Systemkodierung</translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <location filename="../../src/editdialog.ui" line="17"/>
        <source>Dialog</source>
        <translation>Verbindungstest</translation>
    </message>
    <message>
        <location filename="../../src/editdialog.ui" line="64"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/editdialog.ui" line="71"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
</context>
<context>
    <name>EditWidget</name>
    <message>
        <location filename="../../src/editwidget.ui" line="17"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="56"/>
        <source>Find</source>
        <translation>Suche</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="69"/>
        <source>TextWrap</source>
        <translation>Zeilenumbruch</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="82"/>
        <source>Font</source>
        <translation>Schriftart</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="89"/>
        <source>Synchronize</source>
        <translation>Synchronisiere</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="118"/>
        <source>Print Pdf</source>
        <translation>Drucke PDF</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="131"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="141"/>
        <source>Save as</source>
        <translation>Speichern als</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="157"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="287"/>
        <source>Line:Column</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="324"/>
        <source>The current file name.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="101"/>
        <source>Fileinfo</source>
        <translation>Dateiinformation</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="102"/>
        <source>File &apos;%1&apos; not found.</source>
        <translation>Datei &apos;%1&apos; wurde nicht gefunden.</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="243"/>
        <location filename="../../src/editwidget.cpp" line="382"/>
        <source>Application</source>
        <translation>Applikation</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="244"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>Kann Datei &apos;%1&apos; nicht schreiben: %2.</translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="383"/>
        <source>The document &apos;%1&apos; has been modified.
Do you want to save your changes?</source>
        <translation>Das Dokument &apos;%1&apos; wurde geändert. Sollen die Änderungen gespeichert werden?</translation>
    </message>
</context>
<context>
    <name>LookupList</name>
    <message>
        <location filename="../../src/lookuplist.cpp" line="16"/>
        <source>insert key(%1)=&apos;%2&apos; into lookup list &apos;%3&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ParameterDialog</name>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="23"/>
        <source>Edit Parameter</source>
        <translation>Bearbeite Parameter</translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="55"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="132"/>
        <source>Add</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="157"/>
        <source>Del</source>
        <translation>Lösche</translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="170"/>
        <source>filter by Value</source>
        <translation>nach Wert filtern</translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="186"/>
        <source>Add Values</source>
        <translation>Werte hinzufügen</translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="218"/>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="231"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.cpp" line="69"/>
        <source>Query &apos;%1&apos; connected with &apos;%2&apos;</source>
        <translation>Abfrage &apos;%1&apos; verbunden mit Datenbank &apos;%2&apos;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/main.cpp" line="20"/>
        <source>start application &apos;%1&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="21"/>
        <source>build with Qt %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="38"/>
        <source>library path: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="44"/>
        <source>style: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="50"/>
        <source>sql driver plugin: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="56"/>
        <source>odbc connection: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="61"/>
        <source>QSettings storage place: %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="82"/>
        <source>SqlReport for command line usage</source>
        <translation>SqlReport Kommandozeilen Interface</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="85"/>
        <source>database connection XML file</source>
        <translation>Datenbankverbindungen XML Datei</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="86"/>
        <source>queryset XML file</source>
        <translation>Abfrageliste XML Datei</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="87"/>
        <source>query to execute</source>
        <translation>Abfrage zur Ausführung</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="88"/>
        <source>name of the used database connection</source>
        <translation>Name der Datenbankverbindung</translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="90"/>
        <source>optional parameter &quot;key:=value&quot; pairs, can be used multiple times</source>
        <translation>optionale Parameter &quot;parameter-name:=Wert&quot; Paare, kann mehrfach verwendet werden</translation>
    </message>
</context>
<context>
    <name>QueryExecutor</name>
    <message>
        <location filename="../../src/query_executor.cpp" line="90"/>
        <source>can&apos;t open %1 file &apos;%2&apos;</source>
        <translation>kann %1 Datei &apos;%2&apos; nicht öffnen</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="111"/>
        <source>can&apos;t find include file &apos;%1&apos;</source>
        <translation>kann eingebunde Datei &apos;%1&apos; nicht laden</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="115"/>
        <source>cyclic include found at %1 &lt; %2</source>
        <translation>Schleife beim Einbinden von Dateien gefunden %1 &lt; %2</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="161"/>
        <source>ARG_%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="228"/>
        <source>it looks you forgot the empty the description part (two commas) %1,,%2 -- execute anyway</source>
        <translation>es wurden die 2 Kommas bei einer Nutzervariablen vergessen %1,,%2 -- wird ausgeführt</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="325"/>
        <source>No RTF String found -- use given string</source>
        <translation>keine RTF Zeichenkette gefunden --&gt; verwende die Zeichenkette</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="351"/>
        <source>Please input value for the user variable</source>
        <translation>Bitte einen Wert für die Nutzervariable eingeben</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="576"/>
        <source>can&apos;t convert &apos;%1&apos; to a timestamp number</source>
        <translation>kann &apos;%1&apos; nicht in einen Zeitstempel umwandeln</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="589"/>
        <source>%1 modifier need an ouput date format</source>
        <translation>diese Form &apos;%1&apos; benötigt ein Ausgabeformat</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="610"/>
        <source>the string &apos;%1&apos; dosen&apos;t match the date format &apos;%2&apos; for &apos;%3&apos;</source>
        <translation>der Text &apos;%1&apos; entspricht nicht dem geforderten Format &apos;%2&apos; für &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="625"/>
        <source>Not supported variable conversion &apos;%1&apos;.</source>
        <translation>falsche Modifikation &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="681"/>
        <source>lookup list &apos;%1&apos; exists with %2 %3.</source>
        <translation>Nachschlageliste &apos;%1&apos; enthält %2 %3.</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="685"/>
        <source>no lookup list with name &apos;%1&apos; found</source>
        <translation>keine Nachschlageliste für &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="697"/>
        <source>to few parameters expected &apos;__insert,&lt;listname&gt;,&lt;key&gt;,&lt;value&gt;&apos; but given &apos;%1&apos;</source>
        <translation>zu wenige Parameter angegeben &apos;__insert,&lt;listname&gt;,&lt;key&gt;,&lt;value&gt;&apos; ist aber &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="704"/>
        <source>to few parameters expected &apos;__get,&lt;listname&gt;,&lt;key&gt;&apos; but given &apos;%1&apos;</source>
        <translation>zu wenige Parameter angegeben &apos;__get,&lt;listname&gt;,&lt;key&gt;&apos; ist aber &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="713"/>
        <source>lookup list with name &apos;%1&apos; doesn&apos;t exists</source>
        <translation>Nachschlageliste &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="722"/>
        <source>missing lookup key parameter &apos;%1&apos;</source>
        <translation>fehlender Schlüssel &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="737"/>
        <source>missing lookup value parameter &apos;%1&apos;.</source>
        <translation>fehlender Wert &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="789"/>
        <source>wrong &apos;%1&apos; only __COUNTER or __COUNTERH is supported</source>
        <translation>falscher Modifikator &apos;%1&apos; nur __COUNTER oder __COUNTERH erlaubt</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="794"/>
        <source>global variable %1 was removed! Replace with __COUNTER.</source>
        <translation>globale Variable %1 wurde entfernt! Mit __COUNTER ersetzen.</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="849"/>
        <source>unsupported global command &apos;%1&apos; found</source>
        <translation>nicht unterstütztes globales Kommando &apos;%1&apos; gefunden</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="933"/>
        <source>please use &apos;%1&apos; as identifier for the Javascript block in %2 file (&apos;%3&apos; was used)</source>
        <translation>bitte &apos;%1&apos; als Kennzeichnung des Javascript Blockes in der Datei %2 verwenden (&apos;%3&apos; wurde gefunden)</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="950"/>
        <source>the database specific SQL for %1 has no driver avilable in the system</source>
        <translation>der Treiber für das datenbankspezifische SQL für &apos;%1&apos; ist nicht vorhanden</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="957"/>
        <source>the SQL &apos;%1&apos; was added before and is ignored (definition existst multiple times)</source>
        <translation>mehrfache Verwendung von &apos;%1&apos; im SQL wird ignoriert</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="972"/>
        <source>the template &apos;%1&apos; was added before and is ignored (definition existst multiple times)</source>
        <translation>mehrfache Verwendung von &apos;%1&apos; im Template wird ignoriert</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="979"/>
        <source>adding Template-Block &apos;%1&apos;</source>
        <translation>füge Template Block &apos;%1&apos; hinzu</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="995"/>
        <source>application error unknown map type category &apos;%1&apos;</source>
        <translation>Applikationsfehler unbekannte Kategorie &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1141"/>
        <source>Overwrite %1 &apos;%2&apos; at line %3 in %4</source>
        <translation>Überschreibe %1 &apos;%2&apos; an Zeile %3 in %4</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1151"/>
        <source>Missing block for %1 at line %2 in %3</source>
        <translation>fehlender Block für %1 in der Zeile %2 in %3</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1231"/>
        <source>javascript error &apos;%1&apos; at line %2 in %3 file</source>
        <translation>javascript Fehler &apos;%1&apos; an der Zeile %2 in der Datei %3</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1240"/>
        <source>%1: %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2159"/>
        <source>can&apos;t execute batch query &apos;%1&apos; using database &apos;%2&apos;</source>
        <translation>Batch Abfrage &apos;%1&apos; kann nicht ausgeführt werden (Datenbank ist &apos;%2&apos;)</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2170"/>
        <source>Batch Execution File &apos;%1&apos; doesn&apos;t exists</source>
        <translation>Datei zur Batch-Ausführungg &apos;%1&apos; fehlt</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2204"/>
        <source>batch execution time: %1</source>
        <translation>Ausführungszeit Batch: %1</translation>
    </message>
    <message>
        <source>javascript error &apos;%1&apos; at line %2 </source>
        <translation type="vanished">Javascript Fehler &apos;%1&apos; an Zeile %2 </translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1265"/>
        <source>create path %1</source>
        <translation>erstelle Pfad %1</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1272"/>
        <source>Can&apos;t open file &apos;%1&apos;</source>
        <translation>Datei &apos;%1&apos; kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1288"/>
        <source>generated file (%1) &apos;%2&apos;</source>
        <translation>erstelle Datei (%1) &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1318"/>
        <source>add parameter &apos;%1&apos; with value &apos;%2&apos;</source>
        <translation>füge Parameter &apos;%1&apos; mit dem Wert &apos;%2&apos; hinzu</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1324"/>
        <source>overwrite parameter %1 := &apos;%2&apos; with new value &apos;%3&apos;</source>
        <translation>überschriebe Parameter %1 := %2 mit dem Wert &apos;%3&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1348"/>
        <source>sqlReport interprets &lt;b&gt;&apos;%1&apos;&lt;/b&gt; as &lt;b&gt;&apos;eval&apos;&lt;/b&gt;, please remove the surrounding whitspaces</source>
        <translation>&lt;b&gt;&apos;%1&apos;&lt;/b&gt; wird als &lt;b&gt;&apos;eval&apos;&lt;/b&gt; verwendet, entferne die Leerzeichen, um die Maldung zu unterbinden</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1383"/>
        <source>error &apos;%1&apos; at line %2 evaluate script /%3/ at line %4</source>
        <translation>Fehler &apos;%1&apos; auf Zeile %2 beim Parsen von Javascripte /%3/ auf Zeile %4</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1438"/>
        <source>unknwon parameter &apos;%1&apos; used at &apos;%2&apos;</source>
        <translation>unbekannter Parameter &apos;%1&apos; verwendet in &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1480"/>
        <source>using pipelining at line %2 with an existing variable &apos;$%1&apos;, please rename this</source>
        <translation>es wird die interne Pipeline Variable &apos;$%1&apos; auf Zeile %2 in einer Pipeline verwendet, bitte Variable umbenennen</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1605"/>
        <source>error &apos;%1&apos; at line %2 evaluate script /%3/ at template line %4</source>
        <translation>Fehler &apos;%1&apos; auf Zeile %2 während der Ausführung des Javascripts /%3/ auf Template Zeile %4</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1690"/>
        <source>Unknown QuerySet &apos;%1&apos; at line %2</source>
        <translation>Unbekannte Abfrageliste &apos;%1&apos; an Zeile %2</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1831"/>
        <source>no ending double quote in argument list of ##%1##</source>
        <translation>Fehlende abschließenden Anführungszeichen bei den Argumenten ##%1##</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1929"/>
        <source>no parameter &apos;%1&apos; found for SPLIT</source>
        <translation>kein Parameter &apos;%1&apos; für SPLIT vorhanden</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1945"/>
        <source>parameter &apos;%1&apos; doesn&apos;t exists at current replacements</source>
        <translation>Parameter &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1949"/>
        <source>ignore empty list &apos;%1&apos;</source>
        <translation>ignoriere leere Liste &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1957"/>
        <source>output template &apos;%1&apos; from list &apos;%2&apos; split by &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1964"/>
        <source>output template &apos;%1&apos; using query &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2017"/>
        <source>[%1]</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2046"/>
        <source>template %1 isn&apos;t defined</source>
        <translation>Template mit dem Namen &apos;%1&apos; ist nicht definiert</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2078"/>
        <source>stop execution --&gt;  missing database for an existing SQL file</source>
        <translation>Ausführung angehalten --&gt; SQL existiert aber keine Datenbank angegeben</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2123"/>
        <source>summary time: %1; executing %2 %3</source>
        <translation>Ausführungszeit: %1; es wurden %2 %3 ausgeführt</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2126"/>
        <source>query</source>
        <translation>Abfrage</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2126"/>
        <source>queries</source>
        <translation>Abfragen</translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2131"/>
        <source>found some errors during execution</source>
        <translation>während der Ausführung traten Fehler auf</translation>
    </message>
</context>
<context>
    <name>QuerySet</name>
    <message>
        <location filename="../../src/query_set.cpp" line="160"/>
        <source>reading node %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="164"/>
        <source>wrong or empty query set XML file &apos;%1&apos;</source>
        <translation>falsche oder leere QuerySet XML Datei &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="170"/>
        <source>root node in XML must be &apos;SQLREPORT&apos; but is &apos;%1&apos;</source>
        <translation>XML Wurzelknoten muss &apos;SQLREPORT&apos; sein, gefunden wurde &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="179"/>
        <source>read query set file &apos;%1&apos; version %2</source>
        <translation>lese QuerySet Datei &apos;%1&apos; Version %2</translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="186"/>
        <source>sqlreport child %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="191"/>
        <source>queryset node %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="265"/>
        <source>please check, you read a database file as queryset file -- ignoring</source>
        <translation>es wird versucht eine Datenbank-Datei als Abfrage-Datei einzulesen -- ignoriere die Datei</translation>
    </message>
    <message>
        <source>please check, you read a database file as queryset file</source>
        <translation type="vanished">es wird versucht eine Datenbank-Datei als Abfrage-Datei einzulesen</translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="303"/>
        <source>can&apos;t open query set file &apos;%1&apos; for writing</source>
        <translation>QuerySet Datei &apos;%1&apos; kann nicht beschrieben werden</translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="306"/>
        <source>write query set to &apos;%1&apos;</source>
        <translation>speichere QuerySet nach &apos;%1&apos;</translation>
    </message>
</context>
<context>
    <name>RecentComboBox</name>
    <message>
        <location filename="../../src/recentcombobox.cpp" line="39"/>
        <source>query set &apos;%1&apos; not exists --&gt; remove from list</source>
        <translation>Abfrage &apos;%1&apos; existiert nicht mehr --&gt; entferne sie von der Liste</translation>
    </message>
</context>
<context>
    <name>ResultSetList</name>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="10"/>
        <source>split list &apos;%1&apos; at &apos;%2&apos; accessible via &apos;%3&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="17"/>
        <source>compare &apos;%1&apos; with &apos;%2&apos;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="34"/>
        <source>empty separator given using comma</source>
        <translation>verwende das Standardtrennzeichen Komma</translation>
    </message>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="77"/>
        <source>set list value to &apos;%1&apos;</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ResultSetSql</name>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="28"/>
        <source>SQL-Query: %1%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="46"/>
        <source>## error executing %1 ## %2 ##</source>
        <translation>## Fehler bei %1 ## %2 ##</translation>
    </message>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="105"/>
        <source>can&apos;t convert &apos;%1&apos; to QString use binary data</source>
        <translation>&apos;%1&apos; kann nicht in einen QString konvertiert werden</translation>
    </message>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="130"/>
        <source>column[%1]:%2 %3 = /%4/ len(%5)=0x%6 </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SqlReport</name>
    <message>
        <location filename="../../src/sql_report.ui" line="19"/>
        <source>SqlReport</source>
        <translation></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Auswahl</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">Bearbeiten</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Switch the display between developer and executor view.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Umschalten zwischen Benutzer und Entwickler Sicht.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open an edit dialog to modify, add or remove parameters for local system, query and database.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Öffne den Dialog zum Bearbeiten der Parameter (Abfrage, Datenbank, lokale Einstellungen).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Edit Parameter</source>
        <translation type="vanished">Bearbeite Parameter</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit the description for the query set, this is used during generating the Readme.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bearbeite die Beschreibung für die Abfrageliste, diese wird für die Erstellung der Readme-Datei verwendet.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="449"/>
        <location filename="../../src/sql_report.ui" line="1415"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Generate a Readme.adoc Asciidoctor file from the query set entries.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Erzeuge eine Readme.adoc (Asciidoc) für die aktuellen Abfragen.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Generate Readme</source>
        <translation type="vanished">Erstelle Readme</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="84"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Format&lt;/span&gt;: param1:=value1|param2:=value2...&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Usage&lt;/span&gt;: ${?param1}&lt;/p&gt;&lt;p&gt;This local parameters are &lt;span style=&quot; font-weight:600;&quot;&gt;not&lt;/span&gt; stored in the query set file. The can be used to overwrite all other parameters (i.e. query and database) for some cases (e.g. local paths outside of query definition).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="873"/>
        <location filename="../../src/sql_report.ui" line="1328"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="691"/>
        <location filename="../../src/sql_report.cpp" line="30"/>
        <source>Template</source>
        <translation></translation>
    </message>
    <message>
        <source>EN</source>
        <translation type="vanished">EN</translation>
    </message>
    <message>
        <source>DE</source>
        <translation type="vanished">DE</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="609"/>
        <location filename="../../src/sql_report.cpp" line="29"/>
        <source>SQL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="74"/>
        <location filename="../../src/sql_report.ui" line="544"/>
        <location filename="../../src/sql_report.ui" line="1493"/>
        <source>Parameter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="161"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;Queries&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;Abfrageliste&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="226"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete the selected file from the recent queries list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Enferne die Abfrageliste von der Liste zuletzt verwendeten Abfragelisten.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="259"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;&lt;span style=&quot; font-weight:700; color:#ff9300;&quot;&gt;Query&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;&lt;span style=&quot; font-weight:700; color:#ff9300;&quot;&gt;Abfrage&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="517"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display and Edit the Query Description.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Anzeige und Bearbeitung der Abfragebeschreibung.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="664"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the connected SQL file for editing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Öffen die SQL-Datei zur Bearbeitung.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="746"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the connected Template file for editing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Öffne das Template zur Bearbeitung.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="837"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display the created result. This is for the latest result available only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Anzeige der erzeugten Ausgabe, dazu muss diese zuerst erzuegt werden.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="919"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the selected Query after loading the Query Set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Diese Abfrage wird aktiviert, wenn die Liste geladen wird, nur eine möglich.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="922"/>
        <source>Default</source>
        <translation>Aktiviert</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="935"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked the gererated output file is used as input for the process. Each line has the form&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!!&amp;lt;Query Name&amp;gt;!!Param1:=Value1!!Param2:=Value2&lt;/span&gt;&lt;/p&gt;&lt;p&gt;with each parameter optional. See documentation.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="938"/>
        <source>Batch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="957"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked the generated output is added to an existing file, otherwise the file is rewritten. This is useful ic you have multiple queries which are executed as batch process and should build one result file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Die erzeugte Ausgabe wird an eine vorhandene Datei angefügt (bzw. erstellt sie, wenn nicht vorhanden). Das kann verwendet werden, wenn mit einer Batch-Datei eine einzelne Ausgabedatei erzeugt werden soll. Achtung das Flag muss an der erzeugenden Abfrage gesetzt sein, nicht an der Batch-Abfrage.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="960"/>
        <source>Append</source>
        <translation>Anfügen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="973"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add timestamp to generated output file. The format is &apos;yyyy-mm-dd-modifier-test-HHMM.txt&apos; if the output filename is modifier-test.txt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Hinzufügen eines Zeitstempels zum Namen der Ausgabe im Format &apos;yyyy-mm-dd-modifier-test-HHMM.txt&apos; wenn der Dateiname modifier-test.txt ist.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="976"/>
        <source>Timestamp</source>
        <translation>Zeitstempel</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="989"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The generated output file has Latin1 encoding, the default encoding is UTF-8 without BOM.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Die erzeugte Ausgabe ist Latin1 kodiert, die Standardkodierung ist UTF-8 ohne BOM.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="992"/>
        <source>Latin1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1005"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The requested output format is XML. In this case in all incoming strings the characters &amp;amp;,&amp;lt;,&amp;gt; and double quotes are replaced with the long representation (i.e. &amp;amp;amp; for &amp;amp;) &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Das Ausgabeformat ist eine XML-Datei. Es werden die XML-Strukturzeichen &amp;amp;,&amp;lt;,&amp;gt; und doppelte Anführungszeichen durch ihre XML Repräsentation (i.e. &amp;amp;amp; for &amp;amp;) ersetzt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1008"/>
        <source>XML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1021"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If the variable &lt;span style=&quot; font-weight:700;&quot;&gt;${XYZ}&lt;/span&gt; not exists, normally the message  [&apos;XYZ&apos; is unknown] is added to the ouput. If this is set, nothing is added to the output, the error message is shown in the &lt;span style=&quot; font-weight:700;&quot;&gt;More ...&lt;/span&gt; textbox only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Falls die Variable &lt;span style=&quot; font-weight:700;&quot;&gt;${XYZ}&lt;/span&gt; nicht existiert, wird die Nachricht [&apos;XYZ&apos; is unknown] zur Ausgabe hinzugefügt. das wird hiermit verhindert, der Fehler wird nur im Nachrichtenbereich angezeigt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1024"/>
        <source>Suppress</source>
        <translation>Unterbinden</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1031"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If this is checked all values from the database which are NULL are replaced with empty string.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Alle NULL Werte von der Datenbank werden in eine leere Zeichenkette umgewandelt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1034"/>
        <source>NULL as empty</source>
        <translation>NULL als leer</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1047"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximal allowed recursion depth for this query, can be used to stop recursive templated calls.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Die maximale Rekursionstiefe (experimentell), bei erreichen wird keine weiterer Rekuriosnschritt mehr ausgeführt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1050"/>
        <source>10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1063"/>
        <source>Max. Recursion</source>
        <translation>max. Rekursion</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1089"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Switch between &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Info&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Debug&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Trace&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; output level.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Meldungsanzeige für &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Info&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Debug&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Trace&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1092"/>
        <source>Debug</source>
        <translation>Meldungslevel</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Query&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Abfrage&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="416"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit the selected database connection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bearbeite die aktuelle Datenbankverbindung.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="773"/>
        <location filename="../../src/sql_report.cpp" line="31"/>
        <source>Output</source>
        <translation>Resultat</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1455"/>
        <source>Structure</source>
        <translation>Struktur</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1375"/>
        <location filename="../../src/sql_report.ui" line="1390"/>
        <location filename="../../src/sql_report.ui" line="1420"/>
        <location filename="../../src/sql_report.ui" line="1445"/>
        <source>New</source>
        <translation>Neu</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Entfernen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1114"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Messages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Meldungen&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="590"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Format&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; param1:=value1|param2:=value2...&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Usage&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ${?param1}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This parameters are used to set input parameter for the query, this values can be overwritten by the local parameter which are not stored in the query set file.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Format&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; param1:=value1|param2:=value2...&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Anwendung&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ${?param1}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Hier werden die Parameter für die Abfrage festgelegt.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="489"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some useful informations describing the query.&lt;/p&gt;&lt;p&gt;This can be stored in AsciiDoc format and can later be used to generate a documentation.&lt;/p&gt;&lt;p&gt;This line show the first characters of the beginning of the text with the [=]* header removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Beschreibung der Abfrage in AsciiDoc, daraus wird später die Dokumentation erzeugt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Expand</source>
        <translation type="vanished">Bearbeiten</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="345"/>
        <location filename="../../src/sql_report.ui" line="1318"/>
        <source>Database</source>
        <translation>Datenbank</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Load a query set with query and optional database connections from a file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lade eine Liste von Abfragen aus einer Datei.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1380"/>
        <location filename="../../src/sql_report.ui" line="1395"/>
        <source>Load</source>
        <translation>Laden</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1400"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Queries&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Abfragenliste&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="314"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;A query is defined by some properties including file names. By clicking &amp;quot;Edit&amp;quot; the current query can be renamed or duplicated in a new query.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">Anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1193"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This button stop the current execution.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Anhalten der aktuellen Berechnung.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1196"/>
        <source>Stop</source>
        <translation>Anhalten</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1218"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
hr { height: 1px; border-width: 0; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;Press these button to the processing. At this time we create the database connection and parse the template file starting with the &lt;/span&gt;&lt;span style=&quot; font-weight:700; color:#000000;&quot;&gt;::MAIN&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;If the active query set is BATCH we execute the generated query statements atarting with double exclamation mark.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1227"/>
        <source>Start</source>
        <translation>Ausführen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1308"/>
        <source>Query</source>
        <translation>Abfrage</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1332"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1343"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1355"/>
        <source>Queries</source>
        <translation>Abfrageliste</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1405"/>
        <source>Remove from List</source>
        <translation>Entferne von Liste</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1410"/>
        <source>Edit Description</source>
        <translation>Bearbeite Beschreibung</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1430"/>
        <location filename="../../src/sql_report.ui" line="1450"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1435"/>
        <source>Load SQL</source>
        <translation>Öffne SQL</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1440"/>
        <source>Load Template</source>
        <translation>Öffne Template</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1460"/>
        <source>Key Structure</source>
        <translation>Struktur mit Beziehungen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1478"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1488"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1498"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1503"/>
        <source>Exit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="96"/>
        <location filename="../../src/sql_report.ui" line="1347"/>
        <source>Databases</source>
        <translation>Datenbanken</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Applikation</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="32"/>
        <source>Query Set Description</source>
        <translation>Abfragebeschreibung</translation>
    </message>
    <message>
        <source>SQL Editor</source>
        <translation type="vanished">SQL Editor</translation>
    </message>
    <message>
        <source>Show Report Output</source>
        <translation type="vanished">Ausgabe anzeigen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="87"/>
        <source>missing databases file, please select one</source>
        <translation>fehlende Datenbankverbindungsdatei, bitte eine auswählen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="96"/>
        <source>missing queries file, please select one</source>
        <translation>fehlende Datei für Abfrageliste, bitte Datei selektieren</translation>
    </message>
    <message>
        <source>Structure (+)</source>
        <translation type="vanished">Tabellen/Referenzen</translation>
    </message>
    <message>
        <source>Get tables and references from database.</source>
        <translation type="vanished">Tabellen und Referenzen der Datenbank</translation>
    </message>
    <message>
        <source>Create new connection or copy and rename existing connection.</source>
        <translation type="vanished">Erstelle neue Datenbankverbindung.</translation>
    </message>
    <message>
        <source>Remove from databases</source>
        <translation type="vanished">Lösche Datenbankverbindung</translation>
    </message>
    <message>
        <source>New connection list storage file.</source>
        <translation type="vanished">Neue Datenbank XML Datei.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1385"/>
        <source>Merge</source>
        <translation>Vereinigen</translation>
    </message>
    <message>
        <source>Add new and overwrite existing connection.</source>
        <translation type="vanished">Fügt neue Datenbankverbindungen ein und überschreibt vorhandene.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="198"/>
        <source>Select QuerySet file to save</source>
        <translation>Abfrageliste speichern</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="200"/>
        <location filename="../../src/sql_report.cpp" line="223"/>
        <location filename="../../src/sql_report.cpp" line="588"/>
        <location filename="../../src/sql_report.cpp" line="1248"/>
        <source>XML Files (*.xml)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="221"/>
        <source>Select DatabaseSet file to save</source>
        <translation>Datenbankliste speichern</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="334"/>
        <source>Unknown QuerySet &apos;%1&apos; at line %2</source>
        <translation>Unbekannte Abfrageliste &apos;%1&apos; an Zeile %2</translation>
    </message>
    <message>
        <source>Batch Execution</source>
        <translation type="vanished">Batch Ausführung</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; doesn&apos;t exists</source>
        <translation type="vanished">Datei &apos;%1&apos; existiert nicht</translation>
    </message>
    <message>
        <source>batch execution time: %1</source>
        <translation type="vanished">Ausführungszeit Batch: %1</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="535"/>
        <source>Remove database connection &apos;%1&apos;?</source>
        <translation>Soll Datenbankverbindung &apos;%1&apos; entfernt werden?</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="586"/>
        <source>Please select new QuerySet file</source>
        <translation>Erstelle Abfrageliste</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="604"/>
        <source>Please select QuerySet file</source>
        <translation>Lade Abfrageliste</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="606"/>
        <source>XML-Files (*.xml);;All Files(*.*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="624"/>
        <source>query file &apos;%1&apos; no longer exists -&gt; remove from recent queries</source>
        <translation>Abfrageliste &apos;%1&apos; existiert nicht mehr --&gt; von der Liste entfernt</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="640"/>
        <source>%1.lockfile</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="649"/>
        <source>&apos;%3&apos; is locked by %1 at %2 (Attention: information only, no action follows)</source>
        <translation>&apos;%3&apos; ist gesperrt durch %1 auf %2 (Achtung, dies ist nur eine Information)</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="772"/>
        <source>* Ausgabedatei `%1`%2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="774"/>
        <source> wird erweitert</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="774"/>
        <source> wird erstellt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="852"/>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="868"/>
        <source>Missing Name</source>
        <translation>Name nicht gesetzt</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="868"/>
        <source>The entry need a name.</source>
        <translation>Es wird ein Name benötigt.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="895"/>
        <source>No entry selected!</source>
        <translation>Keine Abfrage ausgewählt!</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="895"/>
        <source>You can rename an existing query only.</source>
        <translation>Umbenennung nicht möglich, keine query ausgewählt.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="901"/>
        <source>New Query Name</source>
        <translation>Neuer Abfrage Name</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="929"/>
        <source>Remove query &apos;%1&apos;?</source>
        <translation>Soll Abfrage &apos;%1&apos; entfernt werden?</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="950"/>
        <source>there is no active query, which can removed.</source>
        <translation>es gibt keine aktive Abfrage, die entfernt werden kann.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="958"/>
        <source>You have to set a query to get the command line.</source>
        <translation>Ohne aktive Abfrage, kann keine Kommandozeile angezeigt werden.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="965"/>
        <source>execute this query using the command line </source>
        <translation>die folgende Kommandozeile führt die Abfrage aus </translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="966"/>
        <source>%1 %2 %3 %4 %5 {-p key:=value}</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1077"/>
        <source>get %1database structure ...</source>
        <translation>ermittle %1 Datenbankstruktur ...</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="668"/>
        <source>migrate &apos;%1&apos; to configuration version 3 </source>
        <translation>überführe &apos;%1&apos; in die Version 3 </translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="669"/>
        <source>create database set file %1</source>
        <translation>erstelle Datenbank Datei &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="723"/>
        <source>Can&apos;t open file &apos;%1&apos; for writing.</source>
        <translation>kann Datei &apos;%1&apos; nicht schreiben (Zugriffsrechte, Pfad nicht vorhanden, Datei geöffnet).</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="776"/>
        <source>* `%1` enthält die SQL Anweisungen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="777"/>
        <source>* `%1` enthält die Musterausgabe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="779"/>
        <source>* Standardabfrage bei Programmstart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="781"/>
        <source>* Batch zur Ausführungen weiterer Anweisungen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="783"/>
        <source>* Encoding der Ausgabe ist Latin1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="785"/>
        <source>* Ausgabe als XML, String werden automatisch gequotet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="787"/>
        <source>* Dateiname bekommt einen Zeitstempel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="789"/>
        <source>* Unbekannte Variablen erzeugen Fehler in der Ausgabe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="791"/>
        <source>* NULL wird als leere Zeichenkette verwendet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="793"/>
        <source>* Rekursionstiefe auf %1 festgelegt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="799"/>
        <source>Readme generated at %1</source>
        <translation>Readme-Datei erzeugt als %1</translation>
    </message>
    <message>
        <source>User Mode</source>
        <translation type="vanished">Benutzer Modus</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1468"/>
        <source>Development Mode</source>
        <translation>Entwickler Modus</translation>
    </message>
    <message>
        <source>Rename/Copy Query</source>
        <translation type="vanished">Neu (Umbenenen)</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation type="vanished">Neu aus Kopie</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1425"/>
        <location filename="../../src/sql_report.cpp" line="902"/>
        <source>Rename</source>
        <translation>Umbenennen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="851"/>
        <source>Set Query Name</source>
        <translation>Neue Abfrage</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Erstellen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="861"/>
        <location filename="../../src/sql_report.cpp" line="911"/>
        <source>Entry exists!</source>
        <translation>Abfrage existiert!</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="862"/>
        <location filename="../../src/sql_report.cpp" line="912"/>
        <source>The entry &apos;%1&apos; exists in the set.</source>
        <translation>Eintrag &apos;%1&apos; existiert bereits.</translation>
    </message>
    <message>
        <source>Remove query set &apos;%1&apos;?</source>
        <translation type="vanished">Enferne Abfrageliste &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="930"/>
        <source>Used files are not removed!</source>
        <translation>Es werden keine Dateien gelöscht!</translation>
    </message>
    <message>
        <source>There is no active query set, which can removed.</source>
        <translation type="vanished">Es gibt keine Abfrage zum Löschen.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1001"/>
        <location filename="../../src/sql_report.cpp" line="1027"/>
        <source>Missing File</source>
        <translation>Fehlende Datei</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1002"/>
        <location filename="../../src/sql_report.cpp" line="1028"/>
        <source>Please select a file or give a name!</source>
        <translation>Bitte Datei auswählen oder Name eintragen!</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1051"/>
        <source>Missing or Outdated File!</source>
        <translation>Keine Datei zum Anzeigen vorhanden!</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1052"/>
        <source>Please create output first (Start)
(Uncheck batch checkbox to see the result.)</source>
        <translation>Bitte zuerst eine Ausgabe erzeugen
(um die Batchdatei zu sehen, das Batch-Flag entfernen)</translation>
    </message>
    <message>
        <source>Get database structure ...</source>
        <translation type="vanished">Bestimme die Datenbankstruktur ...</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1081"/>
        <source>ready execution time: %1</source>
        <translation>abgeschlossen in %1</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1161"/>
        <source>No active query entry</source>
        <translation>keine aktive Abfrage</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1162"/>
        <source>Please select a query or create a new.</source>
        <translation>Bitte eine Abfrage auswählen oder erstellen.</translation>
    </message>
    <message>
        <source>reading the same database again</source>
        <translation type="vanished">lese die Datenbankdatei erneut</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1234"/>
        <source>XML-Files (*.xml)</source>
        <translation>XML-Datei (*.xml)</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1246"/>
        <source>Please select new Databases file</source>
        <translation>Lade Datenbank Datei</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1256"/>
        <source>selecting the same database again -- do nothing</source>
        <translation>die Datei ist schon geladen -- ignoriere die Aktion</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1349"/>
        <source>export using base path &apos;%1&apos;</source>
        <translation>erzeuge Abfragen-Export in &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1376"/>
        <source>can&apos;t open export file &apos;%1&apos; for query &apos;%2&apos;</source>
        <translation>Fehler beim Export der Datei &apos;%1&apos; bei der Abfrageliste &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1411"/>
        <source>missing %1 -- stop export.</source>
        <translation>fehlende Dateien %1 -- halte Export an.</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1417"/>
        <source>Please select Output Directory</source>
        <translation>Bitte ein Ausgabeverzeichnis wählen</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1451"/>
        <source>export &apos;%1&apos;</source>
        <translation>exportiere %1</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1454"/>
        <source>files of Query Set exported to &apos;%1&apos;</source>
        <translation>Dateien der Abfrageliste nach &apos;%1&apos; exportiert</translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1460"/>
        <source>Language Switch</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1461"/>
        <source>Please restart the application to activate the new language.</source>
        <translatorcomment>Nicht übersetzen in der Hoffnung englisch wird verstanden.</translatorcomment>
        <translation></translation>
    </message>
</context>
<context>
    <name>SqlReportCli</name>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="36"/>
        <source>the queryset has no query &apos;%1&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="39"/>
        <source>possible queries are:</source>
        <translation>mögliche Abfragen sind:</translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="43"/>
        <source>find and activate query &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="53"/>
        <source>read database connection from separate file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="61"/>
        <source>the queryset has no database connection &apos;%1&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="65"/>
        <source>for queryset files with version greater than 3 is a separate database file required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="74"/>
        <source>possible databases are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="78"/>
        <source>find and activate database connection &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="98"/>
        <source> - &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>batch execution time: %1</source>
        <translation type="obsolete">Ausführungszeit Batch: %1</translation>
    </message>
</context>
</TS>
