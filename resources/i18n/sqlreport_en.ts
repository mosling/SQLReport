<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/about_dialog.ui" line="20"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/about_dialog.ui" line="105"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Generate Output from SQL Sources&lt;/span&gt;&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;See &lt;a href=&quot;https://mosling.gitlab.io/sqlreportdocs/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;Documentation&lt;/span&gt;&lt;/a&gt;&lt;br/&gt;Visit the GitLab Repository &lt;a href=&quot;https://gitlab.com/mosling/SQLReport&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;SQLReport&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/about_dialog.ui" line="127"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbConnection</name>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="152"/>
        <source>unknown encoding &apos;%1&apos; should be in (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="195"/>
        <source>%1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="195"/>
        <source>%1 -- </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="303"/>
        <source>%2 [%1,%4] --&gt; &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="311"/>
        <source>connect database &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="327"/>
        <source>remove database because driver changed from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="330"/>
        <source>adding database type connection %1 for %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="355"/>
        <source>missing database file for &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="379"/>
        <source>open database &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="395"/>
        <source>close database %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="428"/>
        <source>[%1] use special SQL to get tables /%2/ </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="511"/>
        <source>no foreign key readout method for database type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="582"/>
        <source>no referenced field readout method for database type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection.cpp" line="715"/>
        <source>&lt;%1&gt;%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbConnectionForm</name>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="20"/>
        <source>Database Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="38"/>
        <source>Connection Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="48"/>
        <source>Identifying name for data source connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="55"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="71"/>
        <source>The database type, controlled by Qt plugins.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="81"/>
        <source>Database Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="118"/>
        <source>Name of database (filename or simple name using a database server)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="128"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="138"/>
        <source>Database Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Format&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; param1:=value1|param2:=value2...&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Usage&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ${?param1}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This parameter are used to set input parameter for the database, this values can be overwritten by the query (not recommended) and local parameter which are not stored in the query set file.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="155"/>
        <source>Database Encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="175"/>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="185"/>
        <source>The hostname or IP address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="192"/>
        <source>Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="202"/>
        <source>The port for the database server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="209"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="219"/>
        <source>Username to login into database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="226"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="269"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Password to access the data with the username, please be sure your password appears in the project file if the checkox is activated. We store it base64 encoded with some salt to prevent users to get a view on it. But this isn&apos;t secure in any way.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="294"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked the password is stored in the project file, be aware that this isn&apos;t secure way to save passwords.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="307"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="317"/>
        <source>Connection Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="324"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Option1|Option2|...&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="331"/>
        <source>DB Name Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="341"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If this value is set, it is used to set the database name of the Qt connection (i.e. QSqlDatabase::setDatabase). The format ist the same as used for normal templates. The parameters dbname, dbhost, dbport, dbuser and dbpassword are available and can be used with ${dbname} and so on. If a file is involved the additional parameter &lt;span style=&quot; font-weight:700;&quot;&gt;${filename}&lt;/span&gt; contains the absolute path of the current system.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="348"/>
        <source>Connection String</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="405"/>
        <source>Test Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="437"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.ui" line="453"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.cpp" line="88"/>
        <source>Select Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_form.cpp" line="89"/>
        <source>ODBC Datasource:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbConnectionSet</name>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="64"/>
        <source>ignore to read the same database again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="87"/>
        <source>wrong or empty database set XML file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="93"/>
        <source>root node in XML must be &apos;SQLREPORT&apos; but is &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="101"/>
        <source>read database set file &apos;%1&apos; version %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="109"/>
        <source>sqlreport child %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="122"/>
        <source>the database file has no DATABASESET section, possible you select a queryset XML file -- ignore this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="151"/>
        <source>Overwrite connection %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="178"/>
        <source>can&apos;t open database set file &apos;%1&apos; for writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/db_connection_set.cpp" line="181"/>
        <source>write database set to &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbConnectionStatus</name>
    <message>
        <location filename="../../src/db/dbconnectionstatus.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.ui" line="66"/>
        <source>Icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.ui" line="81"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.cpp" line="15"/>
        <source>Successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/db/dbconnectionstatus.cpp" line="23"/>
        <source>Connection Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DbEncoding</name>
    <message>
        <location filename="../../src/db/dbencoding.cpp" line="40"/>
        <source>encoding &apos;%1&apos; is not supported should be one of (%2) -- using system</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <location filename="../../src/editdialog.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editdialog.ui" line="64"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editdialog.ui" line="71"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditWidget</name>
    <message>
        <location filename="../../src/editwidget.ui" line="17"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="56"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="69"/>
        <source>TextWrap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="82"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="89"/>
        <source>Synchronize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="118"/>
        <source>Print Pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="131"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="141"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="157"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="287"/>
        <source>Line:Column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.ui" line="324"/>
        <source>The current file name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="101"/>
        <source>Fileinfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="102"/>
        <source>File &apos;%1&apos; not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="243"/>
        <location filename="../../src/editwidget.cpp" line="382"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="244"/>
        <source>Cannot write file %1:
%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editwidget.cpp" line="383"/>
        <source>The document &apos;%1&apos; has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LookupList</name>
    <message>
        <location filename="../../src/lookuplist.cpp" line="16"/>
        <source>insert key(%1)=&apos;%2&apos; into lookup list &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ParameterDialog</name>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="23"/>
        <source>Edit Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="55"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="132"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="157"/>
        <source>Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="170"/>
        <source>filter by Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="186"/>
        <source>Add Values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="218"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.ui" line="231"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/parameter/parameterdialog.cpp" line="69"/>
        <source>Query &apos;%1&apos; connected with &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/main.cpp" line="20"/>
        <source>start application &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="21"/>
        <source>build with Qt %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="38"/>
        <source>library path: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="44"/>
        <source>style: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="50"/>
        <source>sql driver plugin: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="56"/>
        <source>odbc connection: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="61"/>
        <source>QSettings storage place: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="82"/>
        <source>SqlReport for command line usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="85"/>
        <source>database connection XML file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="86"/>
        <source>queryset XML file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="87"/>
        <source>query to execute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="88"/>
        <source>name of the used database connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/main.cpp" line="90"/>
        <source>optional parameter &quot;key:=value&quot; pairs, can be used multiple times</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QueryExecutor</name>
    <message>
        <location filename="../../src/query_executor.cpp" line="90"/>
        <source>can&apos;t open %1 file &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="111"/>
        <source>can&apos;t find include file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="115"/>
        <source>cyclic include found at %1 &lt; %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="161"/>
        <source>ARG_%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="228"/>
        <source>it looks you forgot the empty the description part (two commas) %1,,%2 -- execute anyway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="325"/>
        <source>No RTF String found -- use given string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="351"/>
        <source>Please input value for the user variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="576"/>
        <source>can&apos;t convert &apos;%1&apos; to a timestamp number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="589"/>
        <source>%1 modifier need an ouput date format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="610"/>
        <source>the string &apos;%1&apos; dosen&apos;t match the date format &apos;%2&apos; for &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="625"/>
        <source>Not supported variable conversion &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="681"/>
        <source>lookup list &apos;%1&apos; exists with %2 %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="685"/>
        <source>no lookup list with name &apos;%1&apos; found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="697"/>
        <source>to few parameters expected &apos;__insert,&lt;listname&gt;,&lt;key&gt;,&lt;value&gt;&apos; but given &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="704"/>
        <source>to few parameters expected &apos;__get,&lt;listname&gt;,&lt;key&gt;&apos; but given &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="713"/>
        <source>lookup list with name &apos;%1&apos; doesn&apos;t exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="722"/>
        <source>missing lookup key parameter &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="737"/>
        <source>missing lookup value parameter &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="789"/>
        <source>wrong &apos;%1&apos; only __COUNTER or __COUNTERH is supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="794"/>
        <source>global variable %1 was removed! Replace with __COUNTER.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="849"/>
        <source>unsupported global command &apos;%1&apos; found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="933"/>
        <source>please use &apos;%1&apos; as identifier for the Javascript block in %2 file (&apos;%3&apos; was used)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="950"/>
        <source>the database specific SQL for %1 has no driver avilable in the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="957"/>
        <source>the SQL &apos;%1&apos; was added before and is ignored (definition existst multiple times)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="972"/>
        <source>the template &apos;%1&apos; was added before and is ignored (definition existst multiple times)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="979"/>
        <source>adding Template-Block &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="995"/>
        <source>application error unknown map type category &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1141"/>
        <source>Overwrite %1 &apos;%2&apos; at line %3 in %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1151"/>
        <source>Missing block for %1 at line %2 in %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1231"/>
        <source>javascript error &apos;%1&apos; at line %2 in %3 file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1240"/>
        <source>%1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2159"/>
        <source>can&apos;t execute batch query &apos;%1&apos; using database &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2170"/>
        <source>Batch Execution File &apos;%1&apos; doesn&apos;t exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2204"/>
        <source>batch execution time: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1265"/>
        <source>create path %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1272"/>
        <source>Can&apos;t open file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1288"/>
        <source>generated file (%1) &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1318"/>
        <source>add parameter &apos;%1&apos; with value &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1324"/>
        <source>overwrite parameter %1 := &apos;%2&apos; with new value &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1348"/>
        <source>sqlReport interprets &lt;b&gt;&apos;%1&apos;&lt;/b&gt; as &lt;b&gt;&apos;eval&apos;&lt;/b&gt;, please remove the surrounding whitspaces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1383"/>
        <source>error &apos;%1&apos; at line %2 evaluate script /%3/ at line %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1438"/>
        <source>unknwon parameter &apos;%1&apos; used at &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1480"/>
        <source>using pipelining at line %2 with an existing variable &apos;$%1&apos;, please rename this</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1605"/>
        <source>error &apos;%1&apos; at line %2 evaluate script /%3/ at template line %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1690"/>
        <source>Unknown QuerySet &apos;%1&apos; at line %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1831"/>
        <source>no ending double quote in argument list of ##%1##</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1929"/>
        <source>no parameter &apos;%1&apos; found for SPLIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1945"/>
        <source>parameter &apos;%1&apos; doesn&apos;t exists at current replacements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1949"/>
        <source>ignore empty list &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1957"/>
        <source>output template &apos;%1&apos; from list &apos;%2&apos; split by &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="1964"/>
        <source>output template &apos;%1&apos; using query &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2017"/>
        <source>[%1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2046"/>
        <source>template %1 isn&apos;t defined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2078"/>
        <source>stop execution --&gt;  missing database for an existing SQL file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2123"/>
        <source>summary time: %1; executing %2 %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2126"/>
        <source>query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2126"/>
        <source>queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_executor.cpp" line="2131"/>
        <source>found some errors during execution</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuerySet</name>
    <message>
        <location filename="../../src/query_set.cpp" line="160"/>
        <source>reading node %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="164"/>
        <source>wrong or empty query set XML file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="170"/>
        <source>root node in XML must be &apos;SQLREPORT&apos; but is &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="179"/>
        <source>read query set file &apos;%1&apos; version %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="186"/>
        <source>sqlreport child %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="191"/>
        <source>queryset node %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="265"/>
        <source>please check, you read a database file as queryset file -- ignoring</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="303"/>
        <source>can&apos;t open query set file &apos;%1&apos; for writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/query_set.cpp" line="306"/>
        <source>write query set to &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecentComboBox</name>
    <message>
        <location filename="../../src/recentcombobox.cpp" line="39"/>
        <source>query set &apos;%1&apos; not exists --&gt; remove from list</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResultSetList</name>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="10"/>
        <source>split list &apos;%1&apos; at &apos;%2&apos; accessible via &apos;%3&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="17"/>
        <source>compare &apos;%1&apos; with &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="34"/>
        <source>empty separator given using comma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resultsetlist.cpp" line="77"/>
        <source>set list value to &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResultSetSql</name>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="28"/>
        <source>SQL-Query: %1%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="46"/>
        <source>## error executing %1 ## %2 ##</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="105"/>
        <source>can&apos;t convert &apos;%1&apos; to QString use binary data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/resultsetsql.cpp" line="130"/>
        <source>column[%1]:%2 %3 = /%4/ len(%5)=0x%6 </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SqlReport</name>
    <message>
        <location filename="../../src/sql_report.ui" line="19"/>
        <source>SqlReport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="691"/>
        <location filename="../../src/sql_report.cpp" line="30"/>
        <source>Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="449"/>
        <location filename="../../src/sql_report.ui" line="1415"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="84"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Format&lt;/span&gt;: param1:=value1|param2:=value2...&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Usage&lt;/span&gt;: ${?param1}&lt;/p&gt;&lt;p&gt;This local parameters are &lt;span style=&quot; font-weight:600;&quot;&gt;not&lt;/span&gt; stored in the query set file. The can be used to overwrite all other parameters (i.e. query and database) for some cases (e.g. local paths outside of query definition).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="873"/>
        <location filename="../../src/sql_report.ui" line="1328"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="609"/>
        <location filename="../../src/sql_report.cpp" line="29"/>
        <source>SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="74"/>
        <location filename="../../src/sql_report.ui" line="544"/>
        <location filename="../../src/sql_report.ui" line="1493"/>
        <source>Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="161"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;Queries&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="226"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Delete the selected file from the recent queries list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="259"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;right&quot;&gt;&lt;span style=&quot; font-weight:700; color:#ff9300;&quot;&gt;Query&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="517"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display and Edit the Query Description.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="664"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the connected SQL file for editing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="746"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Open the connected Template file for editing.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="837"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Display the created result. This is for the latest result available only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="919"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the selected Query after loading the Query Set.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="922"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="935"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked the gererated output file is used as input for the process. Each line has the form&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;!!&amp;lt;Query Name&amp;gt;!!Param1:=Value1!!Param2:=Value2&lt;/span&gt;&lt;/p&gt;&lt;p&gt;with each parameter optional. See documentation.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="938"/>
        <source>Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="957"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked the generated output is added to an existing file, otherwise the file is rewritten. This is useful ic you have multiple queries which are executed as batch process and should build one result file.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="960"/>
        <source>Append</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="973"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Add timestamp to generated output file. The format is &apos;yyyy-mm-dd-modifier-test-HHMM.txt&apos; if the output filename is modifier-test.txt.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="976"/>
        <source>Timestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="989"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The generated output file has Latin1 encoding, the default encoding is UTF-8 without BOM.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="992"/>
        <source>Latin1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1005"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The requested output format is XML. In this case in all incoming strings the characters &amp;amp;,&amp;lt;,&amp;gt; and double quotes are replaced with the long representation (i.e. &amp;amp;amp; for &amp;amp;) &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1008"/>
        <source>XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1021"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If the variable &lt;span style=&quot; font-weight:700;&quot;&gt;${XYZ}&lt;/span&gt; not exists, normally the message  [&apos;XYZ&apos; is unknown] is added to the ouput. If this is set, nothing is added to the output, the error message is shown in the &lt;span style=&quot; font-weight:700;&quot;&gt;More ...&lt;/span&gt; textbox only.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1024"/>
        <source>Suppress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1031"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If this is checked all values from the database which are NULL are replaced with empty string.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1034"/>
        <source>NULL as empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1047"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The maximal allowed recursion depth for this query, can be used to stop recursive templated calls.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1050"/>
        <source>10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1063"/>
        <source>Max. Recursion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1089"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Switch between &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Info&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Debug&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;/&lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Trace&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; output level.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1092"/>
        <source>Debug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="416"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Edit the selected database connection.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1114"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:700;&quot;&gt;Messages&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="590"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Format&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt; param1:=value1|param2:=value2...&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Usage&lt;/span&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;: ${?param1}&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;This parameters are used to set input parameter for the query, this values can be overwritten by the local parameter which are not stored in the query set file.&lt;br/&gt;&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="773"/>
        <location filename="../../src/sql_report.cpp" line="31"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="489"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Some useful informations describing the query.&lt;/p&gt;&lt;p&gt;This can be stored in AsciiDoc format and can later be used to generate a documentation.&lt;/p&gt;&lt;p&gt;This line show the first characters of the beginning of the text with the [=]* header removed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="345"/>
        <location filename="../../src/sql_report.ui" line="1318"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1380"/>
        <location filename="../../src/sql_report.ui" line="1395"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="314"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;A query is defined by some properties including file names. By clicking &amp;quot;Edit&amp;quot; the current query can be renamed or duplicated in a new query.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1375"/>
        <location filename="../../src/sql_report.ui" line="1390"/>
        <location filename="../../src/sql_report.ui" line="1420"/>
        <location filename="../../src/sql_report.ui" line="1445"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1193"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This button stop the current execution.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1196"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1218"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
hr { height: 1px; border-width: 0; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;Press these button to the processing. At this time we create the database connection and parse the template file starting with the &lt;/span&gt;&lt;span style=&quot; font-weight:700; color:#000000;&quot;&gt;::MAIN&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;If the active query set is BATCH we execute the generated query statements atarting with double exclamation mark.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1227"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1308"/>
        <source>Query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1332"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1343"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1355"/>
        <source>Queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1405"/>
        <source>Remove from List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1410"/>
        <source>Edit Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1430"/>
        <location filename="../../src/sql_report.ui" line="1450"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1435"/>
        <source>Load SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1440"/>
        <source>Load Template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1460"/>
        <source>Key Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1478"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1488"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1498"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1503"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="96"/>
        <location filename="../../src/sql_report.ui" line="1347"/>
        <source>Databases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="32"/>
        <source>Query Set Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="87"/>
        <source>missing databases file, please select one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="96"/>
        <source>missing queries file, please select one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1400"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1455"/>
        <source>Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1385"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="198"/>
        <source>Select QuerySet file to save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="200"/>
        <location filename="../../src/sql_report.cpp" line="223"/>
        <location filename="../../src/sql_report.cpp" line="588"/>
        <location filename="../../src/sql_report.cpp" line="1248"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="221"/>
        <source>Select DatabaseSet file to save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="334"/>
        <source>Unknown QuerySet &apos;%1&apos; at line %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="535"/>
        <source>Remove database connection &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="586"/>
        <source>Please select new QuerySet file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="604"/>
        <source>Please select QuerySet file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="606"/>
        <source>XML-Files (*.xml);;All Files(*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="624"/>
        <source>query file &apos;%1&apos; no longer exists -&gt; remove from recent queries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="640"/>
        <source>%1.lockfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="649"/>
        <source>&apos;%3&apos; is locked by %1 at %2 (Attention: information only, no action follows)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="772"/>
        <source>* Ausgabedatei `%1`%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="774"/>
        <source> wird erweitert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="774"/>
        <source> wird erstellt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="852"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="868"/>
        <source>Missing Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="868"/>
        <source>The entry need a name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="895"/>
        <source>No entry selected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="895"/>
        <source>You can rename an existing query only.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="901"/>
        <source>New Query Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="929"/>
        <source>Remove query &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="950"/>
        <source>there is no active query, which can removed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="958"/>
        <source>You have to set a query to get the command line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="965"/>
        <source>execute this query using the command line </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="966"/>
        <source>%1 %2 %3 %4 %5 {-p key:=value}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1077"/>
        <source>get %1database structure ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="668"/>
        <source>migrate &apos;%1&apos; to configuration version 3 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="669"/>
        <source>create database set file %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="723"/>
        <source>Can&apos;t open file &apos;%1&apos; for writing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="776"/>
        <source>* `%1` enthält die SQL Anweisungen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="777"/>
        <source>* `%1` enthält die Musterausgabe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="779"/>
        <source>* Standardabfrage bei Programmstart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="781"/>
        <source>* Batch zur Ausführungen weiterer Anweisungen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="783"/>
        <source>* Encoding der Ausgabe ist Latin1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="785"/>
        <source>* Ausgabe als XML, String werden automatisch gequotet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="787"/>
        <source>* Dateiname bekommt einen Zeitstempel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="789"/>
        <source>* Unbekannte Variablen erzeugen Fehler in der Ausgabe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="791"/>
        <source>* NULL wird als leere Zeichenkette verwendet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="793"/>
        <source>* Rekursionstiefe auf %1 festgelegt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="799"/>
        <source>Readme generated at %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1468"/>
        <source>Development Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.ui" line="1425"/>
        <location filename="../../src/sql_report.cpp" line="902"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="851"/>
        <source>Set Query Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="861"/>
        <location filename="../../src/sql_report.cpp" line="911"/>
        <source>Entry exists!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="862"/>
        <location filename="../../src/sql_report.cpp" line="912"/>
        <source>The entry &apos;%1&apos; exists in the set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="930"/>
        <source>Used files are not removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1001"/>
        <location filename="../../src/sql_report.cpp" line="1027"/>
        <source>Missing File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1002"/>
        <location filename="../../src/sql_report.cpp" line="1028"/>
        <source>Please select a file or give a name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1051"/>
        <source>Missing or Outdated File!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1052"/>
        <source>Please create output first (Start)
(Uncheck batch checkbox to see the result.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1081"/>
        <source>ready execution time: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1161"/>
        <source>No active query entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1162"/>
        <source>Please select a query or create a new.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1234"/>
        <source>XML-Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1246"/>
        <source>Please select new Databases file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1256"/>
        <source>selecting the same database again -- do nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1349"/>
        <source>export using base path &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1376"/>
        <source>can&apos;t open export file &apos;%1&apos; for query &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1411"/>
        <source>missing %1 -- stop export.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1417"/>
        <source>Please select Output Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1451"/>
        <source>export &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1454"/>
        <source>files of Query Set exported to &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1460"/>
        <source>Language Switch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sql_report.cpp" line="1461"/>
        <source>Please restart the application to activate the new language.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SqlReportCli</name>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="36"/>
        <source>the queryset has no query &apos;%1&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="39"/>
        <source>possible queries are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="43"/>
        <source>find and activate query &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="53"/>
        <source>read database connection from separate file &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="61"/>
        <source>the queryset has no database connection &apos;%1&apos; defined.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="65"/>
        <source>for queryset files with version greater than 3 is a separate database file required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="74"/>
        <source>possible databases are:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="78"/>
        <source>find and activate database connection &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/sqlreportcli.cpp" line="98"/>
        <source> - &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
