#pragma once

#include "resultset.h"

class ResultSetSingle : public ResultSet
{
    Q_OBJECT

public:
    ResultSetSingle();

    virtual ~ResultSetSingle() override {}

    virtual QString name() override { return "Singleton Result"; };
    virtual QString lastError() override { return ""; }
    virtual bool updateParameter(QHash <QString, QByteArray> &replacements,
                                 QuerySetEntry *mQSE) override;

private:
    bool first_strike;
};

