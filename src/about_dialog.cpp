#include "about_dialog.h"
#include "ui_about_dialog.h"

AboutDialog::AboutDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AboutDialog)
{
    ui->setupUi(this);
}

void AboutDialog::setVersion(const QString &version)
{
    ui->version->setText(version);
}

AboutDialog::~AboutDialog()
{
    delete ui;
}
