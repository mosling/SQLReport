#include "resultsetsingle.h"

ResultSetSingle::ResultSetSingle()
    :ResultSet()
    ,first_strike(true)
{
    this->setValid(true);
}


bool ResultSetSingle::updateParameter(QHash<QString, QByteArray> &replacements, QuerySetEntry *mQSE)
{
    Q_UNUSED(replacements)
    Q_UNUSED(mQSE)

    if (first_strike)
    {
        first_strike = false;
        return true;
    }

    return false;
}
