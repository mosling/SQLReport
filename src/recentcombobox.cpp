#include "recentcombobox.h"

#include <QLineEdit>
#include <QFileInfo>

RecentComboBox::RecentComboBox(QWidget *parent, int maxEntries)
    :QComboBox(parent)
    , logger(LogMessage::instance())
    , maxEntries(maxEntries)
    , updateModel(false)
{
    QObject::connect(this, &RecentComboBox::currentIndexChanged, this, &RecentComboBox::indexChanged);
    if (this->lineEdit() != nullptr)
    {
        this->setEditable(false);
        QObject::connect(this->lineEdit(), &QLineEdit::returnPressed, this, &RecentComboBox::updateItem);
    }

    this->setModel(&recent_list);
}

//!
//! \brief RecentComboBox::setList
//! \param list a QStringList contains the elements for the RecentComboBox
//!
void RecentComboBox::setList(const QStringList &list)
{
    QStringList l;

    foreach (QString fn, list)
    {
        QFileInfo fni(fn);
        if ( fni.isFile())
        {
            l.append(fn);
        }
        else
        {
            logger.warnMsg(tr("query set '%1' not exists --> remove from list")
                               .arg(fn));
        }
    }
    recent_list.setStringList(l);
}

const QStringList RecentComboBox::getList()
{
    return recent_list.stringList();
}

void RecentComboBox::addToList(QString &entry)
{
    updateRecentList(entry);
}

void RecentComboBox::removeFirstElement()
{
    recent_list.removeRow(0);
}

void RecentComboBox::indexChanged(int index)
{
    if (index == 0)
    {
        // this is called if the first entry changes by add, read, remove and select
        emit recentEntryChanged(this->currentText());
    }
    else
    {
        updateRecentList(this->currentText());
    }
}

//!
//! \brief RecentComboBox::updateItem
//!
//! This is called if the user press enter/return in the combobox, this is needed to
//! commit the change of the combobox used line edit field.
void RecentComboBox::updateItem()
{
    recent_list.setData(recent_list.index(0), this->currentText());
    emit recentEntryChanged(this->currentText());
}

//!
//! \brief RecentComboBox::updateRecentList
//! \param item the active item
//!
//! This method do multiple things with the given item
//! * if not in list add it to list and bring it to front
//! * if in list bring it to front
//! * delete entries if maxEntries is reached
void RecentComboBox::updateRecentList(const QString &item)
{
    // using QStringListModel by rewrite the corresponding string list
    // 1. if not in model, insert at position 0 and show
    // 2. if in model, move to position 0

    if (item.isEmpty())
    {
        // never add or select an empty item to the recent list
        return;
    }

    qsizetype row = recent_list.stringList().indexOf(item);
    if ( 0 == row )
    {
        // item is at first position
        return;
    }

    QStringList rl = recent_list.stringList();
    if (rl.contains(item))
    {
        rl.move(row, 0);
    }
    else
    {
        rl.insert(0, item);
        while (rl.size() > maxEntries)
        {
            rl.removeLast();
        }
    }

    recent_list.setStringList(rl);

    // notify the signal currentIndexChanged(int index)
    this->setCurrentIndex(0);

    return;
}
