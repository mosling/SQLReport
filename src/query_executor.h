#pragma once

#include <QObject>
#include "QtCore/qobject.h"
#include "QtCore/qstring.h"
#include "commonjavascript.h"
#include "logmessage.h"
#include "lookuplist.h"
#include "db_connection.h"
#include "resultset.h"
#include "query_set.h"
#include <QDateTime>
#include <QFile>
#include <QtSql/QtSql>
#include <QTextStream>
#include <QStringList>

#include <QDebug>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlQuery>
#include <QtQml/QJSEngine>

#include <QTextEdit>
#include <qsettings.h>

class QueryExecutor : public QObject
{
	Q_OBJECT
	Q_CLASSINFO ("author", "St. Koehler")
	Q_CLASSINFO ("company", "com.github.mosling")

public:
    explicit QueryExecutor(QObject *parentObj);
    virtual ~QueryExecutor() override;

	bool createOutput(QuerySetEntry *aQSE, DbConnection *dbc,
					  const QString &basePath, const QString &inputDefines);
    bool createBatchOuput(QuerySet &batchQuerySet,
                          DbConnectionSet &batchDatabaseSet,
                          QuerySetEntry *aQSE,
                          const QString &localParamter);
    QString expandString(const QString &str, const QHash <QString, QByteArray> &parameter,
                         bool simpleFormat);

public slots:
    void stopProcess() { stopPressed = true; }

protected:
    void replaceTemplate(const QStringList &aTemplLines, int aLineCnt);
    QString replaceLine(const QString &aLine, int aLineCnt);
    QString replaceString(const QString &aLine, QString encoding);
    bool outputTemplate(const QString &templateStr);
	QString getDate(const QString &aFormat) const;
	void clearStructures();

private:
    bool readFileToList( const QString &category, const QString &basePath,
                         const QString &filename, QStringList &lines, QStringList &file_list);
    QByteArray getParameter(const QString &name, bool &bOk);
    void setArgumentsToResultSet(QStringList &argument_list, ResultSet *resultSet);
    QString replaceEvaluation(QStringList &tmpList, int aLineCnt);
    QString replaceLineUserInput(QStringList &varList);
    bool checkForModifier(const QStringList &varList);
    QString replaceLineVariable(const QByteArray &vStr, const QStringList &varList);
    QString replaceLineGlobal(const QStringList &varList, int currentLineLength, int lineCnt);
	void showDbError(QString vErrStr);
	void createOutputFileName(const QString &basePath);
	void createInputFileNames(const QString &basePath);
    bool parseInputFile(const QString &category, const QString &basePath,
                        const QString &fileName, bool preserve_empty_lines);
    bool executeFileInputOutput(const QString &basePath);
    void setUserParameter(const QString &parameterList);
    void addMapEntry(const QString &category, const QString &name, const QStringList &lines);
    quint32 addActiveTemplate(QString templateName);
    void removeActiveTemplate(QString templateName);
    QString showReplacements(QHash<QString, QByteArray> &variables, const QString &format);
    QString getIndentationString(quint32 addIndent);
    void writeToStream(const QString &str);
    quint32 executeScriptCondition(const QString &templateName, const QString &condition, int lineCnt);
    void updateTreeStorage(const QString &levelName);
    bool executeBatchLine(
        QuerySet &batchQuerySet,
        DbConnectionSet &batchDatabaseSet,
        DbConnection *batchDb,
        QString queryPath,
        const QString &baseInput,
        QString commandLine,
        quint32 cntCommands,
        quint32 lineNr,
        quint32 queryNr);

    LogMessage &logger;
	QuerySetEntry *mQSE;
    QHash <QString, QString> userParameter;
    QHash <QString, QByteArray> replacements;
    QHash <QString, QByteArray> treeReplacements;
    QStringList treeLevelList;
	QMap <QString, quint32> cumulationMap;
	QMap <QString, QString> queriesMap;
    QMap <QString, QStringList> templatesMap;
	QString sqlFileName;
    QString templateFileName;
    QString databaseType;
    QJSEngine scriptEngine;

	QFile fileOut;
	QTextStream streamOut;

	int uniqueId;
	bool firstQueryResult;
	QString currentTemplateBlockName;
    QRegularExpression templateCall;  // all expressions like #{...} capture the ... part
    QRegularExpression singleVariable; // all parts with $foo or $?bar
    QRegularExpression modificationVariable; // all parts ${var,modifier|modifier}
    QRegularExpression paramAssign;
    quint32 sqlQueryCount;
    quint32 lastTabulator;
    DbConnection *currentDbConnection;
    QHash <QString, quint32> activeTemplates;
    qint32 indentation;
    bool stopPressed;
    LookupList lookup_list;
    CommonJavascript common_javascript;
};
