#include "resultsetlist.h"

ResultSetList::ResultSetList(const QByteArray &listStr, const QString &separator, const QString &pname)
    : ResultSet(),
      logger(LogMessage::instance()),
      listIndex(0),
      fieldName(pname)

{
    logger.debugMsg(tr("split list '%1' at '%2' accessible via '%3'").arg(listStr, separator, pname));

    QChar schar;
    bool found = false;
    QMapIterator<QString, QChar> i(sepMap);
    while (i.hasNext() && !found) {
        i.next();
        logger.debugMsg(tr("compare '%1' with '%2'").arg(separator,i.key()));
        if (separator == i.key())
        {
            logger.debugMsg("match");
            schar = i.value();
            found = true;
        }
    }

    if (!found && separator.size() > 0)
    {
        schar = separator.at(0);
        found = true;
    }

    if (!found)
    {
        logger.warnMsg(tr("empty separator given using comma"));
        schar = ',';
    }


    resultList = listStr.split(schar.toLatin1());
    setValid(resultList.size() > 0);
}


ResultSetList::~ResultSetList()
{
    if (isValid())
    {
        resultList.clear();
        setValid(false);
    }
}

QString ResultSetList::lastError()
{
    return "";
}

bool ResultSetList::updateParameter(QHash <QString, QByteArray> &replacements, QuerySetEntry *mQSE)
{
    Q_UNUSED(mQSE)

    if (!isValid() || listIndex >= resultList.size())
    {
        return false;
    }

    setEmptySet(false);

    if (listIndex == 0)
    {
        saveParameter(replacements, fieldName);
    }

    // set the replacement for the current field name
    if (logger.isDebug())
    {
        logger.debugMsg(tr("set list value to '%1'").arg(resultList.at(listIndex)));
    }
    replacements[fieldName] = resultList.at(listIndex);
    listIndex++;

    return true;
}
