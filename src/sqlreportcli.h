#pragma once

#include "query_executor.h"
#include "query_set.h"
#include "logmessage.h"

#include <QString>
#include <QObject>


class SqlReportCli : public QObject
{
    Q_OBJECT

public:
    SqlReportCli(QObject *parent = nullptr);
    ~SqlReportCli();

    void showUsage();
    bool execute(QString &queryset, QString &query, QString &database,
                 QString &dbfile, const QStringList &params);

private:
    void displayList(QString title, QStringList &strList);
    bool start_execution(QuerySetEntry *activeQuerySetEntry, DbConnection *dbcon);
    bool executeBatchLine(QueryExecutor *vpExecutor,
                          DbConnection *batchDb,
                          QString queryPath,
                          QString commandLine,
                          quint32 cntCommands, quint32 lineNr, quint32 queryNr);


    LogMessage &logger;
    QuerySet querySet;
    DbConnectionSet databaseSet;
    QString localParameter;

};
