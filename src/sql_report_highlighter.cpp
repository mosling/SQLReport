#include "sql_report_highlighter.h"
#include <QRegularExpression>
#include <QStyleHints>

SqlReportHighlighter::SqlReportHighlighter(QTextDocument *parentObj)
	: QSyntaxHighlighter(parentObj),
	  highlightingRules(),
      commentStartExpression(QRegularExpression("/\\*")),
      commentEndExpression(QRegularExpression("\\*/")),
      singleLineCommentFormat(),
	  multiLineCommentFormat(),
	  keywordFormat(),
	  blockFormat(),
	  substitutionFormat(),
      templateFormat(),
      includeFormat()
{
	HighlightingRule rule;

    if (QGuiApplication::styleHints()->colorScheme() == Qt::ColorScheme::Dark)
    {
        keywordBrush = QColor("lightskyblue");
        substitutionBrush = QColor("orangered");
        templateBrush = QColor("lightCoral");
        singleLineCommentBrush = QColor("greenyellow");
        includeBrush = Qt::magenta;
        blockBrush = QColor("cornflowerblue");
        multiLineCommentBrush = Qt::green;
    }
    else
    {
        keywordBrush = Qt::darkBlue;
        substitutionBrush = Qt::red;
        templateBrush = Qt::darkMagenta;
        singleLineCommentBrush = Qt::darkGreen;
        includeBrush = Qt::magenta;
        blockBrush = Qt::darkMagenta;
        multiLineCommentBrush = Qt::green;
    }
    keywordFormat.setForeground(keywordBrush);
	keywordFormat.setFontWeight(QFont::Bold);
	QStringList keywordPatterns;
	keywordPatterns << "\\bUPPERCASE" << "\\bCAPITALIZE" << "\\bIFEMPTY"
					<< "\\bBOOL" << "\\bRMLF" << "\\bFMT" << "\\b__TAB"
					<< "\\bHEX" << "\\bTREEMODE" << "\\b__DATE\\b"
					<< "\\b__UNIQUEID\\b" << "\\bLINECNT\\b"
					<< "\\bselect\\b" << "\\bfrom\\b" << "\\bwhere\\b"
					<< "\\border by\\b" << "\\bis\\b" << "\\bNULL\\b"
					<< "\\band\\b" << "\\bor\\b" << "\\bnot\\b"
					<< "\\bunion\\b" << "\\blike\\b" << "\\bgroup by\\b"
					<< "\\bhaving\\b" << "\\bdistinct\\b" << "\\bin\\b"
					<< "\\bdistinct\\b" << "\\binner join\\b" << "\\bouter join\\b"
					<< "\\bleft join\\b" << "\\bright join\\b" << "\\bjoin\\b"
                    << "\\bcount\\b" << "\\bmin\\b" << "\\bmax\\b" << "\\bon\\b";
	foreach (const QString &pattern, keywordPatterns)
	{
        rule.pattern = QRegularExpression(pattern);
        rule.pattern.setPatternOptions(QRegularExpression::CaseInsensitiveOption);
		rule.format = keywordFormat;
		highlightingRules.append(rule);
	}

    substitutionFormat.setForeground(substitutionBrush);
    rule.pattern = QRegularExpression("\\$\\{[^\\}]*\\}");
	rule.format = substitutionFormat;
	highlightingRules.append(rule);

    templateFormat.setForeground(templateBrush);
    rule.pattern = QRegularExpression("\\#\\{[^\\}]*\\}");
	rule.format = templateFormat;
	highlightingRules.append(rule);

    singleLineCommentFormat.setForeground(singleLineCommentBrush);
    rule.pattern = QRegularExpression("^::#.*");
    rule.format = singleLineCommentFormat;
	highlightingRules.append(rule);

    includeFormat.setForeground(includeBrush);
    rule.pattern = QRegularExpression("^::<.*");
    rule.format = includeFormat;
    highlightingRules.append(rule);

    blockFormat.setForeground(blockBrush);
    blockFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegularExpression("^::[^#<].+");
    rule.format = blockFormat;
    highlightingRules.append(rule);

    multiLineCommentFormat.setForeground(multiLineCommentBrush);
}

void SqlReportHighlighter::highlightBlock(const QString &text)
{
	foreach (const HighlightingRule &rule, highlightingRules)
	{
        QRegularExpression expression(rule.pattern);
        QRegularExpressionMatchIterator i = expression.globalMatch(text);
        while (i.hasNext())
        {
          QRegularExpressionMatch match = i.next();
          setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
	}
}
