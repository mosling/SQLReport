#include "sqlreportcli.h"
#include "common.h"
#include "config.h"

#include <QFile>
#include <QFileInfo>
#include <QSettings>

SqlReportCli::SqlReportCli(QObject *parent)
    : QObject(parent),
      logger(LogMessage::instance()),
      querySet(),
      databaseSet(),
      localParameter("")
{

}

SqlReportCli::~SqlReportCli()
{

}

bool SqlReportCli::execute(QString &queryset, QString &query, QString &database,
                           QString &dbfile, const QStringList &params)
{
    bool result = false;

    // first read the query set file and suggest that we have an version
    // before 3 with database connections included
    if (this->querySet.readXml(queryset, this->databaseSet))
    {
        QuerySetEntry *activeQuerySetEntry = this->querySet.getByName(query);
        if (activeQuerySetEntry == nullptr)
        {
            logger.errorMsg(tr("the queryset has no query '%1' defined.").arg(query));
            QStringList pn;
            this->querySet.getNames(pn);
            displayList(tr("possible queries are:"), pn);
        }
        else
        {
            logger.infoMsg(tr("find and activate query '%1'").arg(query));
        }

        DbConnection *dbcon = databaseSet.getByName(database);
        // look at the existing connection from the query set file
        if (dbcon == nullptr)
        {
            // if not found and a databasefile is given, check this
            if (!dbfile.isEmpty())
            {
                logger.infoMsg(tr("read database connection from separate file '%1'").arg(dbfile));
                databaseSet.readXml(dbfile, false);
                dbcon = databaseSet.getByName(database);
            }
            else
            {
                if (this->querySet.getQuerySetVersion() <= 2)
                {
                    logger.errorMsg(tr("the queryset has no database connection '%1' defined.").arg(database));
                }
                else
                {
                    logger.errorMsg(tr("for queryset files with version greater than 3 is a separate database file required"));
                }
            }
        }

        if (dbcon == nullptr)
        {
            QStringList dn;
            databaseSet.getNames(dn);
            displayList(tr("possible databases are:"), dn);
        }
        else
        {
            logger.infoMsg(tr("find and activate database connection '%1'").arg(database));
        }

        if (activeQuerySetEntry != nullptr && dbcon != nullptr)
        {
            localParameter = params.join("|");
            result = start_execution(activeQuerySetEntry, dbcon);
        }
    }

    return result;
}

void SqlReportCli::displayList(QString title, QStringList &strList)
{
    if (strList.size() > 0)
    {
        logger.infoMsg(title);
        foreach (QString s, strList)
        {
            logger.infoMsg(tr(" - '%1'").arg(s));
        }
    }
}

bool SqlReportCli::start_execution(QuerySetEntry *activeQuerySetEntry, DbConnection *dbcon)
{

    bool result = false;

    auto queryExecutor = new QueryExecutor(this);

    QString parameterList = activeQuerySetEntry->getQueryParameter() + "|" + this->localParameter;


    if (activeQuerySetEntry->getBatchrun())
    {
        // we use the database from the command line
        activeQuerySetEntry->setDbName(dbcon->getName());

        result = queryExecutor->createBatchOuput(
            this->querySet,
            this->databaseSet,
            activeQuerySetEntry,
            parameterList);
    }
    else
    {
        QString queryPath = QFileInfo(this->querySet.getQuerySetFileName()).absolutePath();

        result = queryExecutor->createOutput(
            activeQuerySetEntry,
            dbcon,
            queryPath,
            parameterList
            );
    }


    delete queryExecutor;
    queryExecutor = nullptr;

    return result;
}
