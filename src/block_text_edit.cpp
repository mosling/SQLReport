#include "block_text_edit.h"
#include "qabstractitemview.h"
#include "qevent.h"

#include <QScrollBar>

BlockTextEdit::BlockTextEdit(QWidget *parent)
    :QTextEdit(parent)
{

}


BlockTextEdit::~BlockTextEdit()
{

}

void BlockTextEdit::setCompleter(QCompleter *completer)
{
    if (blockCompleter)
        blockCompleter->disconnect(this);

    blockCompleter = completer;

    if (!blockCompleter)
        return;

    blockCompleter->setWidget(this);
    blockCompleter->setCompletionMode(QCompleter::PopupCompletion);
    blockCompleter->setCaseSensitivity(Qt::CaseInsensitive);
    QObject::connect(blockCompleter, QOverload<const QString &>::of(&QCompleter::activated),
                     this, &BlockTextEdit::insertCompletion);
}

QCompleter *BlockTextEdit::completer() const
{
    return blockCompleter;
}

void BlockTextEdit::insertCompletion(const QString &completion)
{
    if (blockCompleter->widget() != this)
        return;

    QTextCursor tc = textCursor();
    // remove completionPrefix to replace ma with MAIN and not maIN only
    for (int i = 0; i < blockCompleter->completionPrefix().length(); ++i)
    {
        tc.deletePreviousChar();
    }
    tc.insertText(completion + "}");
    setTextCursor(tc);
}

QString BlockTextEdit::textUnderCursor() const
{
    QTextCursor tc = this->textCursor();
    int pos = tc.columnNumber();

    tc.select(QTextCursor::LineUnderCursor);
    QString line = tc.selectedText().left(pos);
    QString word = "";
    qsizetype l = line.size();
    // look backward for a character separates two elements
    // hope to find the hashtag
    static QString sow("#~!@$%^&*()+}|:\"<>?,/;'[]\\=\t ");
    for (qsizetype i = l - 1; i >= 0; i--)
    {
        if (sow.contains(line.at(i)))
        {
            word = line.right(l-i);
            if (word.startsWith("#{"))
            {
                word = line.right(l-i-2);
            }
            break;
        }
    }
    return word;
}

void BlockTextEdit::focusInEvent(QFocusEvent *e)
{
    if (blockCompleter)
        blockCompleter->setWidget(this);
    QTextEdit::focusInEvent(e);
}

void BlockTextEdit::keyPressEvent(QKeyEvent *e)
{
    if (blockCompleter && blockCompleter->popup()->isVisible()) {
        // The following keys are forwarded by the completer to the widget
        switch (e->key()) {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Escape:
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            e->ignore();
            return; // let the completer do default behavior
        default:
            break;
        }
    }

    const bool isShortcut = (e->modifiers().testFlag(Qt::ControlModifier) && e->key() == Qt::Key_E); // CTRL+E
    if (!blockCompleter || !isShortcut) // do not process the shortcut when we have a completer
        QTextEdit::keyPressEvent(e);

    const bool ctrlOrShift = e->modifiers().testFlag(Qt::ControlModifier) ||
                             e->modifiers().testFlag(Qt::ShiftModifier);

    if (!blockCompleter || (ctrlOrShift && e->text().isEmpty()))
        return;

    static QString eow("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word
    const bool hasModifier = (e->modifiers() != Qt::NoModifier) && !ctrlOrShift;
    QString completionPrefix = textUnderCursor();

    if (!isShortcut && (hasModifier || e->text().isEmpty()|| completionPrefix.length() < 2
                        || eow.contains(e->text().right(1)))) {
        blockCompleter->popup()->hide();
        return;
    }

    if (completionPrefix != blockCompleter->completionPrefix()) {
        blockCompleter->setCompletionPrefix(completionPrefix);
        blockCompleter->popup()->setCurrentIndex(blockCompleter->completionModel()->index(0, 0));
    }
    QRect cr = cursorRect();
    cr.setWidth(blockCompleter->popup()->sizeHintForColumn(0)
                + blockCompleter->popup()->verticalScrollBar()->sizeHint().width());
    blockCompleter->complete(cr); // popup it up!
}
