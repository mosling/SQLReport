#include "commonjavascript.h"

#include <QStringConverter>

CommonJavascript::CommonJavascript(QObject *parent)
    : QObject{parent}
{}

QString CommonJavascript::encodeBase64(QString text)
{
    return text.toUtf8().toBase64();
}

QString CommonJavascript::encodeLatin1Base64(QString text)
{
    auto fromUtf16 = QStringEncoder(QStringEncoder::Latin1);
    QByteArray encoded = fromUtf16(text);

    return encoded.toBase64();
}
