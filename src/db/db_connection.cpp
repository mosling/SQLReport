#include "db_connection.h"
#include "dbencoding.h"
#include "common.h"

#include <QCoreApplication>
#include <QDebug>
#include <QMessageBox>
#include <QInputDialog>
#include <QStringBuilder>
#include <QSqlQuery>
#include <QDir>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlField>
#include <QtSql/QSqlIndex>

DbConnection::DbConnection(QObject *parentObj) :
    QObject(parentObj),
    name(""),
    dbType(""),
    dbName(""),
    dbEncoding(""),
    dbOptions(""),
    databaseParameter(""),
    host(""),
    username(""),
    password(""),
    passwordSalt(""),
    port(0),
    passwordSave(false),
    stopPressed(false)
  ,connectionError("")
{
}

DbConnection::~DbConnection()
{
    closeDatabase();
    QSqlDatabase::removeDatabase(name);
}

void DbConnection::copyFrom(const DbConnection& rhs)
{
    if (this == &rhs){ return; }

    name = rhs.name;
    dbType = rhs.dbType;
    dbName = rhs.dbName;
    dbEncoding = rhs.dbEncoding;
    dbOptions = rhs.dbOptions;
    databaseParameter = rhs.databaseParameter;
    host = rhs.host;
    username = rhs.username;
    password = rhs.password;
    passwordSalt = rhs.passwordSalt;
    port = rhs.port;
    passwordSave = rhs.passwordSave;
    stopPressed = false;
}

void DbConnection::readXmlNode(const QDomNode &aNode, const qsizetype config_version)
{
    QDomNode cn = aNode.firstChild();        
    while(!cn.isNull())
    {
        QString ce = cn.toElement().tagName();
        qDebug() << "adding element " << ce;
        QString te = cn.firstChild().toText().data();
        ce = ce.toUpper();
        if(ce == "NAME")       { name = te; }
        if(ce == "DBNAME")     { dbName = te; }
        if(ce == "DBENCODING") { dbEncoding = te; }
        if(ce == "DBTEMPLATE") { dbTemplate = te; }
        if(ce == "DBOPTIONS")  { dbOptions = te; }
        if(ce == "DBPARAMETER"){ databaseParameter = te; }
        if(ce == "HOST")       { host = te; }
        if(ce == "USER")       { username = te; }
        if(ce == "PASS")
        {
            if (config_version >= 2)
            {
                password = Common::extractPassword(te, false);
                passwordSalt = Common::extractPassword(te, true);
            }
            else
            {
                password = te;
                passwordSalt = "";
            }
        }
        if(ce == "TYPE")       { dbType = te; }
        if(ce == "PORT")       { port = te.toUInt(); }
        if(ce == "SAVE")       { passwordSave = (te == "yes") ? true : false; }
        cn = cn.nextSibling();
    }

    if (dbEncoding.isEmpty())
    {
        dbEncoding = "SYSTEM";
    }

}

void DbConnection::writeXmlNode(QXmlStreamWriter &aStream)
{
    aStream.writeStartElement("Database");
    aStream.writeTextElement("type", dbType);
    aStream.writeTextElement("name", name);
    aStream.writeTextElement("dbparameter", databaseParameter);
    aStream.writeTextElement("dbtemplate", dbTemplate);
    aStream.writeTextElement("host", host);
    aStream.writeTextElement("port", QString("%1").arg(port));
    aStream.writeTextElement("dbname", dbName);
    aStream.writeTextElement("dbencoding", dbEncoding);
    aStream.writeTextElement("dboptions", dbOptions);
    aStream.writeTextElement("user", username);
    if(passwordSave)
    {
        aStream.writeTextElement("pass", Common::obscurePassword(password, passwordSalt));
    }
    aStream.writeTextElement("save", passwordSave ? "yes" : "no");
    aStream.writeEndElement();
}

void DbConnection::setName(const QString &value)
{
    name = value;
}

QString DbConnection::getDbInternalType() const
{
    return Common::mssqlName() == dbType ? "QODBC" : dbType;
}

void DbConnection::setDbType(const QString &value)
{
    dbType = value;
}

void DbConnection::setDbName(const QString &value)
{
    dbName = value;
}

void DbConnection::setDbEncoding(const QString &value)
{
    if (DbEncoding::getEncodings().contains(value))
    {
        dbEncoding = value;
    }
    else
    {
        qCritical() << tr("unknown encoding '%1' should be in (%2)")
                       .arg(value, DbEncoding::getEncodings().join(","));
    }
}

void DbConnection::setDbOptions(const QString &value)
{
    dbOptions = value;
}


void DbConnection::setUsername(const QString &value)
{
    username = value;
}

void DbConnection::setPassword(const QString &value)
{
    password = value;
}

void DbConnection::setPort(const quint32 &value)
{
    port = value;
}

void DbConnection::setPasswordSave(const bool value)
{
    passwordSave = value;
}

void DbConnection::setDbTemplate(const QString &value)
{
    dbTemplate = value;
}

QString DbConnection::lastDbError(bool showError)
{
    QSqlDatabase db = QSqlDatabase::database(name);

    if(db.lastError().isValid())
    {
        QString dberr = QByteArray(db.lastError().text().toLocal8Bit());
        QString em = tr("%1%2").arg(connectionError.isEmpty() ? "" : tr("%1 -- ").arg(connectionError), dberr);
        if (showError)
        {
            qCritical() << em;
        }
        return em;
    }

    return connectionError;
}

QString DbConnection::toString() const
{
    QSqlDatabase db = QSqlDatabase::database(name, false);

    QString cs = QString("%1://").arg(db.driverName());
    if (!db.hostName().isEmpty())
    {
        cs += db.hostName();
        if (db.port() > 0)
        {
            cs += QString(":%1/").arg(db.port());
        }
        cs += "/";
    }
    cs += db.databaseName();

    if (!db.userName().isEmpty())
    {
        cs += QString(" with credentials for '%1'").arg(db.userName());
    }
    else
    {
        cs += QString(" without credentials");
    }

    return cs;
}


QString DbConnection::getConnectionName(const QString &basePath, bool showInfo /* = true */) const
{
    QString tmpStr;
    QString msgStr;
    QString dbFilename("");
    bool needFile = false;

    if (!dbTemplate.isEmpty())
    {
        // template is given, use it
        tmpStr = dbTemplate;
        needFile = tmpStr.contains("${filename}");
    }
    else if (dbName.endsWith(".mdb") || dbName.endsWith(".accdb"))
    {
        // Access file
        tmpStr = "DRIVER={Microsoft Access Driver (*.mdb, *.accdb)};DSN='';DBQ=${filename}";
        msgStr = "MS Access file";
        needFile = true;
    }
    else if (dbName.endsWith(".xlsx"))
    {
        // Excel file
        tmpStr = "DRIVER={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};DBQ=${filename}";
        msgStr = "MS Excel file";
        needFile = true;
    }
    else if ( (dbType == "QIBASE" && ( host == "localhoat" || host == "127.0.0.1"))
              || dbType == "QSQLITE")
    {
        // SQLite file or local Firebird file
        tmpStr = "${filename}";
        msgStr = "Database File";
        needFile = true;
    }
    else if ( dbType == Common::mssqlName() )
    {
        // MSSQL using QODBC later
        tmpStr = "Driver={SQL Server};SERVER=${dbhost};DATABASE=${dbname}";
        msgStr = "MSSQL-Server";
    }
    else if ( (dbType == "QOCI"))
    {
        // Oracle Call Interface
        tmpStr = "(DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = ${dbhost})(PORT = ${dbport})))(CONNECT_DATA = (SERVICE_NAME = ${dbname})))";
        msgStr = "Oracle OCI connection";
    }
    else
    {
        tmpStr =  dbName;
    }

    if (needFile)
    {
        dbFilename = QDir::toNativeSeparators(Common::findFile(basePath, dbName));
    }

    tmpStr.replace("${dbname}", dbName);
    tmpStr.replace("${dbport}", QString("%1").arg(port));
    tmpStr.replace("${dbhost}", host);
    tmpStr.replace("${dbuser}", username);
    tmpStr.replace("${filename}", dbFilename);
    QString newDbName(tmpStr);
    newDbName.replace("${dbpassword}", password.isEmpty() ? "" : "*****");
    tmpStr.replace("${dbpassword}", password);

    if (showInfo)
    {
        qInfo() << tr("%2 [%1,%4] --> '%3'").arg(dbType, msgStr, newDbName, dbEncoding);
    }

    return tmpStr;
}

bool DbConnection::connectDatabase(const QString &basePath)
{
    qDebug() << tr("connect database '%1'").arg(name);
    connectionError = "";
    QString dbCurrentType = "";
    QString dbInternalType = getDbInternalType();

    if (QSqlDatabase::contains(name))
    {
        dbCurrentType = QSqlDatabase::database(name, false).driverName();
    }

    // add the database with name if the type has changed
    if(dbCurrentType != dbInternalType)
    {
        if(QSqlDatabase::contains(name))
        {
            closeDatabase();
            qDebug() << tr("remove database because driver changed from '%1' to '%2'").arg(dbCurrentType, dbInternalType);
            QSqlDatabase::removeDatabase(name);
        }
        qDebug() << tr("adding database type connection %1 for %2").arg(dbType, name);
        QSqlDatabase::addDatabase(dbInternalType, name);
    }

    QSqlDatabase db = QSqlDatabase::database(name, false); // not open the database connection

    if(!username.isEmpty() && !passwordSave && password.isEmpty())
    {
        // get Password
        bool ok;
        QString pwd = QInputDialog::getText(nullptr, "",
                                            QString("Database Password for '%1'").arg(username),
                                            QLineEdit::Password, "", &ok);
        if(ok)
        {
            setPassword(pwd);
        }
    }

    // set
    if(!dbName.isEmpty())
    {
        QString dbstr = getConnectionName(basePath);
        if (dbstr.isEmpty())
        {
            connectionError = tr("missing database file for '%1'").arg(dbName);
            return false;
        }
        db.setDatabaseName(dbstr);

        if (dbstr == dbName)
        {
            if(!host.isEmpty()) db.setHostName(host);
            if(port != 0) db.setPort(port);
        }
    }

    if(!username.isEmpty()) db.setUserName(username);
    if(!password.isEmpty()) db.setPassword(password);

    if (!dbOptions.isEmpty())
    {
        QStringList ol = dbOptions.split(QLatin1Char('|'), Qt::SkipEmptyParts);
        foreach(QString o, ol)
        {
            db.setConnectOptions(o);
        }
    }

    qDebug() << tr("open database '%1'").arg(toString());

    bool ok = db.open();
    if(ok != true)
    {
        lastDbError(true);
    }

    return ok;
}

void DbConnection::closeDatabase() const
{
    QSqlDatabase db = QSqlDatabase::database(name, false);
    if(db.isOpen())
    {
        qDebug() << tr("close database %1").arg(toString());
        db.close();
    }
}

QString DbConnection::getFieldString(const QSqlField field) const
{
    return QString("%2 %3 %4,%5 %6 '%7' %8 %9")
      .arg(field.name(), field.metaType().name())
      .arg(field.length())
      .arg(field.precision())
      .arg(field.isNull() ? "NULL" : "",
           field.defaultValue().toString(),
           field.isAutoValue() ? "auto" : "",
           field.requiredStatus() == QSqlField::Required ? "required"
           : (field.requiredStatus() == QSqlField::Optional ? "optional" : ""))
      ;
}

QStringList DbConnection::getTableList() const
{
    QStringList tabList;
    QString tabSql = "";

    if ("QOCI" == dbType)
    {
        tabSql = QString(
                    "select table_name from all_tables where owner='%1'"
                    ).arg(username);
    }

    if (!tabSql.isEmpty())
    {
        qInfo() << tr("[%1] use special SQL to get tables /%2/ ").arg(dbType, tabSql);

        QSqlQuery tabQuery;
        if (tabQuery.exec(tabSql))
        {
            while (tabQuery.next())
            {
                tabList << tabQuery.value(0).toString().trimmed();
            };
        }
        else
        {
            qCritical() << tabQuery.lastError().driverText();
        }
    }

    return tabList;

}

QStringList DbConnection::getForeignKeyList(const QString &tableName) const
{
    QStringList fkList;
    QString fkSql("");

    if ("QIBASE" == dbType)
    {
        fkSql = QString(
                    "SELECT rc.RDB$CONSTRAINT_NAME AS constraint_name," \
                    "       s.RDB$FIELD_NAME AS field_name, " \
                    "       i2.RDB$RELATION_NAME AS references_table,"\
                    "       s2.RDB$FIELD_NAME AS references_field"\
                    " FROM RDB$INDEX_SEGMENTS s"\
                    " LEFT JOIN RDB$INDICES i ON i.RDB$INDEX_NAME = s.RDB$INDEX_NAME"\
                    " LEFT JOIN RDB$RELATION_CONSTRAINTS rc ON rc.RDB$INDEX_NAME = s.RDB$INDEX_NAME"\
                    " LEFT JOIN RDB$REF_CONSTRAINTS refc ON rc.RDB$CONSTRAINT_NAME = refc.RDB$CONSTRAINT_NAME"\
                    " LEFT JOIN RDB$RELATION_CONSTRAINTS rc2 ON rc2.RDB$CONSTRAINT_NAME = refc.RDB$CONST_NAME_UQ"\
                    " LEFT JOIN RDB$INDICES i2 ON i2.RDB$INDEX_NAME = rc2.RDB$INDEX_NAME"\
                    " LEFT JOIN RDB$INDEX_SEGMENTS s2 ON i2.RDB$INDEX_NAME = s2.RDB$INDEX_NAME"\
                    " WHERE rc.RDB$CONSTRAINT_TYPE = 'FOREIGN KEY' AND i.RDB$RELATION_NAME = '%1'"\
                    " ORDER BY i.RDB$RELATION_NAME, s.RDB$FIELD_NAME").arg(tableName);
    }
    else if ("QOCI" == dbType)
    {
        fkSql = QString(
                    "select uc_r.constraint_name,"\
                    " listagg(ucc_r.column_name, ', '),"\
                    " uc_p.table_name,"\
                    " listagg(ucc_p.column_name, ', ')"\
                    " from user_constraints uc_r "\
                    " join user_cons_columns ucc_r on ucc_r.constraint_name = uc_r.constraint_name"\
                    " join user_constraints uc_p on uc_p.constraint_name = uc_r.r_constraint_name"\
                    " join user_cons_columns ucc_p on ucc_p.constraint_name = uc_p.constraint_name"\
                    " and ucc_p.position = ucc_r.position"\
                    " where uc_r.constraint_type = 'R' AND uc_r.TABLE_NAME = '%1'"\
                    " GROUP BY uc_r.constraint_name, uc_p.table_name"
                    ).arg(tableName);
    }


    if (!fkSql.isEmpty())
    {
        QSqlQuery fkQuery(QSqlDatabase::database(name));
        if (fkQuery.exec(fkSql))
        {
            while (fkQuery.next())
            {
                //add tree entry line to list
                //expected columns are [constraint name, field_name, references_table, references_field]
                fkList << QString("%1 | %2 --> %3(%4)")
                          .arg(fkQuery.value(0).toString().trimmed(),
                               fkQuery.value(1).toString().trimmed(),
                               fkQuery.value(2).toString().trimmed(),
                               fkQuery.value(3).toString().trimmed());
            }
        }
        else
        {
            qCritical() << fkQuery.lastError().driverText();
        }
    }
    else
    {
        qWarning() << tr("no foreign key readout method for database type %1").arg(dbType);
        fkList << Common::notImplemented();
    }

    return fkList;
}

QStringList DbConnection::getReferencedFieldList(const QString &relation,
                                                 const QString &primary_key) const
{
    QStringList refList;
    QString refSql("");

    if ("QIBASE" == dbType)
    {
        refSql = QString(
                    "SELECT rc.RDB$CONSTRAINT_NAME AS constraint_name,"\
                    " rc.RDB$RELATION_NAME AS relation_name,"\
                    " s.RDB$FIELD_NAME AS field_name,"\
                    " i2.RDB$RELATION_NAME AS references_table,"\
                    " s2.RDB$FIELD_NAME AS references_field"\
                    " FROM RDB$INDEX_SEGMENTS s"\
                    " JOIN RDB$INDICES i ON i.RDB$INDEX_NAME = s.RDB$INDEX_NAME"\
                    " JOIN RDB$RELATION_CONSTRAINTS rc ON rc.RDB$INDEX_NAME = s.RDB$INDEX_NAME"\
                    " JOIN RDB$REF_CONSTRAINTS refc ON rc.RDB$CONSTRAINT_NAME = refc.RDB$CONSTRAINT_NAME"\
                    " JOIN RDB$RELATION_CONSTRAINTS rc2 ON rc2.RDB$CONSTRAINT_NAME = refc.RDB$CONST_NAME_UQ"\
                    " JOIN RDB$INDICES i2 ON i2.RDB$INDEX_NAME = rc2.RDB$INDEX_NAME"\
                    " JOIN RDB$INDEX_SEGMENTS s2 ON i2.RDB$INDEX_NAME = s2.RDB$INDEX_NAME"\
                    " WHERE rc.RDB$CONSTRAINT_TYPE = 'FOREIGN KEY' "\
                    " AND i2.RDB$RELATION_NAME = '%1'"\
                    " AND s2.RDB$FIELD_NAME = '%2'"\
                    " ORDER BY i.RDB$RELATION_NAME, s.RDB$FIELD_NAME")
                .arg(relation.toUpper(), primary_key.toUpper());
    }
    else if ("QOCI" == dbType)
    {
        refSql = QString(
                    "select ucc_p.constraint_name, uc_r.table_name, ucc_r.column_name"\
                    " from user_constraints uc_r"\
                    " join user_cons_columns ucc_r on ucc_r.constraint_name = uc_r.constraint_name"\
                    " join user_constraints uc_p on uc_p.constraint_name = uc_r.r_constraint_name"\
                    " join user_cons_columns ucc_p on ucc_p.constraint_name = uc_p.constraint_name"\
                    " and ucc_p.position = ucc_r.position"\
                    " where uc_r.constraint_type = 'R' "\
                    " AND uc_p.TABLE_NAME = '%1'"\
                    " AND ucc_p.column_name = '%2'"
                  ).arg(relation.toUpper(), primary_key.toUpper());
    }

    if (!refSql.isEmpty())
    {
        QSqlQuery fkQuery(QSqlDatabase::database(name));
        if (fkQuery.exec(refSql))
        {
            while (fkQuery.next())
            {
                //add tree entry line to list
                //expected columns are [constraint name, relation_name, field_name]
                refList << QString("%1 | %2(%3)")
                          .arg(fkQuery.value(0).toString().trimmed(),
                               fkQuery.value(1).toString().trimmed(),
                               fkQuery.value(2).toString().trimmed());
            }
        }
        else
        {
            qCritical() << fkQuery.lastError().driverText();
        }
    }
    else
    {
        qWarning() << tr("no referenced field readout method for database type %1").arg(dbType);
        refList << QString(Common::notImplemented());
    }

    return refList;
}

void DbConnection::showTableList(QSql::TableType aType,
                                 QString aHead,
                                 bool withFk,
                                 QTreeReporter *treeReporter) const
{
    if (stopPressed)
    {
        return;
    }

    treeReporter->reportMsg(aHead);
    treeReporter->incReportLevel();
    bool withForeignKey = withFk;
    bool withReferences = withFk;


    QSqlDatabase db = QSqlDatabase::database(name);
    QStringList tableList;

    if (aType == QSql::Tables)
    {
        tableList = getTableList();
    }

    if (tableList.empty())
    {
        tableList = db.tables(aType);
    }
    tableList.sort(Qt::CaseInsensitive);

    foreach(QString tn, tableList)
    {
        QCoreApplication::processEvents();
        if (stopPressed)
        {
            // at this point the tree is in correct state
            treeReporter->reportMsg("stopped by user ...");
            break;
        }

        treeReporter->reportMsg(tn);
        treeReporter->incReportLevel();
        QSqlRecord tableRecord = db.record(tn);
        if(!tableRecord.isEmpty())
        {
            // Create a List with entries
            QStringList fieldList;
            for(int c = 0; c < tableRecord.count(); ++c)
            {
                fieldList << getFieldString(tableRecord.field(c));
            }

            fieldList.sort();
            foreach(QString f, fieldList)
            {
                treeReporter->reportMsg(f);
            }


            QSqlIndex index = db.primaryIndex(tn);
            if(!index.isEmpty())
            {
                treeReporter->reportMsg("primary key " + index.name());
                treeReporter->incReportLevel();
                for(int c = 0; c < index.count(); ++c)
                {
                    QSqlField f = index.field(c);
                    treeReporter->reportMsg(QString(" %1: %2")
                                            .arg(c)
                                            .arg(f.name())
                                           );
                    // add for each primary key the referenced fields
                    if (withReferences)
                    {
                        QStringList referencedFields = getReferencedFieldList(tn, f.name());
                        if (referencedFields.size() == 1 && referencedFields.first() == Common::notImplemented())
                        {
                            withReferences = false;
                            referencedFields.clear();
                        }

                        if (referencedFields.size() > 0)
                        {
                            treeReporter->incReportLevel();
                            foreach(QString s, referencedFields)
                            {
                                treeReporter->reportMsg(s);
                            }
                            treeReporter->decReportLevel();
                        }
                    }
                }
                treeReporter->decReportLevel();
            }

            if (withForeignKey)
            {
                QStringList foreignKeys = getForeignKeyList(tn);
                if (foreignKeys.size() == 1 && foreignKeys.first() == Common::notImplemented())
                {
                    withForeignKey = false;
                    foreignKeys.clear();
                }

                if (foreignKeys.size() > 0)
                {
                    treeReporter->reportMsg("foreign keys");
                    treeReporter->incReportLevel();
                    foreach(QString s, foreignKeys)
                    {
                        treeReporter->reportMsg(s);
                    }
                    treeReporter->decReportLevel();
                }
            }
        }
        treeReporter->decReportLevel();
    }
    treeReporter->decReportLevel();
}

void DbConnection::showDatabaseTables(QTreeReporter *tree_reporter, bool withFk)
{
    QSqlDatabase db = QSqlDatabase::database(name);
    stopPressed = false;

    tree_reporter->reportMsg(tr("<%1>%2").arg(dbName, withFk ? " with references" : ""));
    tree_reporter->incReportLevel();

    QString features[] = {
        "Transactions", "QuerySize", "BLOB", "Unicode",
        "PreparedQueries", "NamedPlaceHolders", "PositionalPlaceHolders",
        "LastInsertId", "BatchOperations", "SimpleLocking",
        "LowPrecisionNumbers", "EventNotification", "FinishQuery",
        "MultipleResultSets", ""
    };

    QString s;

    tree_reporter->reportMsg("Capabilities");
    tree_reporter->incReportLevel();

    for(int i = 0; !features[i].isEmpty(); ++i)
    {
        s = features[i] + ": ";
        bool b = db.driver()->hasFeature(static_cast<QSqlDriver::DriverFeature>(i));
        s += b ? "yes" : "no";
        tree_reporter->reportMsg(s);
    }
    tree_reporter->decReportLevel();

    showTableList(QSql::Tables, "List of Tables", withFk, tree_reporter);
    showTableList(QSql::Views, "List of Views", false, tree_reporter);
    showTableList(QSql::SystemTables, "List of System Tables", false, tree_reporter);
}
