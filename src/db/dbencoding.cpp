#include "dbencoding.h"

const QMap<QString, QStringConverter::Encoding> DbEncoding::encodingNameMap = {
    {"UTF-8", QStringConverter::Utf8},
    {"ISO-8859-1", QStringConverter::Latin1},
    {"LATIN1", QStringConverter::Latin1},
    {"UTF-16", QStringConverter::Utf16},
    {"UTF-16LE", QStringConverter::Utf16LE},
    {"UTF-16BE", QStringConverter::Utf16BE},
    {"UTF-16LE", QStringConverter::Utf16LE},
    {"UTF-32", QStringConverter::Utf32},
    {"UTF-32LE", QStringConverter::Utf32LE},
    {"UTF-32BE", QStringConverter::Utf32BE},
    {"SYSTEM", QStringConverter::System}
};

const QStringList DbEncoding::encodingList = QStringList::fromList(encodingNameMap.keys());


DbEncoding::DbEncoding()
{

}

// convert a given encoding to the interal UTF8 format
QByteArray DbEncoding::decode(const QByteArray &data, const QString &encoding)
{
    if (encoding.isEmpty())
    {
        return data;
    }

    QStringConverter::Encoding currentEncoding;
    if (encodingNameMap.contains(encoding))
    {
        currentEncoding = encodingNameMap[encoding];
    }
    else
    {
        qInfo() << tr("encoding '%1' is not supported should be one of (%2) -- using system")
                   .arg(encoding, getEncodings().join(","));
        currentEncoding = QStringConverter::System;
    }

    auto converter = QStringDecoder(currentEncoding);
    QString ts = converter(data);
    return ts.toUtf8();
}

QStringList DbEncoding::getEncodings()
{
    return encodingNameMap.keys();
}
