#pragma once

#include "logmessage.h"
#include <QObject>
#include <QMap>
#include <QString>
#include <QStringConverter>

class DbEncoding : public QObject
{
    Q_OBJECT

public:    
    static QByteArray decode(const QByteArray &data, const QString &encoding);
    static QStringList getEncodings();
    static bool hasEncoding(const QString &enc) { return encodingList.contains(enc); }
    static QString getEncodingErrorMsg(const QString &enc);

private:
    DbEncoding();

    const static QMap<QString, QStringConverter::Encoding> encodingNameMap;
    const static QStringList encodingList;
};
