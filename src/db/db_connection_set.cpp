#include "db_connection_set.h"
#include <QDebug>
#include <QFile>
#include <common.h>

DbConnectionSet::DbConnectionSet(QObject *parentObj) :
    QAbstractListModel(parentObj)
    ,logger(LogMessage::instance())
    ,databaseSetFileName("")
{
}

void DbConnectionSet::append(DbConnection *dbcon)
{
    beginInsertRows(QModelIndex(), 0, 0);
    dbList.append(dbcon);
    std::sort(dbList.begin(), dbList.end(),
              [](const DbConnection *a, const DbConnection *b) -> bool
    {
        return a->getName() < b->getName();
    });
    endInsertRows();
}

void DbConnectionSet::remove(DbConnection *dbcon)
{
    beginRemoveRows(QModelIndex(), 0, 0);
    dbList.removeOne(dbcon);
    endRemoveRows();

    delete dbcon;
}

void DbConnectionSet::removeByName(const QString &dbName)
{
    DbConnection *currentDbConnection = this->getByName(dbName);
    if (currentDbConnection == nullptr)
    {
        return;
    }

    this->remove(currentDbConnection);
}

void DbConnectionSet::clear()
{
    foreach(DbConnection *dbc, dbList)
    {
        if(QSqlDatabase::contains(dbc->getName()))
        {
            // close and remove existing databases
            dbc->closeDatabase();
            QSqlDatabase::removeDatabase(dbc->getName());
        }
        remove(dbc);
    }
    dbList.clear();
}

//!
//! \brief DbConnectionSet::readDatabaseSet
//! \param dbsName
//! \param mergeWithExisting
//!
//! This read a database XML file and replace the existing or merge all
//! entries to the existing by overwriting identical entries
//! In case of an error reading the new file by overwriting the existing, we
//! read the last file again.
void DbConnectionSet::readDatabaseSet(const QString &dbsName, bool mergeWithExisting)
{
    QString lastDbsName = this->getDatabaseSetFileName();

    if (dbsName == lastDbsName)
    {
        logger.infoMsg(tr("ignore to read the same database again"));
        return;
    }
    else if (!mergeWithExisting)
    {
        this->writeXml("");
        this->clear();
    }

    if (!this->readXml(dbsName, mergeWithExisting) && !mergeWithExisting)
    {
        this->clear();
        this->readXml(lastDbsName, false);
    }
}

bool DbConnectionSet::readXml(const QString &aFilename, bool mergeFile)
{
    // Jetzt das XML-Dokument parsen
    QDomElement docElem = Common::openXmlFile(aFilename);

    if (docElem.isNull())
    {
        logger.errorMsg(tr("wrong or empty database set XML file '%1'").arg(aFilename));
        return false;
    }

    if (docElem.nodeName().toUpper() != "SQLREPORT")
    {
        logger.errorMsg(tr("root node in XML must be 'SQLREPORT' but is '%1'")
                            .arg(docElem.nodeName().toUpper()));
        return false;
    }

    qsizetype config_version = Common::getXmlVersion(docElem);
    QDomNode p = docElem.firstChild();

    logger.infoMsg(tr("read database set file '%1' version %2")
                       .arg(aFilename)
                       .arg(config_version));

    int cnt = 5;
    bool hasDbSet = false;
    while (!p.isNull() && cnt-- > 0)
    {
        logger.debugMsg(tr("sqlreport child %1").arg(p.nodeName()));

        if ("DATABASESET" == p.nodeName().toUpper())
        {
            readXmlNode(p, config_version);
            hasDbSet = true;
        }

        p = p.nextSibling();
    }

    if (!hasDbSet)
    {
        logger.errorMsg(tr("the database file has no DATABASESET section, possible you select a queryset XML file -- ignore this file"));
        return false;
    }

    if (!mergeFile || this->getDatabaseSetFileName().isEmpty())
    {
        databaseSetFileName = aFilename;
    }

    return true;
}

//! Reading the DatabaseEntry part of the QuerySet file. This contains a number
//! of database entries, every with a name which is used to store it in the map.
//! \param aNode is the DATABASESET node
void DbConnectionSet::readXmlNode(const QDomNode &aNode, const qsizetype config_version)
{
    qDebug() << "connection set reading " << aNode.nodeName();

    QDomNode n = aNode.firstChild();
    while(!n.isNull())
    {
        if(n.nodeName().toUpper() == "DATABASE")
        {
            DbConnection *dbCon = new DbConnection(this);
            dbCon->readXmlNode(n, config_version);
            DbConnection *existingConnection = getByName(dbCon->getName());
            if (existingConnection != nullptr)
            {
                qInfo() << tr("Overwrite connection %1").arg(existingConnection->getName());
                remove(existingConnection);
            }
            append(dbCon);
        }
        n = n.nextSibling();
    }
}

bool DbConnectionSet::writeXml(const QString &aFileName) const
{
    QString localFileName = aFileName;

    if (localFileName.isEmpty())
    {
        // if no name is given use the stored filename
        localFileName = databaseSetFileName;
    }

    if (localFileName.isEmpty())
    {
        return false;
    }

    QFile file(localFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        logger.errorMsg(tr("can't open database set file '%1' for writing").arg(localFileName));
        return false;
    }
    logger.infoMsg(tr("write database set to '%1'").arg(localFileName));

    QXmlStreamWriter vStream(&file);
    Common::writeXmlHeader(vStream);

    vStream.writeStartElement("DatabaseSet");
    writeXmlNode(vStream);
    vStream.writeEndElement();

    vStream.writeEndDocument();
    file.close();

    return true;
}

void DbConnectionSet::writeXmlNode(QXmlStreamWriter &aStream) const
{
    foreach(DbConnection *dbc, dbList)
    {
        qDebug() << "save database configuration " << dbc->getName();
        dbc->writeXmlNode(aStream);
    }
}

//! Return the DbConnection object that is connected to
//! the name.
DbConnection *DbConnectionSet::getByName(const QString &name) const
{
    foreach(auto dbc, dbList)
    {
        if(name == dbc->getName()) return dbc;
    }

    return nullptr;
}

void DbConnectionSet::getNames(QStringList &aList) const
{
    foreach(auto dbc, dbList)
    {
        aList.append(dbc->getName());
    }
}

QString DbConnectionSet::getCopyName(const QString &name) const
{
    if (nullptr != getByName(name))
    {
        return getCopyName(QString("%1_copy").arg(name));
    }
    return name;
}

qint32 DbConnectionSet::rowCount(const QModelIndex &) const
{
    return dbList.count();
}

QVariant DbConnectionSet::data(const QModelIndex &index, int role) const
{
    if(!index.isValid()) return QVariant();

    if(index.row() >= dbList.count()) return QVariant();

    if(role == Qt::DisplayRole)
    {
        return dbList[index.row()]->getName();
    }
    else
    {
        return QVariant();
    }
}

QVariant DbConnectionSet::headerData(int section,
                                     Qt::Orientation orientation,
                                     int role) const
{
    Q_UNUSED(section)
    Q_UNUSED(orientation)

    if(Qt::DisplayRole != role) return QVariant();
    return ("Database");
}
