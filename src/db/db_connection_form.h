#pragma once

#include "db_connection.h"
#include <QDialog>

namespace Ui {
class DbConnectionForm;
}

class DbConnectionForm : public QDialog
{
	Q_OBJECT
	Q_CLASSINFO ("author", "St. Koehler")
	Q_CLASSINFO ("company", "com.github.mosling")

public:
    explicit DbConnectionForm(DbConnection *dbCon, const QString basePath, QWidget *parent = 0);
	~DbConnectionForm();

private slots:
	void on_pushButtonExit_clicked();

	void on_pushButtonCancel_clicked();

	void on_toolButtonName_clicked();

    void on_passwordView_pressed();

    void on_passwordView_released();

    void on_btnTestConnection_clicked();

private:
    bool updateDbc(DbConnection *dbc);
	Ui::DbConnectionForm *ui;
    DbConnection *formDbc;
    DbConnection testDbc;
    const QString basePath;

};
