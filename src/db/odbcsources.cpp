#include <qglobal.h>

#ifdef Q_OS_WIN
#include <windows.h>
#include <tchar.h>
#include <iostream>
#include <sql.h>
#include <sqlext.h>
#include <sqlucode.h>
#endif

#include <QSettings>

#include "odbcsources.h"

OdbcSources::OdbcSources(QObject *parent) : QObject(parent)
{

}

QStringList OdbcSources::getConnectionList()
{
    QStringList l;

#ifdef Q_OS_WIN
    QSettings m("HKEY_CURRENT_USER\\SOFTWARE\\ODBC\\ODBC.INI", QSettings::NativeFormat);
    l = m.childGroups();
    l.sort();
#endif

    return l;
}


