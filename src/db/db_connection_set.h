#pragma once

#include "db_connection.h"
#include "logmessage.h"

#include <QAbstractListModel>
#include <QList>
#include <QString>

class DbConnectionSet : public QAbstractListModel
{
	Q_OBJECT

public:
    explicit DbConnectionSet(QObject *parentObj = nullptr);

	void append(DbConnection *dbcon);
    void remove(DbConnection *dbcon);
    void removeByName(const QString &dbName);
	void clear();
    qsizetype size() const { return dbList.size(); }

    QString getDatabaseSetFileName() const { return databaseSetFileName; }
    void setDatabaseSetFileName(QString &fn) { databaseSetFileName = fn; }
    void readXmlNode(const QDomNode &aNode, const qsizetype config_version);
    void writeXmlNode(QXmlStreamWriter &aStream) const;

	DbConnection *getByName(const QString &name) const;
    void getNames(QStringList &aList) const;
    QString getCopyName(const QString &name) const;

    qint32 rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    bool readXml(const QString &aFilename, bool mergeFile);
    bool writeXml(const QString &aFileName) const;
    void readDatabaseSet(const QString &dbsName, bool mergeWithExisting);
signals:

public slots:

private:
    LogMessage &logger;
    QString databaseSetFileName;
	QList<DbConnection*> dbList;
};
