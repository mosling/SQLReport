#include "dbconnectionstatus.h"
#include "ui_dbconnectionstatus.h"

#include <QStyle>

DbConnectionStatus::DbConnectionStatus(bool status, const QString &errmsg, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DbConnectionStatus)
{
    ui->setupUi(this);
    this->setWindowTitle("Connection");

    if (status)
    {
        ui->lblStatus->setText(tr("Successful"));
        QIcon icon = style()->standardIcon(QStyle::SP_MessageBoxInformation);
        QPixmap pixmap = icon.pixmap(QSize(32, 32));
        ui->lblIcon->setPixmap(pixmap);
        ui->teError->hide();
    }
    else
    {
        ui->lblStatus->setText(tr("Connection Error"));
        QIcon icon = style()->standardIcon(QStyle::SP_MessageBoxCritical);
        QPixmap pixmap = icon.pixmap(QSize(32, 32));
        ui->lblIcon->setPixmap(pixmap);
        ui->teError->appendPlainText(errmsg);
        ui->teError->viewport()->setAutoFillBackground(false);
    }
    this->adjustSize();
}

DbConnectionStatus::~DbConnectionStatus()
{
    delete ui;
}
