#pragma once

#include "qtree_reporter.h"

#include <QString>
#include <QStringList>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlError>
#include <QtXml/QDomDocument>
#include <QtCore/QXmlStreamWriter>

//! This class holds the information for a database connection and has some methods to
//! connect the database, react at errors and show the structure of the database in a
//! QTreeReporter.
class DbConnection : public QObject
{
	Q_OBJECT

public:
    explicit DbConnection(QObject *parentObj = nullptr);
    virtual ~DbConnection() override;
    void copyFrom(const DbConnection& rhs);

    QString lastDbError(bool showError = false);
    QString toString() const;
    bool connectDatabase(const QString &basePath);
	void closeDatabase() const;
    void showTableList(QSql::TableType aType, QString aHead, bool withFk, QTreeReporter *treeReporter) const;
    void showDatabaseTables(QTreeReporter *tr, bool withFk);

    void readXmlNode(const QDomNode &aNode, const qsizetype config_version);
    void writeXmlNode(QXmlStreamWriter &aStream);

    QString getName() const { return name; }
    void setName(const QString &value);

    QString getDbInternalType() const;

	QString getDbType() const {return dbType; }
	void setDbType(const QString &value);

	QString getDbName() const {return dbName; }
	void setDbName(const QString &value);

    QString getDbEncoding() const {return dbEncoding; }
    void setDbEncoding(const QString &value);

    QString getDbOptions() const {return dbOptions; }
    void setDbOptions(const QString &value);

    QString getDatabaseParameter() const {return databaseParameter; }
    void setDatabaseParameter(const QString &value) { databaseParameter = value; };

	QString getHost() const {return host; }
    void setHost(const QString &value) {host = value; };

	QString getUsername() const {return username; }
	void setUsername(const QString &value);

	QString getPassword() const {return password; }
	void setPassword(const QString &value);

	quint32 getPort() const {return port; }
	void setPort(const quint32 &value);

	bool getPasswordSave() const {return passwordSave; }
	void setPasswordSave(const bool value);

    QString getDbTemplate() const {return dbTemplate; }
    void setDbTemplate(const QString &value);

    QString getConnectionName(const QString &basePath, bool showInfo = true) const;

public slots:
    void stopProcess() { stopPressed = true; }

private:
	QString name;
	QString dbType;
	QString dbName;
    QString dbEncoding;
    QString dbOptions;
    QString databaseParameter;
	QString host;
	QString username;
	QString password;
    QString passwordSalt;
	quint32 port;
	bool passwordSave;
    QString dbTemplate;
    bool stopPressed;
    QString connectionError;

    QString getFieldString(const QSqlField field) const;
    QStringList getTableList() const;
    QStringList getForeignKeyList(const QString &tableName) const;
    QStringList getReferencedFieldList(const QString &relation, const QString &primary_key) const;
};
