#include "dbconnectionstatus.h"
#include "odbcsources.h"
#include "db_connection_form.h"
#include "ui_db_connection_form.h"
#include "dbencoding.h"

#include <QFileDialog>
#include <QInputDialog>
#include <common.h>

DbConnectionForm::DbConnectionForm(DbConnection *dbCon, const QString basePath, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DbConnectionForm),
    formDbc(dbCon),
    basePath(basePath)
{
	ui->setupUi(this);

	// Combobox füllen
    QStringList drivers = QSqlDatabase::drivers();
    drivers.append(Common::mssqlName());
    drivers.sort();

    ui->cbDbType->addItems(drivers);

    QStringList encodings = DbEncoding::getEncodings();
    // ugly code to brings UTF-8 to front
    if (encodings.contains("UTF-8"))
    {
        encodings.removeOne("UTF-8");
        encodings.insert(0, "UTF-8");
    }
    ui->cbDbEncoding->addItems(encodings);

    if (nullptr != formDbc)
	{
        ui->lineEditName->setText(formDbc->getName());
        if (!drivers.contains(formDbc->getDbType()))
        {
            // add unknown database connection type
            ui->cbDbType->addItem(formDbc->getDbType());
        }
        if (!encodings.contains(formDbc->getDbEncoding()))
        {
            // set default encoding
            formDbc->setDbEncoding(DbEncoding::getEncodings().at(0));
        }
        ui->cbDbEncoding->setCurrentText(formDbc->getDbEncoding());
        ui->cbDbType->setCurrentText(formDbc->getDbType());
        ui->lineEditDbName->setText(formDbc->getDbName());
        ui->lineEditOptions->setText(formDbc->getDbOptions());
        ui->lineEditDatabaseParameter->setText(formDbc->getDatabaseParameter());
        ui->lineEditHost->setText(formDbc->getHost());
        if (formDbc->getPort() > 0)
        {
            ui->lineEditPort->setText(QString("%1").arg(formDbc->getPort()));
        }
        ui->lineEditUser->setText(formDbc->getUsername());
        ui->lineEditPassword->setText(formDbc->getPassword());
        ui->checkBoxPasswordSave->setChecked(formDbc->getPasswordSave());
        ui->textDbTemplate->setText(formDbc->getDbTemplate());
	}
}

DbConnectionForm::~DbConnectionForm()
{
	delete ui;
}


void DbConnectionForm::on_pushButtonExit_clicked()
{
    updateDbc(formDbc);
	this->close();
}

void DbConnectionForm::on_pushButtonCancel_clicked()
{
	this->close();
}

void DbConnectionForm::on_toolButtonName_clicked()
{
    if (ui->cbDbType->currentText() == "QODBC")
    {
        bool ok;
        QStringList items = OdbcSources::getConnectionList();
        QString item = QInputDialog::getItem(this, tr("Select Connection"),
                                             tr("ODBC Datasource:"), items, 0, false, &ok);
        if (ok && !item.isEmpty())
            ui->lineEditDbName->setText(item);
    }
    else
    {
        QString str = QFileDialog::getOpenFileName(this,
                                                   "Please select database",
                                                   formDbc->getDbName(),
                                                   "Alle Dateien(*.*)"
											   );
        if (!str.isEmpty())
        {
            formDbc->setDbName(str);
            ui->lineEditDbName->setText(str);
        }
    }
}

void DbConnectionForm::on_passwordView_pressed()
{
    ui->lineEditPassword->setEchoMode(QLineEdit::Normal);
}


void DbConnectionForm::on_passwordView_released()
{
    ui->lineEditPassword->setEchoMode(QLineEdit::PasswordEchoOnEdit);
}

void DbConnectionForm::on_btnTestConnection_clicked()
{
    testDbc.copyFrom(*formDbc);

    if (updateDbc(&testDbc))
    {
        bool rv = testDbc.connectDatabase(basePath);
        QString le = testDbc.lastDbError(false);

        DbConnectionStatus st = DbConnectionStatus(rv ,le, this);
        st.setModal(true);
        st.exec();
    }
}

bool DbConnectionForm::updateDbc(DbConnection *dbc)
{
    if (nullptr != dbc)
    {
        dbc->setName(ui->lineEditName->text());
        dbc->setDbName(ui->lineEditDbName->text());
        dbc->setDbOptions(ui->lineEditOptions->text());
        dbc->setDatabaseParameter(ui->lineEditDatabaseParameter->text());
        dbc->setHost(ui->lineEditHost->text());
        dbc->setDbType(ui->cbDbType->currentText());
        dbc->setDbEncoding(ui->cbDbEncoding->currentText());
        bool bOk;
        quint32 p = ui->lineEditPort->text().toUInt(&bOk, 10);
        dbc->setPort(bOk ? p : 0);
        dbc->setUsername(ui->lineEditUser->text());
        dbc->setPassword(ui->lineEditPassword->text());
        dbc->setPasswordSave(ui->checkBoxPasswordSave->checkState() == Qt::Checked);
        dbc->setDbTemplate(ui->textDbTemplate->toPlainText());

        ui->textDbConnection->setPlainText(dbc->getConnectionName(basePath, false)
                                           .replace(dbc->getPassword().isEmpty() ? "XYZ24!?" : dbc->getPassword(), "****"));
        return true;
    }

    return false;
}

