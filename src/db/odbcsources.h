#pragma once

#include <QObject>

class OdbcSources : public QObject
{
    Q_OBJECT
public:
    explicit OdbcSources(QObject *parent = nullptr);
    static QList<QString> getConnectionList();

signals:

};
