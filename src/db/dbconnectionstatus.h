#ifndef DBCONNECTIONSTATUS_H
#define DBCONNECTIONSTATUS_H

#include <QDialog>

namespace Ui {
class DbConnectionStatus;
}

class DbConnectionStatus : public QDialog
{
    Q_OBJECT

public:
    explicit DbConnectionStatus(bool status, const QString &errmsg, QWidget *parent = nullptr);
    ~DbConnectionStatus();

private:
    Ui::DbConnectionStatus *ui;
};

#endif // DBCONNECTIONSTATUS_H
