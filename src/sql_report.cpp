#include "sql_report.h"
#include "editwidget.h"
#include "parameter/parameterdialog.h"
#include "parameter/parametermodel.h"
#include "qtree_reporter.h"
#include "db_connection_form.h"
#include "common.h"
#include "config.h"
#include "ui_sql_report.h"

#include <QRegularExpression>
#include <QtSql/QSqlRecord>
#include <QFileInfo>
#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QTime>
#include <QMenu>

SqlReport::SqlReport(QWidget *parentObj, Qt::WindowFlags flags)
    : QMainWindow(parentObj, flags)
    ,ui(new Ui::SqlReport)
    ,logger(LogMessage::instance())
    ,mQuerySet(this)
    ,databaseSet(this)
    ,activeQuerySetEntry(nullptr)
    ,treeModel()
    ,sqlEditor(nullptr, "sql", tr("SQL"), true, true)
    ,templateEditor(nullptr, "template", tr("Template"), true, true)
    ,outputEditor(nullptr, "output", tr("Output"), false, false)
    ,descriptionEditor(tr("Query Set Description"), this)
    ,queryExecutor(nullptr)
    ,aboutDialog(new AboutDialog(this))
    ,lockFile(nullptr)
    ,interface_language("EN")
    ,showConfiguration(0)
    ,displayModeGroup(new QActionGroup(this))
{
    ui->setupUi(this);
    aboutDialog.setVersion(SQLREPORT_VERSION);

    ui->btnStop->hide();
    ui->progressBarExecute->hide();
    ui->tvTable->setModel(&treeModel);
    ui->tvTable->header()->hide();

    ui->comboBoxDatabase->setModel(&databaseSet);
    ui->cbQuery->setModel(&mQuerySet);
    ui->comboBoxDatabase->model()->sort(0);
    ui->cbQuery->model()->sort(0);

    connectMenuActions();

    // update texteditor to not flood the memory
    ui->textEditReport->setUndoRedoEnabled(false);

    // connect recent query set combo box
    connect(ui->cbRecentQuerySets, &RecentComboBox::recentEntryChanged, this, &SqlReport::readQuerySet);

    // connect sql and template editor
    sqlEditor.setConnectedWidget(&templateEditor);
    templateEditor.setConnectedWidget(&sqlEditor);

    // restore the window geometry for the widgets and the
    // window state for the main window
    QSettings rc;
    this->restoreState(rc.value("mainwindow/windowState").toByteArray());
    this->restoreGeometry(rc.value("mainwindow/geometry").toByteArray());

    // set logger windows
    logger.setMsgWindow(ui->textEditReport);
    logger.setContext("startup");

    ui->lineEditLocalParameter->setText(rc.value("mainwindow/local_parameter","").toString());

    // read database set for application from settings
    // must read before query sets, because the queryset readXml function read the
    // database connection from the settings
    QString dbsn = rc.value("mainwindow/databaseset_name","").toString();
    if (!dbsn.isEmpty())
    {
        databaseSet.readDatabaseSet(dbsn, false);
        ui->lineEditDatabaseSet->setText(databaseSet.getDatabaseSetFileName());
    }
    else
    {
        logger.warnMsg(tr("missing databases file, please select one"));
    }

    // read query set for the application from the settings
    QString qsn = rc.value("mainwindow/queryset_name","").toString();
    ui->cbRecentQuerySets->setMaxEntries(25);
    ui->cbRecentQuerySets->setList(qsn.split("|", Qt::SkipEmptyParts));
    if (qsn.isEmpty())
    {
        logger.warnMsg(tr("missing queries file, please select one"));
    }

    bool bOk;
    this->showConfiguration = rc.value("mainwindow/display_configuration", 0).toInt(&bOk);
    if (!bOk || (this->showConfiguration < 0) || (this->showConfiguration > 2))
    {
        this->showConfiguration = 0;
    }

    // menu entry
    foreach (QAction *action, displayModeGroup->actions())
    {
        if (action->data().toInt() == this->showConfiguration)
        {
            action->setChecked(true);
            action->trigger();
        }
    }

    switch (this->showConfiguration)
    {
    case 1: ui->actionSMUser->setChecked(true);
        ui->actionSMUser->trigger();
        break;
    case 2: ui->actionSMFocus->setChecked(true);
        ui->actionSMFocus->trigger();
        break;
    default: ui->actionSMDevelopment->setChecked(true);
         ui->actionSMDevelopment->trigger();
        this->showConfiguration = 0;
        break;
    }

    logger.setContext("");
}


void SqlReport::connectMenuActions()
{
    // Template
    // connect(ui->action, &QAction::triggered, this, &SqlReport::);

    // File Menu
    connect(ui->actionFDescription, &QAction::triggered, this, &SqlReport::querysetDescription);
    connect(ui->actionFAbout, &QAction::triggered, this, &SqlReport::showAbout);
    connect(ui->actionFExit, &QAction::triggered, this, &SqlReport::exitApplication);

    // File Database Menu
    connect(ui->actionFDNew, &QAction::triggered, this, &SqlReport::new_database_list);
    connect(ui->actionFDMerge, &QAction::triggered, this, &SqlReport::merge_database_list);
    connect(ui->actionFDLoad, &QAction::triggered, this, &SqlReport::import_database_list);

    // File Queries menu
    connect(ui->actionFQNew, &QAction::triggered, this, &SqlReport::actionNewQuerySet);
    connect(ui->actionFQLoad, &QAction::triggered, this, &SqlReport::actionLoadQuerySet);
    connect(ui->actionFQExport, &QAction::triggered, this, &SqlReport::actionExportQuerySet);

    // Query Menu
    connect(ui->actionQNew, &QAction::triggered, this, &SqlReport::actionAddQuery);
    connect(ui->actionQRename, &QAction::triggered, this, &SqlReport::actionRenameQuery);
    connect(ui->actionQRemove, &QAction::triggered, this, &SqlReport::actionDeleteQuery);
    connect(ui->actionQLoad_SQL, &QAction::triggered, this, &SqlReport::actionLoadSql);
    connect(ui->actionQLoad_Template, &QAction::triggered, this, &SqlReport::actionLoadTemplate);

    // Database Menu
    connect(ui->actionDNew, &QAction::triggered, this, &SqlReport::actionAddDatabase);
    connect(ui->actionDRemove, &QAction::triggered, this, &SqlReport::actionDeleteDatabase);
    connect(ui->actionDStructure, &QAction::triggered, this, &SqlReport::actionShowTables);
    connect(ui->actionDKey_Structure, &QAction::triggered, this, &SqlReport::actionShowTablesPlus);

    // Settings Menu
    connect(ui->actionSLEnglish, &QAction::triggered, this, &SqlReport::languageSwitchEnglish);
    connect(ui->actionSLGerman, &QAction::triggered, this, &SqlReport::languageSwitchGerman);
    connect(ui->actionSParameter, &QAction::triggered, this, &SqlReport::parameterDialog);

    // Settings Display Mode Menu
    ui->actionSMDevelopment->setData(0);
    ui->actionSMFocus->setData(2);
    ui->actionSMUser->setData(1);
    displayModeGroup->addAction(ui->actionSMDevelopment);
    displayModeGroup->addAction(ui->actionSMFocus);
    displayModeGroup->addAction(ui->actionSMUser);
    displayModeGroup->setExclusionPolicy(QActionGroup::ExclusionPolicy::Exclusive);

    connect(displayModeGroup, &QActionGroup::triggered, this, &SqlReport::switchDisplayMode);
}


SqlReport::~SqlReport()
{
    try
    {
        QSettings rc;

        rc.setValue("language", this->interface_language);

        rc.beginGroup("mainwindow");
        rc.setValue("queryset_name", ui->cbRecentQuerySets->getList().join("|"));
        rc.setValue("databaseset_name", ui->lineEditDatabaseSet->text());
        rc.setValue("local_parameter", ui->lineEditLocalParameter->text());
        rc.setValue("display_configuration", this->showConfiguration);
        rc.setValue("geometry", this->saveGeometry());
        rc.setValue("windowState", QVariant(this->saveState()));
        rc.endGroup();

        activeQuerySetEntry = nullptr;

        if (lockFile != nullptr)
        {
            lockFile->unlock();
            lockFile = nullptr;
        }
    }
    catch (...) {}
}

void SqlReport::closeEvent(QCloseEvent *event)
{
    // close all editor windows
    sqlEditor.close();
    templateEditor.close();
    outputEditor.close();

    if (sqlEditor.isVisible() || templateEditor.isVisible() || outputEditor.isVisible())
    {
        // something was not closed
        event->ignore();
        return;
    }

    updateQuerySet();
    bool ignoreRequest = false;

    // check if the query set file exists
    QString qsfn = mQuerySet.getQuerySetFileName();

    if (qsfn.isEmpty() && ( mQuerySet.size() > 0 || !mQuerySet.getQuerySetDescription().isEmpty()))
    {
        qsfn = QFileDialog::getSaveFileName(this, tr("Select QuerySet file to save"),
                                                      mQuerySet.getQuerySetFileName(),
                                                      tr("XML Files (*.xml)"));

        if (!qsfn.isEmpty())
        {
            mQuerySet.writeXml(qsfn);
            ui->cbRecentQuerySets->addToList(qsfn);
        }
        else
        {
            ignoreRequest = true;
        }
    }
    else
    {
        mQuerySet.writeXml("");
    }

    // same for databaseset
    QString dbsfn = databaseSet.getDatabaseSetFileName();
    if (dbsfn.isEmpty() && ( databaseSet.size() > 0 ))
    {
        dbsfn = QFileDialog::getSaveFileName(this, tr("Select DatabaseSet file to save"),
                                            databaseSet.getDatabaseSetFileName(),
                                            tr("XML Files (*.xml)"));

        if (!dbsfn.isEmpty())
        {
            databaseSet.writeXml(dbsfn);
            ui->lineEditDatabaseSet->setText(dbsfn);
        }
        else
        {
            ignoreRequest = true;
        }
    }
    else
    {
        databaseSet.writeXml("");
    }

    if (ignoreRequest)
    {
        QMessageBox msgBox;
        msgBox.setText("Close Application");
        msgBox.setInformativeText("Ignore Changes and close application?");
        msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Ok);
        int ret = msgBox.exec();
        if (ret == QMessageBox::Cancel)
        {
            event->ignore();
            return;
        }
    }

}

void SqlReport::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::WindowStateChange)
    {
        if (isMinimized())
        {
            sqlEditor.setWindowState( (sqlEditor.windowState() | Qt::WindowMinimized));
            templateEditor.setWindowState( (templateEditor.windowState() | Qt::WindowMinimized));
            outputEditor.setWindowState( (outputEditor.windowState() | Qt::WindowMinimized));
        }
        else
        {
            sqlEditor.setWindowState( (sqlEditor.windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
            templateEditor.setWindowState( (templateEditor.windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
            outputEditor.setWindowState( (outputEditor.windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
            this->raise();
        }
    }
}

void SqlReport::setDescription(QString str, bool isQueryDescription)
{
   if (!isQueryDescription && activeQuerySetEntry != nullptr)
   {
       activeQuerySetEntry->setDescription(str);
       ui->lineEditDescr->setText(Common::getDescriptionHeadline(str));
   }

   if (isQueryDescription)
   {
       mQuerySet.setQuerySetDescription(str);
   }
}

bool SqlReport::executeBatchLine(
        QueryExecutor *vpExecutor,
        DbConnection *batchDb,
        QString queryPath,
        const QString &baseInput,
        QString commandLine,
        quint32 cntCommands, quint32 lineNr, quint32 queryNr)
{
    QStringList qList = commandLine.split("!!", Qt::SkipEmptyParts);
    QString queryInput = "";

    if (qList.size() > 0)
    {
        bool useBatchDb = false;
        QString queryName = qList.at(0);
        for (qsizetype i = 1; i < qList.size(); ++i)
        {
            queryInput = queryInput + "|" + qList.at(i);
        }

        if (queryName.startsWith("USEBATCHDB_", Qt::CaseInsensitive))
        {
            useBatchDb = true;
            queryName.remove(0,11);
        }

        if (mQuerySet.contains(queryName))
        {
            QuerySetEntry *tmpQuery = mQuerySet.getByName(queryName);
            DbConnection *dbcon = useBatchDb ? batchDb
                                             : databaseSet.getByName(tmpQuery->getDbName());

            QString dbStr = dbcon == nullptr ? QString("no database given") : dbcon->getName();
            logger.setQuerysetName(queryName);
            logger.infoMsg(QString("--[ %1/%2 @ %3 ]---------------------------------------------")
                            .arg(queryNr).arg(cntCommands).arg(dbStr));

            logger.setQuerysetName(tmpQuery->getName());
            QString queryParameter = tmpQuery->getQueryParameter() + "|" + baseInput + queryInput;
            return vpExecutor->createOutput(tmpQuery, dbcon, queryPath, queryParameter);
        }
        else
        {
            logger.errorMsg(tr("Unknown QuerySet '%1' at line %2").arg(queryName).arg(lineNr));
        }
    }

    // no commands found, not a problem at all
    return true;
}

void SqlReport::switchBatchOutputDisplay(bool checked)
{
    // if no batch run or configuration is development mode, we show the output field
    bool displayOuput = !checked || (this->showConfiguration == 0);

    ui->labelOutput->setVisible(displayOuput);
    ui->frameOutput->setVisible(displayOuput);
}

//! Press the Ok-button starts the processing. At this time we
//! create the database connection and parse the template file starting
//! with the ::MAIN entry.
//! If the active query set is BATCH we execute the generated query statements atsrting with !!
void SqlReport::start_execution()
{
    if (queryExecutor != nullptr)
    {
        logger.warnMsg("one active executor allowed at the moment ...");
        return;
    }

    if (activeQuerySetEntry == nullptr)
    {
        logger.infoMsg("Please create an query set to execute ...");
        return;
    }

    queryExecutor = new QueryExecutor(this);
    QObject::connect(ui->btnStop, SIGNAL(clicked()), queryExecutor, SLOT(stopProcess()));

    QString queryPath = QFileInfo(mQuerySet.getQuerySetFileName()).absolutePath();
    QString parameterList = ui->lineEditQueryParameter->text()
                            + "|" + ui->lineEditLocalParameter->text();

    // save open editor
    sqlEditor.saveFile();
    templateEditor.saveFile();

    // do the action
    updateLogger();
    updateQuerySet();

    logger.setDebugFlag(ui->checkBoxDebug->checkState());
    ui->btnStop->show();
    ui->progressBarExecute->show();

    if (activeQuerySetEntry->getBatchrun())
    {
        queryExecutor->createBatchOuput(
            mQuerySet,
            databaseSet,
            activeQuerySetEntry,
            parameterList);
    }
    else
    {
        queryExecutor->createOutput(
            activeQuerySetEntry,
            databaseSet.getByName(activeQuerySetEntry->getDbName()),
            queryPath,
            parameterList
            );

        ui->output->setText(activeQuerySetEntry->getOutputFile());
    }

    delete queryExecutor;
    queryExecutor = nullptr;
    ui->btnStop->hide();
    ui->progressBarExecute->hide();
}

//! Im QuerySet werden nur die Dateinamen gespeichert. Um wirklich
//! zugreifen zu können, benötigen wir den absoluten Namen. Diesen
//! erzeugen wir durch das Hinzufügen des absoluten Pfades zum queryset,
//! falls nicht ein absoluter Name angegeben wurde.
QString SqlReport::getAbsoluteFileName(QString fname) const
{
    QFileInfo f(fname);

    if (!f.isAbsolute())
    {
        return mQuerySet.getQuerySetPath() + "/" + fname;
    }

    return fname;
}

//! Auswahl einer Datei. Ist das Modify-Flag gesetzt wird vor den
//! übergebenen Dateinamen (def) der aktuelle Path der QuerySet-Datei
//! gesetzt und danach wieder entfernt.
QString SqlReport::selectFile(QString desc, QString def, QString pattern,
                              bool modify, bool &cancel)
{
    QString defName(def);
    cancel = false;

    if (modify)
    {
        defName = getAbsoluteFileName(def);
    }

    QString str = QFileDialog::getOpenFileName(this, desc, defName, pattern);

    if (str.isEmpty())
    {
        str = def;
        cancel = true;
    }

    if (modify)
    {
        QFileInfo fi (str);
        QString qsPath = QFileInfo(mQuerySet.getQuerySetFileName()).absolutePath();

        if (fi.absoluteFilePath().startsWith(qsPath))
        {
            str = fi.absoluteFilePath().remove(0, qsPath.size() + 1);
        }
        else
        {
            str = fi.absoluteFilePath();
        }
    }

    return str;
}



//! Here we enter die Database connection dialog. If we have no existing database
//! we start with the 'new connection' connection. This works also without
//! active query, (normally at the start by impatient customer:-)
void SqlReport::actionEditDatabase()
{

    DbConnection *currentDbConnection = databaseSet.getByName(ui->comboBoxDatabase->currentText());

    if (nullptr == currentDbConnection)
    {
        QString nc("new connection");
        currentDbConnection = databaseSet.getByName(nc);
        if (nullptr == currentDbConnection)
        {
            currentDbConnection = new DbConnection(&databaseSet);
            currentDbConnection->setName(nc);
            databaseSet.append(currentDbConnection);
        }
    }

    DbConnectionForm dbForm(currentDbConnection, mQuerySet.getQuerySetPath(), this);
    dbForm.exec();

}

//! Add a new database connection to the system. This is named 'new connection'
void SqlReport::actionAddDatabase()
{
    DbConnection *currentDbConnection = databaseSet.getByName(ui->comboBoxDatabase->currentText());
    DbConnection *newDbConnection = new DbConnection(&databaseSet);

    if (nullptr == currentDbConnection)
    {
        newDbConnection->setName(databaseSet.getCopyName("new connection"));
    }
    else
    {
        newDbConnection->copyFrom(*currentDbConnection);
        newDbConnection->setName(databaseSet.getCopyName(newDbConnection->getName()));
    }

    databaseSet.append(newDbConnection);

    DbConnectionForm dbForm(newDbConnection, mQuerySet.getQuerySetPath(), this);
    dbForm.exec();

    ui->comboBoxDatabase->model()->sort(0);
    ui->comboBoxDatabase->setCurrentText(newDbConnection->getName());
}

//! Delete the selected database connection from the list
//! of available connections. There is no check if other
//! queries use this connection. The object itself is deleted
//! by the databaseSet object, that has created the connection object.
void SqlReport::actionDeleteDatabase()
{

    QMessageBox msgBox;
    QString dbName = ui->comboBoxDatabase->currentText();

    msgBox.setText(tr("Remove database connection '%1'?").arg(dbName));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    int ret = msgBox.exec();

    if (ret == QMessageBox::Yes)
    {
        databaseSet.removeByName(dbName);
    }
}


void SqlReport::actionLoadTemplate()
{
    if (validQuerySet())
    {
        bool selectCancel;
        QString outTemplate = selectFile("Please select Template file",
                                         activeQuerySetEntry->getTemplateFile(),
                                         "Templates (*.template);;All Files(*.*)", true, selectCancel);

        if (!selectCancel)
        {
            ui->outTemplate->setText(outTemplate);
            activeQuerySetEntry->setTemplateFile(outTemplate);
        }
    }
}

void SqlReport::actionLoadSql()
{
    if (validQuerySet())
    {
        bool selectCancel;
        QString outSql = selectFile("Please select SQL file",
                                    activeQuerySetEntry->getSqlFile(),
                                    "SQL-Files (*.sql);;All Files(*.*)", true, selectCancel);

        if (!selectCancel)
        {
            ui->outSql->setText(outSql);
            activeQuerySetEntry->setSqlFile(outSql);
        }
    }
}

//! create new query set, write the existing and write the new one
void SqlReport::actionNewQuerySet()
{
    QString qsName = QFileDialog::getSaveFileName(this, tr("Please select new QuerySet file"),
                                                  mQuerySet.getQuerySetFileName(),
                                                  tr("XML Files (*.xml)"));

    if (!qsName.isEmpty())
    {
        // store an empty queryset, addToList -> readXML do the action to switch a query set
        // the database set is not influenced by creating a new query set
        QuerySet tmpSet;
        tmpSet.writeXml(qsName);
        ui->cbRecentQuerySets->addToList(qsName);
    }
}

//! Select an existing query set xml file to load it into the application.
void SqlReport::actionLoadQuerySet()
{
    bool selectCancel;
    QString qsName = selectFile(tr("Please select QuerySet file"),
                                mQuerySet.getQuerySetFileName(),
                                tr("XML-Files (*.xml);;All Files(*.*)"), false, selectCancel);

    if (!selectCancel)
    {
        ui->cbRecentQuerySets->addToList(qsName);
    }
}

//! Einlesen einer QuerySetDatei.
//!
void SqlReport::readQuerySet(const QString &qsName)
{
    QFile qsFile(qsName);

    if (!qsFile.exists())
    {
        if ( ui->cbRecentQuerySets->getList().contains(qsName))
        {
            logger.errorMsg(tr("query file '%1' no longer exists -> remove from recent queries").arg(qsName));
            // clear current query set
            mQuerySet.clear();
            ui->cbRecentQuerySets->removeFirstElement();
        }
        return;
    }

    if (lockFile != nullptr)
    {
        // in this case we have an active queryset before, and write the changes
        updateQuerySet();
        mQuerySet.writeXml("");
        lockFile->unlock();
    }

    lockFile = new QLockFile(tr("%1.lockfile").arg(qsName));
    if (!lockFile->tryLock())
    {
        qint64 pid;
        QString hostname;
        QString appname;
        lockFile->getLockInfo(&pid, &hostname, &appname);
        QMessageBox::information(this,
                                 "File in Use",
                                 tr("'%3' is locked by %1 at %2 (Attention: information only, no action follows)")
                                     .arg(appname, hostname, qsName));
    }

    // now we have stored all informations, and can switch to the new querySet
    // but first remove the existing queries
    mQuerySet.clear();

    if (mQuerySet.readXml(qsName, databaseSet))
    {
        // successfully read the XML file
        // if queryset version is less than 3 and we have no database set file
        // than create a default file, with the connections read from old query set
        // otherwise the database connection from an old queryset file are
        // merged with the existing database connections
        if (mQuerySet.getQuerySetVersion() <= 2
            && databaseSet.getDatabaseSetFileName().isEmpty())
        {
            QString dbsfn = QString("%1/databases.xml").arg(mQuerySet.getQuerySetPath());
            logger.infoMsg(tr("migrate '%1' to configuration version 3 ").arg(mQuerySet.getQuerySetFileName()));
            logger.infoMsg(tr("create database set file %1").arg(dbsfn));
            databaseSet.writeXml(dbsfn);
            databaseSet.setDatabaseSetFileName(dbsfn);
            ui->lineEditDatabaseSet->setText(dbsfn);
        }

        if (mQuerySet.rowCount() > 0)
        {
            QuerySetEntry *e = mQuerySet.getShowFirst();
            if (e != nullptr)
            {
                ui->cbQuery->setCurrentText(e->getName());
            }
            else
            {
                ui->cbQuery->setCurrentIndex(0);
            }
        }
        else
        {
            // we start with empty activeQuerySet
            activeQuerySetEntry = nullptr;
        }
    }
    else
    {
        ui->cbRecentQuerySets->removeFirstElement();
    }
}


void SqlReport::generateReadmeAdoc()
{
    QString readme_filename = this->getAbsoluteFileName("Readme.adoc");
    QFile readme_file;

    readme_file.setFileName(readme_filename);

    if (!readme_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        logger.errorMsg(tr("Can't open file '%1' for writing.").arg(readme_filename));
        return;
    }

    QTextStream readme;
    readme.setDevice(&readme_file);
    readme.setEncoding(QStringEncoder::Encoding::Utf8);

    // write asciidoc header
    QFileInfo fn(this->mQuerySet.getQuerySetFileName());
    Common::writeReadmeLine(readme, "= " + fn.baseName(), false);
    Common::writeReadmeLine(readme, ":toc:", false);
    Common::writeReadmeLine(readme, ":toclevels: 1", true);

    if (this->mQuerySet.getQuerySetDescription() != nullptr && this->mQuerySet.getQuerySetDescription().length() > 0)
    {
        Common::writeReadmeLine(readme, this->mQuerySet.getQuerySetDescription(), true);
    }

    QStringList qNames;
    this->mQuerySet.getNames(qNames);

    foreach (QString qn, qNames)
    {
        QuerySetEntry *qs = this->mQuerySet.getByName(qn);
        // section
        Common::writeReadmeLine(readme, "== " + qs->getName(), true);
        Common::writeReadmeLine(readme, qs->getDescription(), true);

        // optional parameter
        QStringList l = qs->getQueryParameter().split("|", Qt::SkipEmptyParts);
        if (l.length() > 0)
        {
            Common::writeReadmeLine(readme, "=== Parameter", true);
            Common::writeReadmeLine(readme, "[%header]", false);
            Common::writeReadmeLine(readme, "|===", false);
            Common::writeReadmeLine(readme, "|Parameter|Value", false);
            foreach (QString p, l) {
                QStringList e = p.split(":=");
                if (e.size() == 2)
                {
                    Common::writeReadmeLine(readme, "|" + e.at(0) + " | " + e.at(1), false);
                }
            }
            Common::writeReadmeLine(readme, "|===", true);
        }

        // options
        Common::writeReadmeLine(readme, "=== Optionen", true);
        Common::writeReadmeLine(readme, tr("* Ausgabedatei `%1`%2")
                                            .arg(qs->getOutputFile(),
                                            qs->getAppendOutput() ? tr(" wird erweitert") : tr(" wird erstellt")), false);
        if (qs->getSqlFile().length() > 0)
            Common::writeReadmeLine(readme, tr("* `%1` enthält die SQL Anweisungen").arg(qs->getSqlFile()), false);
        Common::writeReadmeLine(readme, tr("* `%1` enthält die Musterausgabe").arg(qs->getTemplateFile()), false);
        if (qs->getShowFirst())
            Common::writeReadmeLine(readme, tr("* Standardabfrage bei Programmstart"), false);
        if (qs->getBatchrun())
            Common::writeReadmeLine(readme, tr("* Batch zur Ausführungen weiterer Anweisungen"), false);
        if (qs->getOutputLatin1())
            Common::writeReadmeLine(readme, tr("* Encoding der Ausgabe ist Latin1"), false);
        if (qs->getOutputXml())
            Common::writeReadmeLine(readme, tr("* Ausgabe als XML, String werden automatisch gequotet"), false);
        if (qs->getWithTimestamp())
            Common::writeReadmeLine(readme, tr("* Dateiname bekommt einen Zeitstempel"), false);
        if (! qs->getEmptyNotFound())
            Common::writeReadmeLine(readme, tr("* Unbekannte Variablen erzeugen Fehler in der Ausgabe"), false);
        if (qs->getEmptyNull())
            Common::writeReadmeLine(readme, tr("* NULL wird als leere Zeichenkette verwendet"), false);
        if (qs->getMaxRecursion() > 0)
            Common::writeReadmeLine(readme, tr("* Rekursionstiefe auf %1 festgelegt").arg(qs->getMaxRecursion()), false);

        Common::writeReadmeLine(readme, "", false);
    }

    readme_file.close();
    logger.infoMsg(tr("Readme generated at %1").arg(readme_filename));
}

void SqlReport::switchDisplayMode(QAction *action)
{
    this->showConfiguration = action->data().toInt();

    bool displayState = this->showConfiguration == 0;
    bool displayFocus = displayState || this->showConfiguration == 2;

    ui->cbShowFirst->setVisible(displayFocus);

    // application condiguration elements
    ui->labelLocalParameter->setVisible(displayState);
    ui->frameConfiguration->setVisible(displayState);

    ui->labelDatabaseSet->setVisible(displayState);
    ui->frameDatabaseSet->setVisible(displayState);    

    ui->labelQuerySet->setVisible(displayState);
    ui->frameQuerySet->setVisible(displayState);

    // query configuration elements
    ui->tbDatabase->setVisible(displayFocus);
    ui->dockWidget->setVisible(displayFocus);

    ui->labelQueryParameter->setVisible(displayFocus);
    ui->frameQueryParameter->setVisible(displayFocus);

    ui->labelSql->setVisible(displayFocus);
    ui->frameSql->setVisible(displayFocus);

    ui->labelTemplate->setVisible(displayFocus);
    ui->frameTemplate->setVisible(displayFocus);

    ui->labelCheckBoxes->setVisible(displayFocus);
    ui->frameCheckBoxes->setVisible(displayFocus);

    switchBatchOutputDisplay(ui->checkBoxBatchRun->isChecked());
}

void SqlReport::updateLogger()
{
    logger.setDebugFlag(ui->checkBoxDebug->checkState());
    ui->textEditReport->clear();
    ui->textEditReport->setPlainText("");
    ui->textEditReport->setTextColor(QApplication::palette().text().color());
}

void SqlReport::actionAddQuery()
{
    QInputDialog inputDialog(this);
    inputDialog.setTextEchoMode(QLineEdit::Normal);
    inputDialog.setTextValue(ui->cbQuery->currentText());
    inputDialog.setLabelText(tr("Set Query Name"));
    inputDialog.setOkButtonText(tr("Create"));

    bool ok = inputDialog.exec();
    QString newEntryName = inputDialog.textValue();

    if (ok) // add new query
    {
        if (mQuerySet.contains(newEntryName))
        {
            QMessageBox::information(this, tr("Entry exists!"),
                                     tr("The entry '%1' exists in the set.").arg(newEntryName) );
            return;
        }

        if (newEntryName.isEmpty())
        {
            QMessageBox::information(this, tr("Missing Name"), tr("The entry need a name.") );
            return;
        }

        QuerySetEntry *tmpQSE = new QuerySetEntry();

        if (activeQuerySetEntry != nullptr)
        {
            // save current and use the = operator to copy values from activeQuerySet to tmpQSE
            updateQuerySet();
            *tmpQSE = *activeQuerySetEntry;
        }

        tmpQSE->setName(newEntryName);
        tmpQSE->setShowFirst(false);  // not copy the show first flag

        mQuerySet.append(tmpQSE);
        // minimal one existing entry (index 0)
        ui->cbQuery->model()->sort(0);
        ui->cbQuery->setCurrentText(newEntryName);
    }
}

void SqlReport::actionRenameQuery()
{
    if (activeQuerySetEntry == nullptr)
    {
        QMessageBox::information(this, tr("No entry selected!"), tr("You can rename an existing query only.") );
    }

    QInputDialog inputDialog(this);
    inputDialog.setTextEchoMode(QLineEdit::Normal);
    inputDialog.setTextValue(ui->cbQuery->currentText());
    inputDialog.setLabelText(tr("New Query Name"));
    inputDialog.setOkButtonText(tr("Rename"));

    bool ok = inputDialog.exec();
    QString newEntryName = inputDialog.textValue();

    if (ok) // duplicate or add
    {
        if (mQuerySet.contains(newEntryName))
        {
            QMessageBox::information(this, tr("Entry exists!"),
                                     tr("The entry '%1' exists in the set.").arg(newEntryName) );
            return;
        }

        if (!newEntryName.isEmpty())
        {
            activeQuerySetEntry->setName(newEntryName);
        }
    }
}

void SqlReport::actionDeleteQuery()
{
    if (nullptr != activeQuerySetEntry)
    {
        QMessageBox msgBox;

        msgBox.setText(tr("Remove query '%1'?").arg(activeQuerySetEntry->getName()));
        msgBox.setInformativeText(tr("Used files are not removed!"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        int ret = msgBox.exec();

        if (ret == QMessageBox::Yes)
        {
            QuerySetEntry *tmpQSE = activeQuerySetEntry;
            mQuerySet.remove(tmpQSE);

            if (tmpQSE == activeQuerySetEntry)
            {
                // no new value set by the model, the old QuerySetEntry was
                // deleted, set the active set to null.
                activeQuerySetEntry = nullptr;
            }
        }
    }
    else
    {
        logger.errorMsg(tr("there is no active query, which can removed."));
    }
}

void SqlReport::actionShowCommandQuery()
{
    if (nullptr == activeQuerySetEntry)
    {
        logger.infoMsg(tr("You have to set a query to get the command line."));
        return;
    }

    auto q = [](QString i){ return i.contains(" ") ? QString("\"%1\"").arg(i) : i; };

    updateQuerySet();
    logger.infoMsg(tr("execute this query using the command line "));
    logger.infoMsg(tr("%1 %2 %3 %4 %5 {-p key:=value}").arg(
        q(QCoreApplication::applicationFilePath()),
        q(databaseSet.getDatabaseSetFileName()),
        q(mQuerySet.getQuerySetFileName()),
        q(activeQuerySetEntry->getName()),
        q(activeQuerySetEntry->getDbName())
        ));
}

void SqlReport::cbQuery_currentIndexChanged(int aIndex)
{
    updateQuerySet();
    setActiveQuerySetEntry(ui->cbQuery->itemText(aIndex));
}

void SqlReport::actionEditSql()
{
    if (validQuerySet())
    {
        if (!activeQuerySetEntry->getSqlFile().isEmpty())
        {
            QString pStr = mQuerySet.getQuerySetPath();
            QString fStr = getAbsoluteFileName(activeQuerySetEntry->getSqlFile());

            if (sqlEditor.needUpdate(pStr, fStr))
            {
                if (sqlEditor.maybeSave())
                {
                    sqlEditor.newFile(pStr, fStr);
                }
            }
            Common::widgetToFront(&sqlEditor);
        }
        else
        {
            QMessageBox::information(this, tr("Missing File"),
                                     tr("Please select a file or give a name!") );
        }
    }
}

void SqlReport::actionEditTemplate()
{
    if (validQuerySet())
    {
        if (!activeQuerySetEntry->getTemplateFile().isEmpty())
        {
            QString pStr = mQuerySet.getQuerySetPath();
            QString fStr = getAbsoluteFileName(activeQuerySetEntry->getTemplateFile());

            if (templateEditor.needUpdate(pStr, fStr))
            {
                if (templateEditor.maybeSave())
                {
                    templateEditor.newFile(pStr, fStr);
                }
            }
            Common::widgetToFront(&templateEditor);
        }
        else
        {
            QMessageBox::information(this, tr("Missing File"),
                                     tr("Please select a file or give a name!") );
        }
    }
}

//! If the file not exist, there is no need to show the empty file.
void SqlReport::showOutput()
{
    if (validQuerySet())
    {
        if (!activeQuerySetEntry->getLastOutputFile().isEmpty())
        {
            if (outputEditor.newFile(
                    mQuerySet.getQuerySetPath(),
                    activeQuerySetEntry->getLastOutputFile(),
                    activeQuerySetEntry->getOutputLatin1())
                )
            {
                Common::widgetToFront(&outputEditor);
            }
        }
        else
        {
            QMessageBox::information(this, tr("Missing or Outdated File!"),
                                     tr("Please create output first (Start)\n(Uncheck batch checkbox to see the result.)"));
        }
    }
}

void SqlReport::showTables(bool withReferences)
{
    updateLogger();
    updateQuerySet();

    DbConnection *currentDbConnection = databaseSet.getByName(ui->comboBoxDatabase->currentText());

    if (nullptr != currentDbConnection)
    {
        QString basePath = mQuerySet.getQuerySetPath();
        if (currentDbConnection->connectDatabase(basePath))
        {
            QObject::connect(ui->btnStop, SIGNAL(clicked()), currentDbConnection, SLOT(stopProcess()));
            ui->btnStop->show();

            QTreeReporter treeReporter;

            QElapsedTimer batchTime;
            batchTime.start();

            logger.infoMsg(tr("get %1database structure ...").arg(withReferences ? "extended " : ""));
            treeReporter.setReportRoot(treeModel.invisibleRootItem());
            currentDbConnection->showDatabaseTables(&treeReporter, withReferences);

            logger.infoMsg(tr("ready execution time: %1")
                               .arg(Common::formatMilliSeconds(batchTime.elapsed())));

            currentDbConnection->closeDatabase();
            ui->btnStop->hide();
            QObject::disconnect(ui->btnStop, SIGNAL(clicked()), currentDbConnection, SLOT(stopProcess()));

            if (ui->dockWidget->isHidden()) ui->dockWidget->show();
        }
    }
}


void SqlReport::actionShowTables()
{
    showTables(false);
}

void SqlReport::actionShowTablesPlus()
{
    showTables(true);
}

//! After pressing the exit button we close the
//! window and the destructor writes the last
//! query set and close the database.
void SqlReport::exitApplication()
{
    this->close();
}

void SqlReport::editDescription()
{
    if (activeQuerySetEntry != nullptr)
    {
        updateQuerySet();
        descriptionEditor.setText(activeQuerySetEntry->getDescription(), false);
        descriptionEditor.show();
    }
    else
    {
        logger.infoMsg("please create a query to add a description");
    }
}

void SqlReport::querysetDescription()
{
    descriptionEditor.setText(mQuerySet.getQuerySetDescription(), true);
    descriptionEditor.show();
}

//! Es werden die Werte der Oberfläche in das aktive QuerySet übernommen.
void SqlReport::updateQuerySet()
{
    if ( nullptr != activeQuerySetEntry)
    {
        activeQuerySetEntry->setDbName(ui->comboBoxDatabase->currentText());
        activeQuerySetEntry->setQueryParameter(ui->lineEditQueryParameter->text());
        activeQuerySetEntry->setSqlFile(ui->outSql->text());
        activeQuerySetEntry->setTemplateFile(ui->outTemplate->text());
        activeQuerySetEntry->setOutputFile(ui->output->text());
        activeQuerySetEntry->setBatchrun(ui->checkBoxBatchRun->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setWithTimestamp(ui->cbTimeStamp->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setAppendOutput(ui->cbAppendOutput->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setOutputLatin1(ui->cbOutputLatin1->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setOutputXml(ui->cbOutputXml->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setEmptyNull(ui->cbEmptyNull->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setEmptyNotFound(ui->checkBoxNotFound->checkState()==Qt::Checked?true:false);
        activeQuerySetEntry->setMaxRecursion(ui->lineEditMaxRecursion->text());
        mQuerySet.setShowFirst(activeQuerySetEntry, ui->cbShowFirst->checkState()==Qt::Checked);
    }
}

//! Check if we have an active query set
bool SqlReport::validQuerySet()
{
    bool result = activeQuerySetEntry != nullptr;

    if (!result)
    {
        QMessageBox::information(this, tr("No active query entry"),
                                 tr("Please select a query or create a new."));
    }
    else
    {
        // before doing an action we update the queryset entry with the current UI settings
        updateQuerySet();
    }

    return result;
}


//! Diese Methode setzt die Werte aus dem aktiven QuerySet in der Oberfläche.
//! Wird keine solcher Index gefunden, werden alle Felder gelöscht.
void SqlReport::setActiveQuerySetEntry(const QString &aIdxName)
{
    activeQuerySetEntry = mQuerySet.getByName(aIdxName);
    if (activeQuerySetEntry != nullptr)
    {
        ui->comboBoxDatabase->setCurrentText(activeQuerySetEntry->getDbName());
        ui->lineEditDescr->setText(Common::getDescriptionHeadline(activeQuerySetEntry->getDescription()));
        ui->lineEditQueryParameter->setText(activeQuerySetEntry->getQueryParameter());
        ui->outSql->setText(activeQuerySetEntry->getSqlFile());
        ui->outTemplate->setText(activeQuerySetEntry->getTemplateFile());
        ui->output->setText(activeQuerySetEntry->getOutputFile());
        ui->checkBoxBatchRun->setCheckState(activeQuerySetEntry->getBatchrun()?Qt::Checked:Qt::Unchecked);
        ui->cbTimeStamp->setCheckState(activeQuerySetEntry->getWithTimestamp()?Qt::Checked:Qt::Unchecked);
        ui->cbAppendOutput->setCheckState(activeQuerySetEntry->getAppendOutput()?Qt::Checked:Qt::Unchecked);
        ui->cbOutputLatin1->setCheckState(activeQuerySetEntry->getOutputLatin1()?Qt::Checked:Qt::Unchecked);
        ui->cbOutputXml->setCheckState(activeQuerySetEntry->getOutputXml()?Qt::Checked:Qt::Unchecked);
        ui->cbEmptyNull->setCheckState(activeQuerySetEntry->getEmptyNull()?Qt::Checked:Qt::Unchecked);
        ui->checkBoxNotFound->setCheckState(activeQuerySetEntry->getEmptyNotFound()?Qt::Checked:Qt::Unchecked);
        ui->cbShowFirst->setCheckState(activeQuerySetEntry->getShowFirst()?Qt::Checked:Qt::Unchecked);
        ui->lineEditMaxRecursion->setText(QString("%1").arg(activeQuerySetEntry->getMaxRecursion()));
    }
    else
    {
        ui->comboBoxDatabase->setCurrentIndex(0);
        ui->lineEditQueryParameter->setText("");
        ui->lineEditDescr->setText("");
        ui->outSql->setText("");
        ui->outTemplate->setText("");
        ui->output->setText("");
        ui->checkBoxBatchRun->setCheckState(Qt::Unchecked);
        ui->cbTimeStamp->setCheckState(Qt::Unchecked);
        ui->cbAppendOutput->setCheckState(Qt::Unchecked);
        ui->cbOutputLatin1->setCheckState(Qt::Unchecked);
        ui->cbOutputXml->setCheckState(Qt::Unchecked);
        ui->cbShowFirst->setCheckState(Qt::Unchecked);
        ui->lineEditMaxRecursion->setText(QString("%1").arg(QuerySetEntry::defaultMaxRecursion));
    }

    switchBatchOutputDisplay(ui->checkBoxBatchRun->isChecked());
}

void SqlReport::showAbout()
{
    aboutDialog.show();
}

void SqlReport::import_database_list_file(bool mergeFile)
{
    bool selectCancel;
    QString startFolder = databaseSet.getDatabaseSetFileName();

    if (startFolder.isEmpty())
    {
        startFolder = mQuerySet.getQuerySetFileName();
    }

    QString importFile = selectFile("Please select Database file",
                                    startFolder,
                                    tr("XML-Files (*.xml)"), false, selectCancel);


    if (!selectCancel && !importFile.isEmpty())
    {
        databaseSet.readDatabaseSet(importFile, mergeFile);
        ui->lineEditDatabaseSet->setText(databaseSet.getDatabaseSetFileName());
    }
}

void SqlReport::new_database_list()
{
    QString dbsName = QFileDialog::getSaveFileName(this, tr("Please select new Databases file"),
                                                  mQuerySet.getQuerySetFileName(),
                                                  tr("XML Files (*.xml)"));

    if (!dbsName.isEmpty())
    {
        QString lastDbsName = databaseSet.getDatabaseSetFileName();

        if (dbsName == lastDbsName)
        {
            logger.infoMsg(tr("selecting the same database again -- do nothing"));
        }
        else
        {
            // store the current and remove all entries
            databaseSet.writeXml("");
            databaseSet.clear();
            databaseSet.setDatabaseSetFileName(dbsName);
            ui->lineEditDatabaseSet->setText(databaseSet.getDatabaseSetFileName());
        }
    }
}

void SqlReport::import_database_list()
{
    import_database_list_file(false);
}

void SqlReport::merge_database_list()
{
    import_database_list_file(true);
}

void SqlReport::actionRemoveQuerySet()
{
    ui->cbRecentQuerySets->removeFirstElement();
    if (ui->cbRecentQuerySets->getList().size() == 0)
    {
        // we removed the last entry, also cleanup the UI
        activeQuerySetEntry = nullptr;
        mQuerySet.clear();
        setActiveQuerySetEntry("");
    }
}


void SqlReport::parameterDialog()
{
    if (activeQuerySetEntry == nullptr)
    {
        logger.infoMsg("Please create a query set to edit parameters ...");
        return;
    }

    updateQuerySet();
    ParameterModel pm(this);
    auto db = databaseSet.getByName(activeQuerySetEntry->getDbName());

    pm.importCategory(ParameterEntry::Local, ui->lineEditLocalParameter->text() , false);
    pm.importCategory(ParameterEntry::Query, activeQuerySetEntry->getQueryParameter(), false);
    pm.importCategory(ParameterEntry::Database, db->getDatabaseParameter() , false);

    ParameterDialog d(&pm, this);
    d.setInfoLabel(activeQuerySetEntry->getName(), activeQuerySetEntry->getDbName());

    d.show();
    int res = d.exec();

    if (QDialog::Accepted == res)
    {
        ui->lineEditLocalParameter->setText(pm.exportCategory(ParameterEntry::Local));
        QString qp = pm.exportCategory(ParameterEntry::Query);
        activeQuerySetEntry->setQueryParameter(qp);
        ui->lineEditQueryParameter->setText(qp);
        db->setDatabaseParameter(pm.exportCategory(ParameterEntry::Database));
    }

    pm.clearParameter();
}


//! Exportiere die benötigten Dateien für alle aktuellen Queries.
void SqlReport::actionExportQuerySet()
{
    // update and write before export, because we copy files only
    updateLogger();
    updateQuerySet();
    mQuerySet.writeXml("");

    if (mQuerySet.size() == 0)
    {
        logger.infoMsg("Please create a query set to export ...");
        return;
    }

    QFileInfo querysetFileInfo(mQuerySet.getQuerySetFileName());
    QString basePath = querysetFileInfo.absolutePath();
    QStringList qsl;
    mQuerySet.getNames(qsl);

    QStringList exportFileList;
    quint32 missingFiles = 0;

    logger.infoMsg(tr("export using base path '%1'").arg(basePath));

    foreach (auto qsn, qsl)
    {
        QStringList exportCandidateList;

        QuerySetEntry *qse = mQuerySet.getByName(qsn);
        exportCandidateList.append(qse->getTemplateFile());
        exportCandidateList.append(qse->getSqlFile());

        // using a while loop to handle recursive included files
        while (exportCandidateList.size() > 0)
        {
            QStringList includeList;

            foreach (QString cfn, exportCandidateList)
            {

                if (exportFileList.contains(cfn) || cfn.isEmpty())
                {
                    // duplicate file or empty (possible for SQL files)
                    continue;
                }

                QFile inputFile(QString("%1/%2").arg(basePath,cfn));
                if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    logger.errorMsg(tr("can't open export file '%1' for query '%2'")
                                    .arg(cfn, qsn));
                    missingFiles++;
                }

                QTextStream streamInSql(&inputFile);
                while ( !streamInSql.atEnd())
                {
                    QString line = streamInSql.readLine().trimmed();
                    if (line.startsWith("::<"))
                    {
                        QString fnp = line.mid(3).trimmed();
                        QString fn = Common::findFile(basePath, fnp);
                        if ( !fn.isEmpty())
                        {
                            includeList.append(fnp);
                        }
                        else
                        {
                            missingFiles++;
                        }
                    }
                }

                exportFileList.append(cfn);
            }

            exportCandidateList.clear();
            exportCandidateList << includeList;
            includeList.clear();
        }
    }

    if ( missingFiles > 0)
    {
        logger.errorMsg(tr("missing %1 -- stop export.")
                        .arg(missingFiles > 1 ? "files" : "file"));
        return;
    }

    QString exportFolder = QFileDialog::getExistingDirectory(this,
                                                             tr("Please select Output Directory"),
                                                             basePath);

    if (exportFolder.isEmpty())
    {
        logger.errorMsg("missing output folder -- stop export");
        return;
    }

    if (exportFolder == basePath)
    {
        logger.errorMsg("can't export to the same folder -- stop export");
        return;
    }

    exportFileList.append(querysetFileInfo.fileName());
    foreach (auto efn, exportFileList)
    {
        QString exportFile(QString("%1/%2").arg(exportFolder, efn));

        QFileInfo fileOutInfo(exportFile);
        if (!fileOutInfo.absoluteDir().exists())
        {
            // need to create target folders
            fileOutInfo.absoluteDir().mkpath(fileOutInfo.absoluteDir().absolutePath());
        }

        if ( QFile::exists(exportFile))
        {
            // copy not overwrite files, remove existing files
            QFile::remove(exportFile);
        }

        QFile::copy(QString("%1/%2").arg(basePath, efn), exportFile );
        logger.infoMsg(tr("export '%1'").arg(efn));
    }

    logger.infoMsg(tr("files of Query Set exported to '%1'").arg(exportFolder));
}

void SqlReport::languageSwitchEnglish()
{
    this->interface_language = "EN";
    QMessageBox::information(this, "Language Switch", "Please restart the application to activate the new language.");
}

void SqlReport::languageSwitchGerman()
{
    this->interface_language = "DE";
    QMessageBox::information(this, "Sprachumstellung", "Bitte die Applikation neu Starten um Deutsch zu aktivieren.");
}
