#include "query_set_entry.h"
#include "QDebug"

#include <QRegularExpression>

QuerySetEntry::QuerySetEntry() :
    logger(LogMessage::instance()),
	name(""),
	dbname(""),
	description(""),
    queryParameter(""),
	sqlFile(""),
	templateFile(""),
	outputFile(""),
	lastOutputFile(""),
	batchrun(false),
	withTimestamp(true),
	appendOutput(false),
    outputLatin1(false),
	outputXml(false),
    showFirst(false),
    emptyNull(false),
    emptyNotFound(false),
    maxRecursion(defaultMaxRecursion),
    databaseEncoding("")
{
}

QuerySetEntry::~QuerySetEntry()
{

}

QuerySetEntry &QuerySetEntry::operator=(const QuerySetEntry &rhs)
{
	if (this != &rhs)
	{
		name          = rhs.name;
		dbname        = rhs.dbname;
        description   = rhs.description;
        queryParameter  = rhs.queryParameter;
		sqlFile       = rhs.sqlFile;
		templateFile  = rhs.templateFile;
		outputFile    = rhs.outputFile;
		lastOutputFile= "";
		batchrun      = rhs.batchrun;
		withTimestamp = rhs.withTimestamp;
        outputLatin1  = rhs.outputLatin1;
		outputXml     = rhs.outputXml;
		appendOutput  = rhs.appendOutput;
        maxRecursion  = rhs.maxRecursion;
        showFirst     = false;
        emptyNull     = false;
        emptyNotFound = false;
	}
	return *this;
}

//! Comparsion of two entries is done by the comparsion
//! of its names.
bool QuerySetEntry::operator()(const QuerySetEntry *l, const QuerySetEntry *r)
{
	return (l->getName() < r->getName());
}

void QuerySetEntry::setName(const QString &value)
{
	name = value;
}

void QuerySetEntry::setDbName(const QString &value)
{
	dbname = value;
}

QString QuerySetEntry::getDescription() const
{
    return description;
}


void QuerySetEntry::setDescription(const QString &value)
{
	description = value;
}

QString QuerySetEntry::getQueryParameter() const
{
    return queryParameter;
}

void QuerySetEntry::setQueryParameter(const QString &value)
{
    queryParameter = value;
}

QString QuerySetEntry::getSqlFile() const
{
	return sqlFile;
}

void QuerySetEntry::setSqlFile(const QString &value)
{
	sqlFile = value;
}

QString QuerySetEntry::getTemplateFile() const
{
	return templateFile;
}

void QuerySetEntry::setTemplateFile(const QString &value)
{
	templateFile = value;
}

QString QuerySetEntry::getOutputFile() const
{
	return outputFile;
}

void QuerySetEntry::setOutputFile(const QString &value)
{
	outputFile = value;
}

QString QuerySetEntry::getLastOutputFile() const
{
	return lastOutputFile;
}

void QuerySetEntry::setLastOutputFile(const QString &value)
{
	lastOutputFile = value;
}

bool QuerySetEntry::getBatchrun() const
{
	return batchrun;
}

void QuerySetEntry::setBatchrun(bool value)
{
	batchrun = value;
}

bool QuerySetEntry::getWithTimestamp() const
{
	return withTimestamp;
}

void QuerySetEntry::setWithTimestamp(bool value)
{
	withTimestamp = value;
}

bool QuerySetEntry::getAppendOutput() const
{
	return appendOutput;
}

void QuerySetEntry::setAppendOutput(bool value)
{
	appendOutput = value;
}

bool QuerySetEntry::getOutputXml() const
{
    return outputXml;
}

void QuerySetEntry::setOutputXml(bool value)
{
	outputXml = value;
}

bool QuerySetEntry::getShowFirst() const
{
    return showFirst;
}

void QuerySetEntry::setShowFirst(bool value)
{
    showFirst = value;
}

bool QuerySetEntry::getEmptyNull() const
{
    return emptyNull;
}

void QuerySetEntry::setEmptyNull(bool value)
{
    emptyNull = value;
}

bool QuerySetEntry::getEmptyNotFound() const
{
    return emptyNotFound;
}

void QuerySetEntry::setEmptyNotFound(bool value)
{
    emptyNotFound = value;
}

quint32 QuerySetEntry::getMaxRecursion() const
{
    return maxRecursion;
}

void QuerySetEntry::setMaxRecursion(QString value)
{
    bool bOk = false;
    int i = value.toInt(&bOk);
    maxRecursion = bOk ? i : defaultMaxRecursion;
}

void QuerySetEntry::setMaxRecursion(quint32 value)
{
    maxRecursion = value;
}

