#pragma once

#include "resultset.h"
#include "logmessage.h"

class ResultSetList : public ResultSet
{
    Q_OBJECT

public:
    explicit ResultSetList(const QByteArray &listStr,
                           const QString &separator, const QString &pname);
    virtual ~ResultSetList() override;

    virtual QString name() override { return "List Result"; };
    virtual QString lastError() override;
    virtual bool updateParameter(QHash <QString, QByteArray> &replacements,
                                 QuerySetEntry *mQSE) override;

private:
    LogMessage &logger;
    QList<QByteArray> resultList;
    quint32 listIndex;
    QString fieldName;
    QMap<QString, QChar> sepMap = {{"\\n", '\n'}, {"\\t", '\t'}};
};

