#pragma once

#include "QtSql/qsqlerror.h"
#include "QtSql/qsqlquery.h"
#include "QtSql/qsqlrecord.h"
#include "query_set_entry.h"
#include "logmessage.h"
#include "resultset.h"

class ResultSetSql : public ResultSet
{
    Q_OBJECT

public:
    explicit ResultSetSql(QString dbName, QString &queryStr);
    virtual ~ResultSetSql() override;

    virtual QString name() override { return "SQL Database Results"; };
    virtual QString lastError() override;
    virtual bool updateParameter(QHash <QString, QByteArray> &replacements, QuerySetEntry *mQSE) override;
    virtual void finishResultSet(QHash <QString, QByteArray> &replacements) override;

private:
    QString decode(const QByteArray &data, const QString &encoding);

    LogMessage &logger;
    bool firstResult;
    QSqlQuery query;  // hold the sql query
    QSqlRecord rec;
    QString sqlQuery;
};
