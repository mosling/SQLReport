#pragma once

#include "logmessage.h"
#include <QObject>
#include <QString>
#include <QMap>
#include <QHash>
#include <QByteArray>

class LookupList : public QObject
{
    Q_OBJECT

public:

    explicit Q_INVOKABLE LookupList(QObject *parent = nullptr);

    void insert_value(QString mapName, QString key, QString value);

    int mapSize(QString mapName);
    bool hasMapEntry(QString mapName);
    bool hasMapKey(QString mapName, QString key);

    Q_INVOKABLE bool hasKey(QString mapName, QString key);
    Q_INVOKABLE QString getValue(QString mapName, QString key);

private:
    LogMessage &logger;

    QMap<QString, QHash<QString, QString> > lookup_map;
};

