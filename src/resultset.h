#pragma once

#include "query_set_entry.h"

#include <QObject>
#include <QHash>

//! ResultSet handles the
class ResultSet : public QObject
{
    Q_OBJECT

public:
    explicit ResultSet();
    virtual ~ResultSet() override;

    virtual QString name() = 0;
    void setValid(bool b) { validResultSet = b; }
    bool isValid() { return validResultSet; }
    void setEmptyRow(bool b) { emptyResultRow = b; }
    bool wasEmptyRow() { return emptyResultRow; }
    void setEmptySet(bool b) { emptyResultSet = b; }
    bool wasEmptySet() { return emptyResultSet; }
    virtual QString lastError() = 0;

    virtual bool updateParameter(QHash <QString, QByteArray> &replacements, QuerySetEntry *mQSE) = 0;
    virtual void saveParameter(QHash<QString, QByteArray> &replacements, QString &parameter_name);
    virtual void finishResultSet(QHash <QString, QByteArray> &replacements);

protected:
    QHash<QString, QByteArray> overwrittenReplacements;
    QStringList insertedReplacementsKeys;

private:
    bool validResultSet;
    bool emptyResultRow;
    bool emptyResultSet;

};
