#pragma once

#include <QObject>
#include <QTextEdit>

class LogMessage : public QObject
{
public:
    static LogMessage & instance() {
        static LogMessage * _instance = nullptr;
        if ( _instance == nullptr ) {
            _instance = new LogMessage();
            qInstallMessageHandler(windowMessageHandler);
        }
        return *_instance;
    }

private:
    LogMessage();
    ~LogMessage() override;
    LogMessage( const LogMessage& ) = delete;
    LogMessage& operator=( const LogMessage& ) = delete;

    static QTextStream& qStdOut();
    static void windowMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

public:
    static void setMsgWindow(QTextEdit *te);
    static void setDebugFlag(Qt::CheckState flag);
    static void setContext(const QString &contextString);
    static void setQuerysetName(const QString &str);

    static bool isDebug() { return debugOutput; }
    static bool isTrace() { return traceOutput; }

    enum class LogLevel {ERR, WARN, MSG, DBG, TRACE};

    static void traceMsg(const QString &vMsgStr) { showMsg(vMsgStr, LogLevel::TRACE); }
    static void debugMsg(const QString &vMsgStr) { showMsg(vMsgStr, LogLevel::DBG); }
    static void infoMsg(const QString &vMsgStr) { showMsg(vMsgStr, LogLevel::MSG); }
    static void warnMsg(const QString &vMsgStr) { showMsg(vMsgStr, LogLevel::WARN); }
    static void errorMsg(const QString &vMsgStr) { showMsg(vMsgStr, LogLevel::ERR); }

private:
    static QString getContextString();
    static void showMsg(const QString &vMsgStr, LogLevel ll);

    static QTextEdit *mMsgWin;

    static bool debugOutput;
    static bool traceOutput;

    static QString context;
    static QString querysetname;
};
