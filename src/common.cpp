#include <QDebug>
#include <QRandomGenerator>
#include <QString>
#include <QFileInfo>
#include <QSettings>
#include <common.h>

#include "QtCore"
#include "QtCore/qprocess.h"
#include "QtCore/qregularexpression.h"
#include "logmessage.h"
#include "qttranslation.h"

Common::Common()
{

}

void Common::widgetToFront(QWidget *widget)
{
    widget->show();
    widget->setWindowState( (widget->windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
    widget->raise();
}

//! \param ms time intervall in milli seconds
//! \return formatted string 1h 0m 23s 120ms
QString Common::formatMilliSeconds(qint64 ms)
{
    qint64 sec = ms / 1000;
    ms = ms - (sec * 1000);
    qint64 min = sec / 60;
    sec = sec - min * 60;
    qint64 hrs = min / 60;
    min = min - hrs * 60;

    bool b = false;
    QString s = "";
    if(hrs > 0)      { s += QString("%1h ").arg(hrs); b = true; }
    if(min > 0 || b) { s += QString("%1m ").arg(min); b = true; }
    if(sec > 0 || b) { s += QString("%1s ").arg(sec); b = true; }
    if(ms  > 0 || b) { s += QString("%1ms").arg(ms); }
    if(ms == 0 && s.isEmpty()) {s = "0ms"; }

    return s;
}

void Common::quoteXmlString(QByteArray &str)
{
    str.replace("&", "&amp;");
    str.replace("<", "&lt;");
    str.replace(">","&gt;");
    str.replace("\"", "&quot;");
}

void Common::quoteJsonString(QString &json_string)
{
    json_string
            .replace('"', "\\\"")
            .replace('\n', "\\\\n");
}

QString Common::randomString(quint32 size)
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

    QString randomString;
    for(quint32 i=0; i < size; ++i)
    {
        qint64 index = QRandomGenerator::global()->bounded(possibleCharacters.length());
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}

// This brings absolute no security but prevents the viewer from the project
// configuration file to see the password.
// Also add some salt to see different base64 encoded values for fun :)
QString Common::obscurePassword(const QString &pwd, const QString &salt)
{
    qDebug() << "password = " << pwd;

    QString salted = "";
    for(int i = 0; i< pwd.length(); i++)
    {
        salted = QString("%1%2%3").arg(salted, pwd.at(i),
                                       salt.length() > i ? salt.at(i) : Common::randomString(1) );
    }

    auto encoded = salted.toUtf8().toBase64();
    return encoded;
}

QString Common::extractPassword(const QString &str, bool saltOnly)
{
    QString pwdString = QString(QByteArray::fromBase64(str.toUtf8()));
    QString pwd = "";
    int start = saltOnly ? 1 : 0;
    for(int i = start; i< pwdString.length(); i=i+2)
    {
        pwd = QString("%1%2").arg(pwd, pwdString.at(i));
    }

    return pwd;
}

//! Using the file name and a base path to find the file name.
//! 1. the file exists and will be returned
//! 2. the basepath/filename exists and this is returned
//! 3. no of the two file positions exists and empty string is returned
QString Common::findFile(const QString &basePath, const QString &filename)
{
    LogMessage &logger = LogMessage::instance();
    if (!QFileInfo::exists(filename))
    {
        QString nf = QString("%1/%2").arg(basePath, filename);
        if (!QFileInfo::exists(nf))
        {
            logger.warnMsg(QString("file '%1' not found").arg(filename));
            if (!basePath.isEmpty())
            {
                logger.warnMsg(QString("... and file '%1' not found").arg(nf));
            }
            return "";
        }
        return nf;
    }
    return QFileInfo(filename).absoluteFilePath();
}

void Common::writeReadmeLine(QTextStream &stream, QString line, bool withEmptyLine)
{
    stream << line << Qt::endl;
    if (withEmptyLine)
    {
        stream << Qt::endl;

    }
}

QString Common::getDescriptionHeadline(const QString &description, qsizetype maxLength)
{
    if (!description.isEmpty())
    {
        QString result = description;
        qsizetype x;

        if ((x = description.indexOf("\n", 0)))
        {
            result = description.left(x);
        }

        else if (( x = description.indexOf(".", 0)))
        {
            result =  description.left(x);
        }

        if (result.length() > maxLength)
        {
            result = result.left(maxLength);
            result += " ... <more>";
        }

        return result;
    }

    return "";
}

//! Erzeugen einer Liste von Strings, deren maximale Breite
//! width ist.
QStringList Common::splitString(const QString &str, int width, const QString &startOfLine)
{
    qsizetype len = str.length();
    QStringList l;
    QString sol("");

    qint32 idx   = 0;
    qint32 start = 0;
    qint32 split = 0;

    while (idx < len)
    {
        // gut Möglichkeiten zum Beginn einer neuen Zeile merken
        if (str[idx]==' ' || str[idx]==',' || str[idx]=='\t')
        {
            split = idx;
        }

        // existiert schon Zeilenumbruch, dann wird er übernommen
        if (str[idx] == QChar(0x0a) || str[idx] == QChar(0x0d))
        {
            l.append(sol + str.mid(start, idx-start).trimmed());
            sol = startOfLine;
            idx++;
            start = idx;
            split = start;
        }
        else if ((idx-start) != 0 && (idx-start) % width == 0)
        {
            // Eintrag erzeugen
            if (split == start) split = idx;
            l.append(sol + str.mid(start, split-start).trimmed());
            sol = startOfLine;
            start = split;
            split = start;
        }

        idx++;
    }

    if ((len-start) > 0)
    {
        l.append(sol + str.mid(start).trimmed());
    }

    return l;
}

//! Convert a number given as string into a 32Bit unsigned integer. The
//! representation follows the c++ convention with some addtitional enhancements:
//! * number can be grouped by underlines
//! * interprets 0b as binary number (0 and 1 allowed only)
//! * prefix $ and suffix h or H identify a hexadecimal number
qsizetype Common::convertToNumber(QString aNumStr, bool &aOk)
{
    qsizetype vRes = 0x0;
    qsizetype vBase = 10;  // Basis der Zahl 2, 10 oder 16
    aOk = true;
    QRegularExpression binValue("[01]+");

    aNumStr.replace("_","");
    aNumStr = aNumStr.trimmed();

    if (aNumStr.startsWith("0x") || aNumStr.startsWith("0X") )
    {
        aNumStr.remove(0,2);
        vBase = 16;
    }
    else if (aNumStr.startsWith("$"))
    {
        aNumStr.remove(0,1);
        vBase = 16;
    }
    else if (aNumStr.endsWith("H") || aNumStr.endsWith("h") )
    {
        aNumStr.truncate(aNumStr.length()-1);
        vBase = 16;
    }
    else if (aNumStr.startsWith("0b") || aNumStr.startsWith("0B") )
    {
        aNumStr.remove(0,2);
        vBase = 2;
    }

    if (16 == vBase)
    {
        vRes = static_cast<quint32>(aNumStr.toUInt(&aOk, 16));
    }
    else if (2 == vBase)
    {
        if (binValue.match(aNumStr).hasMatch())
        {
            qsizetype l = aNumStr.length() - 1;
            for (qsizetype i = l; i >= 0; --i)
            {
                vRes <<= 1;
                if ('1' == aNumStr[i])
                {
                    vRes |= 0x1;
                }
            }
        }
        else
        {
            aOk = false;
        }
    }
    else
    {
        vRes = static_cast<quint32>(aNumStr.toUInt(&aOk, 10));
    }

    if (!aOk)
    {
        vRes = 0;
    }

    return vRes;
}

QString Common::convertRtf(QString rtfText, QString resultType, bool cleanupFont, bool xmlOutput)
{
    QString unrtfCmd="unrtf";

    LogMessage &logger = LogMessage::instance();
    QRegularExpression fontElement("<[/]*font[^>]*>");
    QRegularExpression spanElement("<[/]*span[^>]*>");
    QRegularExpression htmlBody("<body[^>]*>((.|[\\n\\r])*)</body>");

    QProcess unrtfVersion;
    QStringList unrtfArgs;
    unrtfArgs.append("--version");
    unrtfVersion.setProgram(unrtfCmd);
    unrtfVersion.setArguments(unrtfArgs);
    unrtfVersion.start();
    bool started = false;
    if (!unrtfVersion.waitForFinished(10000))
    {
        // 10 seconds timeout
        unrtfVersion.kill();
    }
    else
    {
        started = unrtfVersion.exitCode() == 0;
    }

    if(started)
    {
        if (logger.isDebug())
        {
            logger.debugMsg(QString("using unrtf version %1 to convert rtf -> %2")
                                .arg(QString::fromLocal8Bit(unrtfVersion.readAllStandardError()).trimmed(), resultType));
        }

        // write to a temporary file
        QTemporaryFile tempFile;
        if(tempFile.open())
        {
            QTextStream out(&tempFile);
            out << rtfText.toLatin1();
            tempFile.close();
        }
        else
        {
            logger.errorMsg(QString("Failure creating remporary file '%1'")
                                .arg(tempFile.fileName()));
            return "";
        }

        QProcess program;
        unrtfArgs.clear();
        unrtfArgs.append(QString("--%1").arg(resultType.toLower()));
        unrtfArgs.append(tempFile.fileName());
        program.setProgram(unrtfCmd);
        program.setArguments(unrtfArgs);
        program.start();
        int exitCode = 1;
        QString stdError;
        QString stdOutput = "";
        if (!program.waitForFinished(10000))
        {
            // 10 seconds timeout
            program.kill();
            stdError = "RTF converter not ready within 10s.";
        }
        else
        {
            exitCode = program.exitCode();
            stdOutput = QString::fromLocal8Bit(program.readAllStandardOutput());
            stdError = QString::fromLocal8Bit(program.readAllStandardError());
        }

        if (exitCode != 0)
        {
            logger.errorMsg(QString("%1").arg(stdError));
        }
        else
        {
            // extract body content from generated html
            QRegularExpressionMatch htmlMatch = htmlBody.match(stdOutput);
            if (htmlMatch.hasMatch())
            {
                if (cleanupFont)
                {
                    QString fontless = htmlMatch.captured(1).remove(fontElement);
                    return fontless.remove(spanElement);
                }
                else
                {
                    return htmlMatch.captured(1);
                }
            }
        }
    }
    else
    {
        logger.warnMsg("Please add unrtf to your PATH for better results.");
        QRegularExpression rtfTextExp("lang1031 ([^}]*)");
        QRegularExpressionMatch rtfMatch= rtfTextExp.match(rtfText);
        if (rtfMatch.hasMatch())
        {
            QString part = rtfMatch.captured(1);

            QMap<QString, QString> rtfToHtml;
            rtfToHtml.insert("\\'f6", "ö");
            rtfToHtml.insert("\\'e4", "ä");
            rtfToHtml.insert("\\'fc", "ü");
            rtfToHtml.insert("\\'c4", "Ä");
            rtfToHtml.insert("\\'d6", "Ö");
            rtfToHtml.insert("\\'dc", "Ü");
            rtfToHtml.insert("\\'df", "ß");
            if (xmlOutput)
            {
                rtfToHtml.insert("\\par", "</p>");
            }
            else
            {
                rtfToHtml.insert("\\par", "");
            }

            QMapIterator<QString, QString> iter(rtfToHtml);
            while(iter.hasNext())
            {
                iter.next();
                part.replace(iter.key(), iter.value());
            }
            return part;
        }
    }

    return "";
}

QDomElement Common::openXmlFile(QString aFilename)
{
    LogMessage &logger = LogMessage::instance();

    if (aFilename.isEmpty())
    {
        // ignore empty files
        return QDomElement();
    }

    QDomDocument doc;
    QFile file(aFilename);

    if (!file.open(QIODevice::ReadOnly))
    {
        logger.errorMsg(QT_TR_NOOP(QString("can't open XML file '%1'")).arg(aFilename));
        return QDomElement();
    }

    QDomDocument::ParseResult result = doc.setContent(&file);

    if (!result)
    {
        logger.errorMsg(QT_TR_NOOP(QString("error set XML content at line %1:%2 (%3)"))
                          .arg(result.errorLine)
                          .arg(result.errorColumn)
                          .arg(result.errorMessage) );
        file.close();
        return QDomElement();
    }

    return doc.documentElement();
}

qsizetype Common::getXmlVersion(QDomElement dom)
{
    QDomElement version = dom.firstChildElement("version");
    qsizetype version_number = 0;

    if (!version.isNull())
    {
        bool bOk;
        version_number = version.text().toUInt(&bOk);
        if (!bOk)
        {
            version_number = 0;
        }
    }

    return version_number;
}


void Common::writeXmlHeader(QXmlStreamWriter &aStream)
{
    aStream.setAutoFormatting(true);
    aStream.writeStartDocument("1.0");

    aStream.writeStartElement("SqlReport");
    aStream.writeTextElement("version", Common::versionString());

}

QStringList Common::availableDrivers()
{
    QStringList dl;
    dl.append(QSqlDatabase::drivers());
    dl.append(Common::mssqlName());
    return dl;
}
