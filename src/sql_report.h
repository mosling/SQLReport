#pragma once

#include "editwidget.h"
#include "query_executor.h"
#include "query_set.h"
#include "db_connection_set.h"
#include "editdialog.h"
#include "about_dialog.h"

#include <QMainWindow>
#include <QStandardItemModel>
#include <QStringListModel>
#include <QActionGroup>

namespace Ui {
class SqlReport;
}

class SqlReport : public QMainWindow
{
	Q_OBJECT

public:
    explicit SqlReport(QWidget *parentObj = nullptr,
                       Qt::WindowFlags = Qt::WindowType::Widget);
    ~SqlReport() override;

    void setDescription(QString str, bool isQueryDescription);

protected:
    void closeEvent(QCloseEvent *event) override;
    void changeEvent(QEvent *event) override;
    void setActiveQuerySetEntry(const QString &aIdxName);
    void updateQuerySet();

private slots:
    void start_execution();

    // Query Set
    void actionLoadQuerySet();
    void actionNewQuerySet();
    void actionExportQuerySet();
    void actionRemoveQuerySet();
    void readQuerySet(const QString &qsName);

    // Query
    void cbQuery_currentIndexChanged(int);
    void actionAddQuery();
    void actionRenameQuery();
    void actionDeleteQuery();
    void actionShowCommandQuery();

    // Database Set
    void new_database_list();
    void import_database_list();
    void merge_database_list();

    // Database

    void actionEditDatabase();
    void actionShowTables();
    void actionShowTablesPlus();
    void actionAddDatabase();
    void actionDeleteDatabase();

    // SQL
    void actionEditSql();
    void actionLoadSql();

    // Template
    void actionEditTemplate();
    void actionLoadTemplate();

    // other
    void exitApplication();
    void editDescription();
    void showOutput();
    void showAbout();
    void switchDisplayMode(QAction *action);
    void switchBatchOutputDisplay(bool checked);
    void generateReadmeAdoc();
    void parameterDialog();
    void querysetDescription();

    void languageSwitchEnglish();
    void languageSwitchGerman();

private:
    QString stylesheet;
	bool validQuerySet();
	QString getAbsoluteFileName(QString fname) const;
	QString selectFile(QString desc, QString def, QString pattern, bool modify, bool &cancel);
    void updateLogger();
    bool executeBatchLine(QueryExecutor *vpExecutor, DbConnection *batchDb,
                          QString queryPath, const QString &baseInput, QString commandLine,
                          quint32 cntCommands, quint32 lineNr, quint32 queryNr);
    void showTables(bool withReferences);
    void connectMenuActions();
    void import_database_list_file(bool mergeFile);

    Ui::SqlReport *ui;
    LogMessage &logger;
	QuerySet mQuerySet;
	DbConnectionSet databaseSet;
	QuerySetEntry *activeQuerySetEntry;
	QStandardItemModel treeModel;
	EditWidget sqlEditor;
	EditWidget templateEditor;
    EditWidget outputEditor;
    EditDialog descriptionEditor;
    QueryExecutor *queryExecutor;
    AboutDialog aboutDialog;
    QLockFile *lockFile;
    QString interface_language;
    qsizetype showConfiguration;
    QActionGroup *displayModeGroup;
};
