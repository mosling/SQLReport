#include <QMessageBox>
#include <QTextStream>
#include <QFileDialog>
#include <QFontDialog>
#include <QMutableListIterator>
#include <QtPrintSupport/QPrinter>

#include "common.h"
#include "editwidget.h"
#include "ui_editwidget.h"

EditWidget::EditWidget(QWidget *parentObj, const QString &etype, const QString &name,
                       bool showToc, bool withSyntax) :
    QWidget(parentObj),
    ui(new Ui::EditWidget),
    highlighter(nullptr),
    name(name != nullptr ? name : ""),
    editType(etype != nullptr ? etype : "unknown"),
    currentFileName(""),
    searchString(""),
    tableOfContent(nullptr),
    connectedWidget(nullptr),
    parentWidget(nullptr),
    controlled_by_parent(false),
    new_line("[\n]")
    ,completer(nullptr)
{
    ui->setupUi(this);
    this->setWindowTitle(name);
    this->setWindowFlags(Qt::Window);

    if (withSyntax)
    {
        highlighter = new SqlReportHighlighter(ui->teEditor->document());
        ui->teEditor->setProperty("code_edit", true);
    }

    if (showToc)
    {
        tableOfContent = new QStringListModel();
        ui->lvToc->setModel(tableOfContent);
        // in this case we connect the completer with the tableOfContent
        completer = new QCompleter(this);
        completer->setModel(tableOfContent);
        completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
        completer->setCaseSensitivity(Qt::CaseInsensitive);
        completer->setWrapAround(false);
        ui->teEditor->setCompleter(completer);
    }
    else
    {
        ui->cbSync->hide();
        ui->lvToc->hide();
    }

    this->readSettings();
    // we not support insert of formatted text
    ui->teEditor->setAcceptRichText(false);
}

EditWidget::~EditWidget()
{
    storeSettings();
    delete ui;
    qDeleteAll(childEditList.begin(), childEditList.end());
    childEditList.clear();
}

bool EditWidget::operator==(const EditWidget &other) const
{
    return this->name == other.name;
}

bool EditWidget::needUpdate(QString querysetBasepath, QString aFileName)
{
    return this->querysetBasepath != querysetBasepath || this->currentFileName != aFileName;
}

bool EditWidget::newFile(QString querysetBasepath, QString aFileName, bool isLatin)
{
    Q_UNUSED(isLatin)

    bool bRet = true;

    QFile file(aFileName);

    if (file.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream in(&file);
        if (isLatin)
        {
            in.setEncoding(QStringConverter::Encoding::Latin1);
        }

        ui->teEditor->setPlainText(in.readAll());
        updateTableOfContent();
        file.close();
    }
    else
    {
        QMessageBox::information(this, tr("Fileinfo"),
                                 tr("File '%1' not found.").arg(aFileName),QMessageBox::Ok);
        ui->teEditor->clear();
        bRet = false;
    }

    this->querysetBasepath = querysetBasepath;
    this->currentFileName = aFileName;
    ui->leFileName->setText(currentFileName);

    return bRet;
}

//! Aufruf der Speicherfunkion, falls das Fenster sichtbar ist.
void EditWidget::saveFile()
{
    if (this->isVisible())
    {
        on_btnSave_clicked();
        foreach (EditWidget *ew, childEditList)
        {
            ew->saveFile();
        }
    }
}

void EditWidget::setLineWrapMode(QTextEdit::LineWrapMode lwp)
{
    ui->teEditor->setLineWrapMode(lwp);
    ui->pushButtonWrap->setChecked(lwp!=QTextEdit::NoWrap);
}

void EditWidget::placeCursorAtNode(QString nodeName)
{
    int idx = tableOfContent->stringList().indexOf(nodeName);
    if (idx > -1)
    {
        ui->lvToc->setCurrentIndex(ui->lvToc->model()->index(idx, 0));
        ui->teEditor->moveCursor(QTextCursor::End);
        ui->teEditor->moveCursor(QTextCursor::StartOfLine);
        QString stmp = QString("^::%1$").arg(nodeName);
        ui->teEditor->find(QRegularExpression(stmp), QTextDocument::FindBackward);
        showPosition();
    }
}

void EditWidget::storeSettings()
{
    QSettings rc;

    if ( ! this->editType.isEmpty() )
    {
        rc.beginGroup(this->editType);
        rc.setValue("geometry",this->saveGeometry());
        rc.setValue("font", ui->teEditor->font().toString());
        rc.setValue("wrapmode", ui->pushButtonWrap->isChecked());
        rc.endGroup();
    }
}

void EditWidget::readSettings()
{
    QSettings rc;

    if ( ! this->editType.isEmpty() )
    {
        this->restoreGeometry(rc.value(this->editType + "/geometry").toByteArray());
        QString fs = rc.value(this->editType + "/font").toString();
        QFont f;
        f.fromString(fs);
        updateEditorFont(f);
        bool wm = rc.value(this->editType + "/wrapmode").value<bool>();
        ui->pushButtonWrap->setChecked(wm);
        this->on_pushButtonWrap_toggled(wm);
    }
}

//!
void EditWidget::keyPressEvent(QKeyEvent *event)
{
    if (Qt::Key_F3 == event->key() && !searchString.isEmpty())
    {
        on_pushButtonFind_clicked();
    }

    if (Qt::Key_F2 == event->key())
    {
        QTextCursor cursor = ui->teEditor->textCursor();
        cursor.select(QTextCursor::LineUnderCursor);
        ui->teEditor->setTextCursor(cursor);
        QString line = cursor.selectedText();
        cursor.movePosition(QTextCursor::EndOfLine);
        ui->teEditor->setTextCursor(cursor);
        if (line.startsWith("::<"))
        {
            QString fn = line.mid(3).trimmed();
            bool new_file = true;
            foreach (EditWidget *e, childEditList)
            {
                if (e->name == fn)
                {
                    Common::widgetToFront(e);
                    new_file = false;
                }
            }

            if (new_file)
            {
                // create a new edit window with the same type (i.e. name) to reuse font settings
                EditWidget *ew = new EditWidget(this, this->editType, fn, true, true);
                ew->setWindowTitle(QString("%1 included from %2").arg(ew->windowTitle(),this->name));
                childEditList.append(ew);
                ew->setParent(this);
                ew->newFile(querysetBasepath, QString("%1/%2").arg(querysetBasepath, fn), false);
                ew->show();
            }
        }
    }

    if ((event->key() == Qt::Key_F)
            && (QApplication::keyboardModifiers() & Qt::ControlModifier))
    {
        ui->lineEditFind->setFocus();
    }


    if (Qt::Key_F7 == event->key() && ui->lvToc->isVisible())
    {
        QTextCursor cursor = ui->teEditor->textCursor();
        cursor.select(QTextCursor::WordUnderCursor);
        ui->teEditor->setTextCursor(cursor);
        updateCursorNode(cursor.selectedText());
    }

    QWidget::keyPressEvent(event);
}

bool EditWidget::on_btnSave_clicked()
{
    QFile file(currentFileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(currentFileName, file.errorString()));
        return false;
    }

    QTextStream out(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << ui->teEditor->toPlainText().replace('\r', "");
    QApplication::restoreOverrideCursor();

    ui->teEditor->document()->setModified(false);
    return true;
}

bool EditWidget::on_btnSaveAs_clicked()
{
    QString newFileName =
            QFileDialog::getSaveFileName(this,
                                         "Please select new file",
                                         currentFileName, "All Files(*.*)");

    if (!newFileName.isEmpty())
    {
        QString tmp(currentFileName);
        currentFileName = newFileName;
        bool r = on_btnSave_clicked();
        currentFileName = tmp;
        return r;
    }
    return true;
}

void EditWidget::on_pushButtonFind_clicked()
{
    bool findSomething = ui->teEditor->find(searchString);

    if (!findSomething)
    {
        // start from beginning
        ui->teEditor->moveCursor(QTextCursor::Start);
        ui->teEditor->find(searchString);
        showPosition();
    }
}

void EditWidget::on_pushButtonPdf_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Export PDF",
                                                    QString(currentFileName + ".pdf"), "*.pdf");
    if (!fileName.isEmpty())
    {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");

        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);
        ui->teEditor->document()->print(&printer);
    }
}

void EditWidget::on_lineEditFind_textChanged(const QString &str)
{
    searchString = str;
}

void EditWidget::on_lineEditFind_returnPressed()
{
    ui->pushButtonFind->setFocus();
    on_pushButtonFind_clicked();
}

void EditWidget::on_pushButtonWrap_toggled(bool b)
{
    ui->teEditor->setLineWrapMode(b ? QTextEdit::WidgetWidth: QTextEdit::NoWrap);
}

//! Wenn die Speichern-Nachfrage abgebrochen wurde, dann wird das
//! Ereignis ignoriert.
void EditWidget::closeEvent(QCloseEvent *event)
{
    bool all_closed = true;

    QMutableListIterator<EditWidget *> i(childEditList);
    while (i.hasNext()) {
        EditWidget *ew = i.next();

        ew->controlled_by_parent = true;
        ew->closeEvent(event);
        ew->controlled_by_parent = false;

        if (ew->currentFileName.isEmpty())
        {
            // successful closed
            delete ew;
            i.remove();            
        }
        else
        {
            all_closed = false;
        }
    }

    if (!all_closed || !maybeSave())
    {
        event->ignore();
    }
    else
    {
        ui->teEditor->clear();
        currentFileName = "";
        if (!controlled_by_parent && parentWidget != nullptr)
        {
            parentWidget->removeFromChildlist(this);
            delete(this);
        }
    }
}

void EditWidget::removeFromChildlist(EditWidget *w)
{
    if (childEditList.contains(w))
    {
        childEditList.removeOne(w);
    }
}

void EditWidget::updateEditorFont(QFont font)
{
    ui->teEditor->setFont(font);
    this->storeSettings();
}

bool EditWidget::maybeSave()
{
    if (ui->teEditor->document()->isModified())
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Application"),
                                   tr("The document '%1' has been modified.\n"
                                      "Do you want to save your changes?").arg(currentFileName),
                                   QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return on_btnSave_clicked();
        else if (ret == QMessageBox::Cancel)
            return false;
    }

    return true;
}

void EditWidget::updateTableOfContent()
{
    if (tableOfContent == nullptr)
    {
        return;
    }

    QStringList tocList;
    foreach(QString s, ui->teEditor->toPlainText().split(new_line))
    {
        if (s.startsWith("::") && !s.startsWith("::#"))
        {
            tocList << s.replace("::", "");
        }
    }
    tocList.sort(Qt::CaseInsensitive);
    tableOfContent->setStringList(tocList);
}

void EditWidget::updateCursorNode(QString nodeName)
{
    if (ui->lvToc->isVisible())
    {
        placeCursorAtNode(nodeName);

        if (connectedWidget != nullptr && ui->cbSync->isChecked())
        {
            connectedWidget->placeCursorAtNode(nodeName);
        }
    }
}

void EditWidget::on_lvToc_clicked(const QModelIndex &index)
{
    QString itemText = index.data(Qt::DisplayRole).toString();
    updateCursorNode(itemText);
}


void EditWidget::on_teEditor_textChanged()
{
    updateTableOfContent();
}

void EditWidget::on_btnFont_clicked()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, ui->teEditor->font(), this);

    if (ok)
    {
        updateEditorFont(font);
    }
}

void EditWidget::showPosition()
{
    QTextCursor cursor = ui->teEditor->textCursor();
    int y = cursor.blockNumber() + 1;
    int x = cursor.columnNumber() + 1;
    ui->labelPosition->setText(QString("%1:%2").arg(y).arg(x));
}

void EditWidget::on_teEditor_cursorPositionChanged()
{
    showPosition();
}

