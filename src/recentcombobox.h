#ifndef RECENTCOMBOBOX_H
#define RECENTCOMBOBOX_H

#include "logmessage.h"
#include <QComboBox>
#include <QWidget>
#include <QStringListModel>

//!
//! \brief The RecentComboBox class
//!
//! This is a special implementation for QCombobox which uses internal a
//! QStringListModel to order the entries by there usage, the newest
//! element is at first position and the oldest at the last postion. The
//! position is updated if the user selects another element.
class RecentComboBox : public QComboBox
{

    Q_OBJECT

public:
    RecentComboBox(QWidget *parent = nullptr, int maxEntries = 0);

    void setList(const QStringList &list);
    const QStringList getList();
    void addToList(QString &entry);
    void removeFirstElement();

    void setMaxEntries(int m) { maxEntries = m; }
    int getMaxEntries() { return maxEntries; }

signals:
    void recentEntryChanged(const QString &entry);

private slots:
    void indexChanged(int index);
    void updateItem();

private:
    LogMessage &logger;

    void updateRecentList(const QString &item);
    void beginUpdateModel() { updateModel = true; }
    void endUpdateModel() { updateModel = false; }

    QStringListModel recent_list;
    int maxEntries;
    bool updateModel;

};

#endif // RECENTCOMBOBOX_H
