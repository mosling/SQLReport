#pragma once

#include "qdom.h"
#include "qxmlstream.h"
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <qsqldatabase.h>


class Common
{
public:
    static void widgetToFront(QWidget *widget);

    static QString formatMilliSeconds(qint64 ms);
    static void quoteXmlString(QByteArray &str);
    static void quoteJsonString(QString &json_string);
    static QString randomString(quint32 size);
    static QString obscurePassword(const QString &pwd, const QString &salt);
    static QString extractPassword(const QString &str, bool saltOnly);
    static QString findFile(const QString &basePath, const QString &filename);
    static void writeReadmeLine(QTextStream &stream, QString line, bool withEmptyLine);
    static QString getDescriptionHeadline(const QString &description, qsizetype maxLength = 120);
    static QStringList splitString(const QString &str, int width, const QString &startOfLine);
    static qsizetype convertToNumber(QString aNumStr, bool &aOk);
    static QString convertRtf(QString rtfText, QString resultType, bool cleanupFont, bool xmlOutput);
    static QDomElement openXmlFile(QString aFilename);
    static qsizetype getXmlVersion(QDomElement dom);
    static void writeXmlHeader(QXmlStreamWriter &aStream);

    static QStringList availableDrivers();

    static QString jsName() { return "Javascript"; }
    static QString mssqlName() { return "MSSQL"; }
    static QString notImplemented() { return "not implemented"; }
    static qsizetype versionNumber() { return 3; }
    static QString versionString() { return QString("%1").arg(Common::versionNumber()); }

private:
    Common();
};

