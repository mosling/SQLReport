#include <QApplication>
#include <QString>
#include <QStyleFactory>
#include <QStyleHints>

#include "QtCore/qforeach.h"
#include "config.h"
#include "sql_report.h"
#include "odbcsources.h"
#include "sqlreportcli.h"

#ifdef Q_OS_WIN
#include <windows.h>
#endif

void displayStartupInformation()
{
    LogMessage &logger = LogMessage::instance();

    logger.infoMsg(QObject::tr("start application '%1'").arg(QCoreApplication::applicationFilePath()));
    logger.infoMsg(QObject::tr("build with Qt %1").arg(qVersion()));
    switch (QGuiApplication::styleHints()->colorScheme())
    {
        case Qt::ColorScheme::Dark:
            logger.infoMsg("color scheme: dark mode");
            break;
        case Qt::ColorScheme::Light:
            logger.infoMsg("color scheme: light mode");
            break;
        default:
            logger.infoMsg("color schema: unknwon");
            break;
    }

    QStringList l = QCoreApplication::libraryPaths ();
    foreach (QString s, l)
    {
        logger.infoMsg(QObject::tr("library path: %1").arg(s));
    }

    QStringList styles = QStyleFactory::keys();
    foreach (QString s, styles)
    {
        logger.infoMsg(QObject::tr("style: %1").arg(s));
    }

    QStringList drivers = QSqlDatabase::drivers();
    foreach (QString s, drivers)
    {
        logger.infoMsg(QObject::tr("sql driver plugin: %1").arg(s));
    }

    auto ol = OdbcSources::getConnectionList();
    foreach (QString s, ol)
    {
        logger.infoMsg(QObject::tr("odbc connection: %1").arg(s));
    }

    // show settings entries
    QSettings settings;
    logger.infoMsg(QObject::tr("QSettings storage place: %1").arg(settings.fileName()));
}


//! The application startup, setting some values and the default INI Format
//! and location. This depends on the system where sqlReport runs, please look
//! at the Qt documentation.
int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName  ("mosling");
    QCoreApplication::setOrganizationDomain("mosling.gitlab.io");
    QCoreApplication::setApplicationName   ("sql-report");
    QCoreApplication::setApplicationVersion(SQLREPORT_VERSION);

    displayStartupInformation();

    if (argc > 1)
    {
        QCommandLineParser parser;
        parser.setApplicationDescription(QObject::tr("SqlReport for command line usage"));
        auto cli_help = parser.addHelpOption();
        auto cli_version = parser.addVersionOption();
        parser.addPositionalArgument("database-file", QObject::tr("database connection XML file"));
        parser.addPositionalArgument("queryset-file", QObject::tr("queryset XML file"));
        parser.addPositionalArgument("query-name", QObject::tr("query to execute"));
        parser.addPositionalArgument("database", QObject::tr("name of the used database connection"));
        QCommandLineOption parameterOptions(QStringList() << "p" << "parameter",
                                       QObject::tr("optional parameter \"key:=value\" pairs, can be used multiple times"),
                                            "parameter");
        parser.addOption(parameterOptions);

        parser.process(app);

        if (parser.isSet(cli_help) || parser.isSet(cli_version))
        {
            return 0;
        }

        const QStringList args = parser.positionalArguments();
        const QStringList params = parser.values(parameterOptions);

        QString dbfile(args.at(0));
        QString querySet(args.at(1));
        QString query(args.at(2));
        QString database(args.at(3));

        SqlReportCli reportCli;
        bool res = reportCli.execute(querySet, query, database, dbfile, params);
        return res ? 0 : 1;
}
    else
    {
        app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));

        QApplication::setStyle(QStyleFactory::create("Fusion"));

        LogMessage &logger = LogMessage::instance();
        QSettings settings;

        QString appFolder = app.applicationDirPath();
        logger.infoMsg(QString("start application from '%1'").arg(appFolder));
        bool emptyLanguage = (settings.value("language", "").toString()).isEmpty();

        // without language setting we use the system information
        if ( !emptyLanguage )
        {
            QString settingsLangCode = settings.value("language", "").toString();
            QLocale::setDefault(QLocale(settingsLangCode));
            logger.infoMsg(QString("found language code '%1' in the settings").arg(settingsLangCode));
        }

        QLocale locale;
        logger.infoMsg(QString("language '%1' using localization '%2'")
                           .arg(locale.nativeLanguageName(),
                                locale.name())
                       );
        QStringList ll = locale.uiLanguages();
        qsizetype cnt = 1;
        foreach (QString l, ll)
        {
            logger.infoMsg(QString("%1. %2").arg(cnt).arg(l));
            cnt++;
        }

        // reading localization file if exists
        QTranslator qtTtranslator;
        if (qtTtranslator.load(locale,
                               "qt", "_",
                               QLibraryInfo::path(QLibraryInfo::TranslationsPath)))
        {
            logger.infoMsg(QString("load Qt translation file '%1'").arg(qtTtranslator.filePath()));
            app.installTranslator(&qtTtranslator);
        }
        else
        {
            logger.errorMsg(QString("can't load Qt localization file"));
        }

        QTranslator appTranslator;
        if (appTranslator.load(locale, "sqlreport", "_", ":/i18n"))
        {
            app.installTranslator(&appTranslator);
            logger.infoMsg(QString("load application translation for %1 from file '%2'")
                               .arg(appTranslator.language(), appTranslator.filePath()));
            if (emptyLanguage)
            {
                // set the language after successful loading
                QString langCode = locale.name().split("_")[0].toUpper();
                if (langCode == "DE" || langCode == "EN")
                {
                    settings.setValue("language", langCode);
                    logger.infoMsg(QString("set language code to '%1'").arg(langCode));
                }
            }
        }
        else
        {
            logger.errorMsg(QString("can't load application localization file"));
        }

#ifdef Q_OS_WIN
        FreeConsole();
#endif

        SqlReport report;
        report.setVisible(true);

        qint32 res = app.exec();

        return res;
    }
}
