#pragma once

#include <QObject>
#include <QStringList>
#include <QList>
#include <QDomDocument>
#include <QAbstractListModel>
#include <QFileInfo>

#include "logmessage.h"
#include "db_connection_set.h"
#include "query_set_entry.h"

class QuerySet : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit QuerySet(QObject *parentObj = NULL);
	~QuerySet();

    bool readXml(const QString &aFilename, DbConnectionSet &dbSet);

    bool writeXml(const QString &aFileName) const;
	void getNames(QStringList &aList) const;
	bool contains(const QString &aName) const;
    QuerySetEntry *getByName(const QString &aName) const;
    QuerySetEntry *getShowFirst() const;
	void append(QuerySetEntry *aEntry);
	void remove(QuerySetEntry *entry);
	void clear();
    qsizetype size() { return mQueries.size(); }
	QString getQuerySetFileName() const { return querySetFileName; }
    QString getQuerySetPath() const { return QFileInfo(querySetFileName).absolutePath(); }
    qsizetype getQuerySetVersion() const { return querySetVersion; }

    QString getQuerySetDescription() const { return querySetDescription; }
    void setQuerySetDescription(QString &descr) { this->querySetDescription = descr; }

    void readDatabaseMapping();
    void storeDatabaseMapping() const;

    void setShowFirst(QuerySetEntry *entry, bool b);
    qint32 rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
	QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
private:

    QString readXmlCdata(QDomNode &node);

	QList<QuerySetEntry* > mQueries;
    QString querySetFileName;
    QString querySetDescription;
    QString querySetUuid;
    qsizetype querySetVersion;
    LogMessage &logger;
    QMap<QString, QString> databaseMap;
};
