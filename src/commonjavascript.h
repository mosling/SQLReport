#pragma once

#include <QObject>

class CommonJavascript : public QObject
{
    Q_OBJECT

public:
    explicit Q_INVOKABLE CommonJavascript(QObject *parent = nullptr);

    Q_INVOKABLE QString encodeBase64(QString text);
    Q_INVOKABLE QString encodeLatin1Base64(QString text);

signals:
};

