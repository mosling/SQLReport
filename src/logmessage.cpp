#include "logmessage.h"

#include <QFileInfo>

QTextEdit *LogMessage::mMsgWin = nullptr;

bool LogMessage::debugOutput = false;
bool LogMessage::traceOutput = false;

QString LogMessage::context = "";
QString LogMessage::querysetname = "";

LogMessage::LogMessage()
    : QObject(nullptr)
{
}

QTextStream &LogMessage::qStdOut()
{
    static QTextStream ts( stdout );
    return ts;
}

void LogMessage::windowMessageHandler(QtMsgType type,
                                      const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context)

    QString logmsg = msg;

    // add context information in TRACE mode if context is available and not info type
    QString contextFile = QString(context.file);
    if (!contextFile.isEmpty() && isTrace() && type != QtInfoMsg)
    {
        // we have context information
        QString cf = QFileInfo(contextFile).fileName();
        logmsg = QString("%1:%2(%3): %4").arg(cf, context.function).arg(context.line).arg(msg);
    }


    switch (type)
    {
    case QtDebugMsg:
        debugMsg(logmsg);
        break;
    case QtInfoMsg:
        // info without context information
        infoMsg(logmsg);
        break;
    case QtWarningMsg:
        warnMsg(logmsg);
        break;
    case QtCriticalMsg:
        errorMsg(logmsg);
        break;
    case QtFatalMsg:
        break;
    }

}

LogMessage::~LogMessage()
{
    try
    {
        mMsgWin = nullptr;
    }
    catch (...)
    {
        // catch all exception
    }
}

void LogMessage::setMsgWindow(QTextEdit *te)
{
    mMsgWin = te;
}


void LogMessage::setDebugFlag(Qt::CheckState flag)
{
    traceOutput = (flag == Qt::Checked);
    debugOutput = (flag == Qt::PartiallyChecked) || traceOutput;
}

void LogMessage::setContext(const QString &contextString)
{
    context = contextString;
}

void LogMessage::setQuerysetName(const QString &str)
{
    querysetname = str;
}

QString LogMessage::getContextString()
{
    bool qb = !querysetname.isEmpty();
    bool cb = !context.isEmpty();

    if (qb || cb)
    {
        QString cs = "[";
        if (qb)
        {
            cs = cs + querysetname;
        }
        if (cb && qb)
        {
            cs = cs + ".";
        }
        if (cb)
        {
            cs = cs + context;
        }
        cs = cs + "]";
        return cs;
    }

    return "";
}

//! show message string
//!
//! If we have two display windows mMsgWin and mErrorWin we display
//! MSG, WARN and ERR in the mMsgWin and WARN, ERR, DBG and TRACE in the mErrorWin
//! This shows non messages at a more focused point and we not loose the relation
//! between the normal message and the error or warning
void LogMessage::showMsg(const QString &vMsgStr, LogLevel ll)
{
    if (vMsgStr.contains("QFont::setPixelSize:"))
    {
        return;
    }

    if (ll < LogLevel::DBG || (ll == LogLevel::DBG && debugOutput) || (ll == LogLevel::TRACE && traceOutput))
    {

        QString logStr("");
        QString formatStr("%1");
        switch (ll)
        {
        case LogLevel::ERR:
            logStr = "ERROR";
            formatStr="<span style='color:crimson;'>%1</span>";
            break;
        case LogLevel::WARN:
            logStr = "WARN ";
            formatStr="<span style='color:orange;'>%1</span>";
            break;
        case LogLevel::MSG:
            logStr = "INFO ";
            break;
        case LogLevel::DBG:
            logStr = "DEBUG";
            break;
        case LogLevel::TRACE:
            logStr = "TRACE";
            break;
        default: break;
        }

        QString showStr = QString("%1:%2 %3").arg(logStr, getContextString(), vMsgStr);

        if (mMsgWin != nullptr)
        {
            mMsgWin->append(QString(formatStr).arg(showStr));
        }
        else
        {
            qStdOut() << showStr << Qt::endl;
        }
    }
}


