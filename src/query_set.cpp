#include "common.h"
#include "query_set.h"

#include <QSettings>
#include <QFile>
#include <QTextStream>
#include <QUuid>
#include <QXmlStreamWriter>

QuerySet::QuerySet(QObject *parentObj)
    : QAbstractListModel(parentObj)
    ,querySetFileName("")
    ,querySetUuid(QUuid::createUuid().toString())
    ,querySetVersion(0)
    ,logger(LogMessage::instance())
{
}

QuerySet::~QuerySet()
{
	clear();
}

bool QuerySet::contains(const QString &aName) const
{
	return (nullptr != getByName((aName)));
}

QuerySetEntry *QuerySet::getByName(const QString &aName) const
{
	QListIterator<QuerySetEntry* > it(mQueries);
	while (it.hasNext())
	{
		QuerySetEntry *qse = it.next();
		if (qse->getName() == aName) return qse;
	}
    return nullptr;
}

QuerySetEntry *QuerySet::getShowFirst() const
{
    QListIterator<QuerySetEntry* > it(mQueries);
    while (it.hasNext())
    {
        QuerySetEntry *qse = it.next();
        if (qse->getShowFirst()) return qse;
    }

    if (mQueries.count() > 0)
    {
        return mQueries.at(0);
    }

    return nullptr;
}

//! add a new entry to the model
void QuerySet::append(QuerySetEntry *aEntry)
{
	beginInsertRows(QModelIndex(), 0, 0);
	mQueries.append(aEntry);
    std::sort(mQueries.begin(), mQueries.end(), QuerySetEntry());
	endInsertRows();
}

void QuerySet::remove(QuerySetEntry *entry)
{
	beginRemoveRows(QModelIndex(), 0, 0);
	mQueries.removeOne(entry);
	endRemoveRows();

	delete entry;
}

void QuerySet::clear()
{
	foreach (QuerySetEntry *qse, mQueries)
	{
		remove(qse);
	}
    mQueries.clear();
    querySetFileName = "";
    querySetDescription = "";
    querySetUuid = "";
    querySetVersion = 0;
    databaseMap.clear();
}

void QuerySet::readDatabaseMapping()
{
    databaseMap.clear();

    QSettings settings;
    int size = settings.beginReadArray(querySetUuid);
    for (int i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        QString q = settings.value("query").toString();
        QString c = settings.value("connection").toString();
        databaseMap[q] = c;
    }
    settings.endArray();
}

void QuerySet::storeDatabaseMapping() const
{
    QSettings settings;

    // first remove existing mappings
    settings.beginGroup(querySetUuid);
    settings.remove("");
    settings.endGroup();

    // store the current connections between query and database
    settings.beginWriteArray(querySetUuid);
    for (int i = 0; i < mQueries.size(); ++i) {
        settings.setArrayIndex(i);
        settings.setValue("query", mQueries.at(i)->getName());
        settings.setValue("connection", mQueries.at(i)->getDbName());
    }
    settings.endArray();
}

void QuerySet::setShowFirst(QuerySetEntry *entry, bool b)
{
    if (b)
    {
        QListIterator<QuerySetEntry* > it(mQueries);
        while (it.hasNext())
        {
            QuerySetEntry *qse = it.next();
            qse->setShowFirst(false);
        }
    }
    entry->setShowFirst(b);
}


QString QuerySet::readXmlCdata(QDomNode &node)
{
    // One CDATA is enough, but sometime (e.g. line starts with ]]>)
    // more CDATA elements are generated during write
    QDomNode cdn = node.firstChild();
    QString dstr = "";
    while (!cdn.isNull())
    {
        dstr += cdn.toCDATASection().data();
        cdn = cdn.nextSibling();
    }

    return dstr;
}

//! Read the XML file
bool QuerySet::readXml(const QString &aFilename, DbConnectionSet &dbSet)
{   
	// Jetzt das XML-Dokument parsen
    QDomElement docElem = Common::openXmlFile(aFilename);

    QString vName = docElem.nodeName();
    logger.debugMsg(tr("reading node %1").arg(vName));

    if (docElem.isNull())
    {
        logger.errorMsg(tr("wrong or empty query set XML file '%1'").arg(aFilename));
        return false;
    }

    if (docElem.nodeName().toUpper() != "SQLREPORT")
    {
        logger.errorMsg(tr("root node in XML must be 'SQLREPORT' but is '%1'")
                            .arg(docElem.nodeName().toUpper()));
        return false;
    }

    databaseMap.clear();
    querySetVersion = Common::getXmlVersion(docElem);
    QDomNode p = docElem.firstChild();

    logger.infoMsg(tr("read query set file '%1' version %2")
                       .arg(aFilename)
                       .arg(querySetVersion));

    int cnt = 5;
    while (!p.isNull() && cnt-- > 0)
    {
        logger.debugMsg(tr("sqlreport child %1").arg(p.nodeName()));

        if (p.nodeName().toUpper() == "QUERYSET")
        {
            QDomNode n = p.firstChild();
            logger.debugMsg(tr("queryset node %1").arg(p.nodeName()));

            while(!n.isNull())
            {
                QDomElement e = n.toElement();
                if (!e.isNull() && e.tagName().toUpper() == "DESCRIPTION")
                {
                    this->querySetDescription = this->readXmlCdata(n);
                }

                if (!e.isNull() && e.tagName().toUpper() == "QUERYUUID")
                {
                    this->querySetUuid = n.firstChild().toText().data();
                    readDatabaseMapping();
                }

                if(!e.isNull() && e.tagName().toUpper() == "QUERY")
                {
                    QuerySetEntry *vQEntry = new QuerySetEntry();
                    QDomNode cn = n.firstChild();
                    while (!cn.isNull())
                    {
                        QString ce = cn.toElement().tagName().toUpper();
                        QString te = cn.firstChild().toText().data();
                        if (ce == "DBNAME")       { vQEntry->setDbName(te); }  // up to version 2
                        if (ce == "DESCRIPTION")  { vQEntry->setDescription(this->readXmlCdata(cn)); }
                        if (ce == "PARAMETER")    { vQEntry->setQueryParameter(te); }
                        if (ce == "SQL")          { vQEntry->setSqlFile(te); }
                        if (ce == "TEMPLATE")     { vQEntry->setTemplateFile(te); }
                        if (ce == "OUTPUT")       { vQEntry->setOutputFile(te); }
                        if (ce == "NAME")         { vQEntry->setName(te); }
                        if (ce == "BATCHRUN")     { vQEntry->setBatchrun((te.toUpper()=="YES"));	}
                        if (ce == "USETIMESTAMP") {	vQEntry->setWithTimestamp((te.toUpper()=="YES")); }
                        if (ce == "APPENDOUTPUT") { vQEntry->setAppendOutput((te.toUpper()=="YES")); }
                        if (ce == "UTF8")         { vQEntry->setOutputLatin1((te.toUpper()!="YES"));}
                        if (ce == "LATIN1")       { vQEntry->setOutputLatin1((te.toUpper()=="YES")); }
                        if (ce == "ASXML")        { vQEntry->setOutputXml((te.toUpper()=="YES")); }
                        if (ce == "SHOWFIRST")    { vQEntry->setShowFirst((te.toUpper()=="YES")); }
                        if (ce == "EMPTYNULL")    { vQEntry->setEmptyNull((te.toUpper()=="YES")); }
                        if (ce == "EMPTYNOTFOUND"){ vQEntry->setEmptyNotFound((te.toUpper()=="YES")); }
                        if (ce == "MAXRECURSION") { vQEntry->setMaxRecursion(te); }

                        // some parameter for autoupdate of old XML files only
                        // this elements are read but never written
                        if (ce == "DESCR")        { vQEntry->setDescription(te); }
                        if (ce == "DEFINES")      { vQEntry->setQueryParameter(te); }

                        cn = cn.nextSibling();
                    }
                    if ( !vQEntry->getName().isEmpty() )
                    {
                        if (databaseMap.contains(vQEntry->getName()))
                        {
                            // set the database before adding, this implies a version greater than 2
                            vQEntry->setDbName(databaseMap[vQEntry->getName()]);
                        }
                        append(vQEntry);
                    }
                    else
                    {
                        delete vQEntry;
                    }
                }
                n = n.nextSibling();
            }
        }
        else if (p.nodeName().toUpper() == "DATABASESET")
        {
            if (querySetVersion <= 2)
            {
                dbSet.readXmlNode(p, querySetVersion);
            }
            else
            {
                logger.errorMsg(tr("please check, you read a database file as queryset file -- ignoring"));
                this->clear();
                return false;
            }
        }

        p = p.nextSibling();
    }

    querySetFileName = aFilename;
    if (querySetUuid.isEmpty())
    {
        // during the first time after migrate from version 2 to version 3
        querySetUuid = QUuid::createUuid().toString();
    }

    return true;
}


bool QuerySet::writeXml(const QString &aFileName) const
{
	QString localFileName = aFileName;

    if (localFileName.isEmpty())
    {
        // if no name is given use the stored filename
        localFileName = querySetFileName;
    }

    if (localFileName.isEmpty())
    {
        return false;
    }

    QFile file(localFileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        logger.errorMsg(tr("can't open query set file '%1' for writing").arg(localFileName));
        return false;
    }
    logger.infoMsg(tr("write query set to '%1'").arg(localFileName));

    QXmlStreamWriter vStream(&file);
    Common::writeXmlHeader(vStream);

    vStream.writeStartElement("QuerySet");
    vStream.writeTextElement("queryuuid", querySetUuid);
    vStream.writeStartElement("description");
    vStream.writeCDATA(this->querySetDescription);
    vStream.writeEndElement();

    QListIterator<QuerySetEntry* > it(mQueries);
    while (it.hasNext())
    {
        QuerySetEntry *qse = it.next();
        vStream.writeStartElement("Query");
        vStream.writeTextElement("name",         qse->getName() );
        //vStream.writeTextElement("dbname",		 qse->getDbName() );
        vStream.writeStartElement("description");
        vStream.writeCDATA(qse->getDescription());
        vStream.writeEndElement();
        vStream.writeTextElement("parameter",    qse->getQueryParameter());
        vStream.writeTextElement("sql",			 qse->getSqlFile() );
        vStream.writeTextElement("template",	 qse->getTemplateFile() );
        vStream.writeTextElement("output",		 qse->getOutputFile());
        vStream.writeTextElement("batchrun",     qse->getBatchrun()?"yes":"no");
        vStream.writeTextElement("useTimestamp", qse->getWithTimestamp()?"yes":"no");
        vStream.writeTextElement("appendOutput", qse->getAppendOutput()?"yes":"no");
        // write to use the main version will be removed later
        vStream.writeTextElement("utf8",         qse->getOutputLatin1()?"no":"yes");
        vStream.writeTextElement("latin1",       qse->getOutputLatin1()?"yes":"no");
        vStream.writeTextElement("asXml",        qse->getOutputXml()?"yes":"no");
        vStream.writeTextElement("showFirst",    qse->getShowFirst()?"yes":"no");
        vStream.writeTextElement("emptyNull",    qse->getEmptyNull()?"yes":"no");
        vStream.writeTextElement("emptyNotFound",qse->getEmptyNotFound()?"yes":"no");
        vStream.writeTextElement("maxRecursion", QString("%1").arg(qse->getMaxRecursion()));
        vStream.writeEndElement();
    }
    vStream.writeEndElement();

    vStream.writeEndDocument();
    file.close();

    storeDatabaseMapping();

    return true;
}

void QuerySet::getNames(QStringList &aList) const
{
    QListIterator<QuerySetEntry* > it(mQueries);
    while (it.hasNext())
    {
        QuerySetEntry *qse = it.next();
        aList.append(qse->getName());
    }
}

qint32 QuerySet::rowCount(const QModelIndex &) const
{
	return mQueries.count();
}

QVariant QuerySet::data(const QModelIndex &index, int role) const
{
	if (!index.isValid()) return QVariant();

	if (index.row() >= mQueries.count()) return QVariant();

	if (role == Qt::DisplayRole)
	{
		return mQueries[index.row()]->getName();
	}
	else
	{
		return QVariant();
	}
}

QVariant QuerySet::headerData(int section,
									 Qt::Orientation orientation,
									 int role) const
{
	Q_UNUSED(section)
	Q_UNUSED(orientation)

	if (Qt::DisplayRole != role) return QVariant();
	return ("Query");
}
