#pragma once

#include <QSyntaxHighlighter>
#include <QRegularExpression>

class SqlReportHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
	Q_CLASSINFO ("author", "St. Koehler")
	Q_CLASSINFO ("company", "com.github.mosling")

public:
	 explicit SqlReportHighlighter(QTextDocument *parentObj = 0);

 protected:
	 void highlightBlock(const QString &text);

 private:
	 struct HighlightingRule
	 {
         QRegularExpression pattern;
		 QTextCharFormat format;
	 };
	 QVector<HighlightingRule> highlightingRules;

     QRegularExpression commentStartExpression;
     QRegularExpression commentEndExpression;

     QTextCharFormat singleLineCommentFormat;
	 QTextCharFormat multiLineCommentFormat;
	 QTextCharFormat keywordFormat;
	 QTextCharFormat blockFormat;
	 QTextCharFormat substitutionFormat;
	 QTextCharFormat templateFormat;
     QTextCharFormat includeFormat;

     QBrush keywordBrush;
     QBrush substitutionBrush;
     QBrush templateBrush;
     QBrush singleLineCommentBrush;
     QBrush includeBrush;
     QBrush blockBrush;
     QBrush multiLineCommentBrush;
};
