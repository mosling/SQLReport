#include "resultset.h"

#include <QStringDecoder>

ResultSet::ResultSet()
    : validResultSet(false),
      emptyResultRow(false),
      emptyResultSet(true)
{
}

ResultSet::~ResultSet()
{
    overwrittenReplacements.clear();
    insertedReplacementsKeys.clear();
}


void ResultSet::saveParameter(QHash<QString, QByteArray> &replacements, QString &parameter_name)
{
    if (replacements.contains(parameter_name))
    {
        overwrittenReplacements[parameter_name] = replacements[parameter_name];
    }
    else
    {
        insertedReplacementsKeys.append(parameter_name);
    }
}

void ResultSet::finishResultSet(QHash<QString, QByteArray> &replacements)
{

    // restore the overwritten replacements to the replacements array
    QHashIterator<QString,QByteArray> it(overwrittenReplacements);
    while (it.hasNext())
    {
        it.next();
        replacements[it.key()] = it.value();
    }

    // remove inserted entries from replacements
    foreach (QString ik, insertedReplacementsKeys )
    {
        replacements.remove(ik);
    }

    // invalidate the result set
    if (isValid())
    {
        setValid(false);
    }
}
