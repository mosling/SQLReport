#pragma once

#include <QAbstractButton>
#include <QDialog>

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QString dialogTitle, QWidget *parent = nullptr);
    ~EditDialog();

    void setText(QString str, bool isQueryDescription);

private slots:
    void ok_clicked();

    void cancel_clicked();

private:
    Ui::EditDialog *ui;
    bool queryDescription;
};
