#ifndef QUERYSETHISTORY_H
#define QUERYSETHISTORY_H

#include <QAbstractListModel>

class QuerySetHistory : public QAbstractListModel
{
public:
    explicit QuerySetHistory(QObject *parent = nullptr);

    QString &currentFile();
    void insertNewFile(QString& filename);
    void removeFile(QString& filename);
    void setList(QString& filename_list);
    QString getList();

    qint32 rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

private:
    QString defaultName;
    QStringList querySetHistory;
};

#endif // QUERYSETHISTORY_H
