#pragma once

#include <QObject>
#include <QTextEdit>
#include <QCompleter>

class BlockTextEdit : public QTextEdit
{
    Q_OBJECT

public:
    BlockTextEdit(QWidget *parent = nullptr);
    ~BlockTextEdit() override;

    void setCompleter(QCompleter *c);
    QCompleter *completer() const;

protected:
    void keyPressEvent(QKeyEvent *e) override;
    void focusInEvent(QFocusEvent *e) override;

private slots:
    void insertCompletion(const QString &completion);

private:
    QString textUnderCursor() const;

private:
    QCompleter *blockCompleter = nullptr;
};

