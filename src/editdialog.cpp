#include "editdialog.h"
#include "ui_editdialog.h"

#include <sql_report.h>

EditDialog::EditDialog(QString dialogTitle, QWidget *parent) :
    QDialog(parent)
    ,ui(new Ui::EditDialog)
    ,queryDescription(false)
{
    ui->setupUi(this);
    QSettings rc;

    this->restoreGeometry(rc.value("editdialog_geometry").toByteArray());
    this->setWindowTitle(dialogTitle);
}

EditDialog::~EditDialog()
{
    QSettings rc;

    rc.setValue("editdialog_geometry", this->saveGeometry());
    delete ui;
}

void EditDialog::setText(QString str, bool isQueryDescription)
{
    this->queryDescription = isQueryDescription;
    ui->textEdit->setText(str);
}


void EditDialog::ok_clicked()
{
    static_cast<SqlReport *>(this->parent())->setDescription(ui->textEdit->toPlainText(), this->queryDescription);
    this->close();
}


void EditDialog::cancel_clicked()
{
    this->close();
}

