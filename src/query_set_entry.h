#pragma once

#include "logmessage.h"
#include <QDomNode>

class QuerySetEntry
{

public:
    explicit QuerySetEntry();
    ~QuerySetEntry();

    const static quint32 defaultMaxRecursion = 8;

    QuerySetEntry& operator=(const QuerySetEntry& rhs);
    bool operator()(const QuerySetEntry *l, const QuerySetEntry *r);

    QString getName() const { return name; }
    void setName(const QString &value);

    QString getDbName() const { return dbname; }
    void setDbName(const QString &value);

    QString getDescription() const;
    void setDescription(const QString &value);

    QString getQueryParameter() const;
    void setQueryParameter(const QString &value);

    QString getSqlFile() const;
    void setSqlFile(const QString &value);

    QString getTemplateFile() const;
    void setTemplateFile(const QString &value);

    QString getOutputFile() const;
    void setOutputFile(const QString &value);

    QString getLastOutputFile() const;
    void setLastOutputFile(const QString &value);

    bool getBatchrun() const;
    void setBatchrun(bool value);

    bool getWithTimestamp() const;
    void setWithTimestamp(bool value);

    bool getAppendOutput() const;
    void setAppendOutput(bool value);

    bool getOutputLatin1() const {return outputLatin1; }
    void setOutputLatin1(bool value) { outputLatin1 = value; }

    bool getOutputXml() const;
    void setOutputXml(bool value);

    bool getShowFirst() const;
    void setShowFirst(bool value);

    bool getEmptyNull() const;
    void setEmptyNull(bool value);

    bool getEmptyNotFound() const;
    void setEmptyNotFound(bool value);

    quint32 getMaxRecursion() const;
    void setMaxRecursion(QString value);
    void setMaxRecursion(quint32 value);

    QString getDateFormat() const;
    void setDateFormat(const QString &value);

    QString getDatabaseEncoding() const {return databaseEncoding; }
    void setDatabaseEncoding(const QString &value) { databaseEncoding = value; };

private:
    LogMessage &logger;
    QString name;
    QString dbname;
    QString description;
    QString queryParameter;
    QString sqlFile;
    QString templateFile;
    QString outputFile;
    QString lastOutputFile;
    bool batchrun;
    bool withTimestamp;
    bool appendOutput;
    bool outputLatin1;
    bool outputXml;
    bool showFirst;
    bool emptyNull;
    bool emptyNotFound;
    quint32 maxRecursion;
    QString databaseEncoding;  // comes from the connection

};
