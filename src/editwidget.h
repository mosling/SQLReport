#pragma once

#include "sql_report_highlighter.h"

#include <QCloseEvent>
#include <QCompleter>
#include <QDialog>
#include <QFile>
#include <QSettings>
#include <QString>
#include <QStringListModel>
#include <QTextEdit>
#include <QWidget>

namespace Ui {
    class EditWidget;
}

class EditWidget : public QWidget
{
    Q_OBJECT
	Q_CLASSINFO ("author", "St. Koehler")
	Q_CLASSINFO ("company", "com.github.mosling")

public:
    explicit EditWidget(QWidget *parentObj, const QString &etype,
                        const QString &name, bool showToc, bool withSyntax);
    ~EditWidget() override;

    bool operator==(const EditWidget& other) const;
    bool needUpdate(QString querysetBasepath, QString aFileName);
    bool newFile(QString querysetBasepath, QString aFileName, bool isLatin = false);
	void saveFile();
    bool maybeSave();
	void setLineWrapMode(QTextEdit::LineWrapMode lwp);
    void placeCursorAtNode(QString nodeName);
    void setConnectedWidget(EditWidget *w) { connectedWidget = w; }
    void storeSettings();
    void readSettings();

protected:
	void keyPressEvent(QKeyEvent *event) override;

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
	bool on_btnSave_clicked();
    bool on_btnSaveAs_clicked();
	void on_pushButtonFind_clicked();
	void on_pushButtonPdf_clicked();
    void on_lineEditFind_textChanged(const QString &str);
	void on_lineEditFind_returnPressed();
	void on_pushButtonWrap_toggled(bool b);

    void on_lvToc_clicked(const QModelIndex &index);

    void on_teEditor_textChanged();

    void on_btnFont_clicked();

    void on_teEditor_cursorPositionChanged();

private:
    void updateTableOfContent();
    void updateCursorNode(QString nodeName);
    void showPosition();
    void setParent(EditWidget *w) { parentWidget = w; }
    void removeFromChildlist(EditWidget *w);
    void updateEditorFont(QFont font);

    Ui::EditWidget *ui;
	SqlReportHighlighter *highlighter;
    QString name;
    QString editType;
    QString querysetBasepath;
	QString currentFileName;
	QString searchString;
    QStringListModel *tableOfContent;
    EditWidget *connectedWidget;
    EditWidget *parentWidget;
    bool controlled_by_parent;
    QList<EditWidget *> childEditList;
    QRegularExpression new_line;
    QCompleter *completer;
};
