#include "lookuplist.h"

LookupList::LookupList(QObject *parent)
    :QObject(parent),
      logger(LogMessage::instance())
{

}

void LookupList::insert_value(QString mapName, QString key, QString value)
{
    lookup_map[mapName].insert(key, value);

    if (logger.isTrace())
    {
        logger.traceMsg(tr("insert key(%1)='%2' into lookup list '%3'").arg(key,value,mapName));
    }
}

int LookupList::mapSize(QString mapName)
{
    if (hasMapEntry(mapName))
    {
        return lookup_map[mapName].size();
    }

    return -1;
}

bool LookupList::hasMapEntry(QString mapName)
{
    return lookup_map.contains(mapName);
}

bool LookupList::hasMapKey(QString mapName, QString key)
{
    return hasMapEntry(mapName) && lookup_map[mapName].contains(key);
}

bool LookupList::hasKey(QString mapName, QString key)
{
    return hasMapKey(mapName, key);
}

QString LookupList::getValue(QString mapName, QString key)
{
    if (hasMapKey(mapName, key))
    {
        return lookup_map[mapName].value(key);
    }
    else
    {
        return key;
    }
}
