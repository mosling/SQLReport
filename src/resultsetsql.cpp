#include "resultsetsql.h"
#include "common.h"
#include "QtSql/qsqldriver.h"
#include "QtSql/qsqlerror.h"
#include "QtSql/QSqlRecord"
#include "QtSql/qsqlfield.h"
#include "query_set_entry.h"

#include <dbencoding.h>

ResultSetSql::ResultSetSql(QString dbName, QString &queryStr)
    : ResultSet()
    , logger(LogMessage::instance())
    , firstResult(false)
    , query(QSqlDatabase::database(dbName))
    , sqlQuery(queryStr)
{
    setValid(query.exec(queryStr));
    if (isValid())
    {
        rec = query.record();
        if (logger.isDebug())
        {
            QString nr = "";
            if (QSqlDatabase::database(dbName).driver()->hasFeature(QSqlDriver::QuerySize)) {
                nr = QString(" with %1 rows").arg(query.size());
            }
            logger.debugMsg(tr("SQL-Query: %1%2").arg(queryStr,nr));
        }

        firstResult = true;
    }
}

ResultSetSql::~ResultSetSql()
{
    if (isValid())
    {
        query.finish();
        setValid(false);
    }
}

QString ResultSetSql::lastError()
{
    return tr("## error executing %1 ## %2 ##").arg(sqlQuery,query.lastError().text());
}

bool ResultSetSql::updateParameter(QHash <QString, QByteArray> &replacements, QuerySetEntry *mQSE)
{
    if (!isValid())
    {
        return false;
    }

    bool emptyResultRow = true;
    bool bRet = query.next();

    // check if a next result row is available and handle first and normal result row
    if (bRet)
    {
        // we have at least one result
        setEmptySet(false);

        // get the sql values
        for (int i=0; i<rec.count(); ++i)
        {
            // using byte array to
            QByteArray tmpStr;
            QString tmpFieldName = rec.fieldName(i).toUpper();
            bool isBlob = "QByteArray" == QString( rec.field(i).metaType().name());
            bool isString = "QString" == QString( rec.field(i).metaType().name());

            if (mQSE->getEmptyNull() && query.isNull(i))
            {
                tmpStr = "";
            }
            else if (isString)
            {
                // if string use the given encoding to convert content to internal UTF-8
                tmpStr = DbEncoding::decode(query.value(i).toByteArray(),
                                            mQSE->getDatabaseEncoding());
                if (mQSE->getOutputXml())
                {
                    Common::quoteXmlString(tmpStr);
                }
            }
            else if (isBlob)
            {
                // QByteArray used as blobs are copied without modification, id needed this
                // can be done later by convert the content
                tmpStr = query.value(i).toByteArray();
            }
            else if (query.value(i).canConvert<QString>())
            {
                // Can convert to string, do this and store as UTF-8, normally in this categorie
                // we have numbers and dates which has ASCII encoding
                // trouble can be come in by weekdays names, but hopefully this results marked as
                // string by the database connector
                tmpStr = query.value(i).toString().toUtf8();
            }
            else
            {
                // this case should not appear, if so print error and use it as byte array
                logger.errorMsg(tr("can't convert '%1' to QString use binary data"));
                tmpStr = query.value(i).toByteArray();
            }

            if (emptyResultRow)
            {
                // Query is defined as empty if all columns NULL or empty string
                emptyResultRow = query.isNull(i) || tmpStr.isEmpty();
            }

            // store equal named columns in the overwrittenReplacements Hash
            // this is done once for the first query result only, we need to store the
            // value from the calling template to restore after executing all rows from
            // the current result set, store inserted values to remove this columns
            // from mReplacements
            if (firstResult)
            {
                saveParameter(replacements, tmpFieldName);
            }

            // set the replacement for the current field name
            replacements[tmpFieldName] = tmpStr;

            if (logger.isTrace())
            {
                logger.traceMsg(tr("column[%1]:%2 %3 = /%4/ len(%5)=0x%6 ")
                                .arg(i)
                                .arg(rec.field(i).metaType().name())
                                .arg(tmpFieldName, replacements[tmpFieldName])
                                .arg(query.value(i).toByteArray().size())
                                .arg(query.value(i).toByteArray().toHex())
                                );
            }
        }

        firstResult = false;
    }

    setEmptyRow(emptyResultRow);
    return bRet;
}

void ResultSetSql::finishResultSet(QHash <QString, QByteArray> &replacements)
{
    if (isValid())
    {
        query.finish();
    }

    ResultSet::finishResultSet(replacements);
}
