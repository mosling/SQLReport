#ifndef PARAMETERMODEL_H
#define PARAMETERMODEL_H

#include "parameterentry.h"

#include <QAbstractTableModel>

class ParameterModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit ParameterModel(QObject *parent = nullptr);

    ~ParameterModel() override;

    // Methods
    void addOrUpdate(ParameterEntry &pentry);
    qint32 findIndex(ParameterEntry &pentry) const;
    void importCategory(const ParameterEntry::ParameterCategory &category, const QString &str, bool removeExisting);
    QString exportCategory(const ParameterEntry::ParameterCategory &category) const;
    void clearCategory(const ParameterEntry::ParameterCategory &category);
    void clearParameter() {
        parameters.clear();
    }
    void setDefaultCategory(const QString &s) { default_category = s; }

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override {
        Q_UNUSED(parent)
        return parameters.size();
    }
    int columnCount(const QModelIndex &parent = QModelIndex()) const override {
        Q_UNUSED(parent)
        return 3;
    }

    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    bool insertRows(int position, int rows, const QModelIndex &parent) override;
    bool removeRows(int position, int rows, const QModelIndex &parent) override;

private:
    QString default_category;
    QList<QSharedPointer<ParameterEntry>> parameters;
};

#endif // PARAMETERMODEL_H
