#ifndef PARAMETERCATEGORYDELEGATE_H
#define PARAMETERCATEGORYDELEGATE_H

#include <QStyledItemDelegate>

class ParameterCategoryDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    ParameterCategoryDelegate(QObject *parent = nullptr);

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // PARAMETERCATEGORYDELEGATE_H
