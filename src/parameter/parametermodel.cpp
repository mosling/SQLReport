#include "parametermodel.h"

ParameterModel::ParameterModel(QObject *parent)
    : QAbstractTableModel(parent)
    , default_category("Query")
{
}

ParameterModel::~ParameterModel()
{
    parameters.clear();
}

void ParameterModel::addOrUpdate(ParameterEntry &pentry)
{
    qint32 idx = findIndex(pentry);
    if (idx < 0)
    {
        idx = parameters.size();
        this->insertRows(idx, 1, QModelIndex());
        setData(this->index(idx, 0, QModelIndex()), pentry.getCategoryString(), Qt::EditRole);
        setData(this->index(idx, 1, QModelIndex()), pentry.getName(), Qt::EditRole);
    }

    setData(this->index(idx, 2, QModelIndex()), pentry.getValue(), Qt::EditRole);
}

qint32 ParameterModel::findIndex(ParameterEntry &pentry) const
{
    qint32 idx = 0;
    foreach(QSharedPointer<ParameterEntry> entry, parameters)
    {
        if (pentry == *entry.data())
        {
            return idx;
        }
        idx += 1;
    }

    return -1;
}

void ParameterModel::importCategory(const ParameterEntry::ParameterCategory &category,
                                       const QString &str, bool removeExisting)
{

    if (removeExisting)
    {
        clearCategory(category);
    }

    QStringList l = str.split("|");
    if (l.size() > 0)
    {
        foreach (QString p, l) {
            QStringList e = p.split(":=");
            if (e.size() == 2)
            {
                auto p = ParameterEntry(category, e.at(0), e.at(1));
                addOrUpdate(p);
            }
        }
    }
}

QString ParameterModel::exportCategory(const ParameterEntry::ParameterCategory &category) const
{
    QStringList l;
    foreach(QSharedPointer<ParameterEntry> entry, parameters)
    {
        if (category == entry->getCategory())
        {
            l.append(QString("%1:=%2").arg(entry->getName(), entry->getValue()));
        }
    }

    return l.join("|");
}

void ParameterModel::clearCategory(const ParameterEntry::ParameterCategory &category)
{
    bool has_category = true;
    while (has_category)
    {
        has_category = false;
        for (qint32 i = 0; i < parameters.size(); ++i)
        {
            if (category == parameters.at(i)->getCategory())
            {
                removeRow(i, QModelIndex());
                has_category = true;
                break;
            }
        }
    }
}

QVariant ParameterModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() > this->parameters.size()) {
        return QVariant();
    }
    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return this->parameters.at(index.row())->getCategoryString();
        case 1:
            return this->parameters.at(index.row())->getName();
        case 2:
            return this->parameters.at(index.row())->getValue();
        default:
            return "unknown";
        }

    }
    return QVariant();
}

QVariant ParameterModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole) {
        if (orientation == Qt::Horizontal) {
            switch(section) {
            case 0:
                return "Category";
            case 1:
                return "Name";
            case 2:
                return "Value";
            default:
                return "unknown";
            }
        }
    }
    return QVariant();
}

Qt::ItemFlags ParameterModel::flags(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return Qt::ItemIsEnabled;
    }
    return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

bool ParameterModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole) {
        switch(index.column()) {
        case 0:
            this->parameters.at(index.row())->setCategory(value.toString());
            break;
        case 1:
            this->parameters.at(index.row())->setName(value.toString());
            break;
        case 2:
            this->parameters.at(index.row())->setValue(value.toString());
            break;
        default:
            return false;
        }
        emit dataChanged(index, index, {role});
        return true;
    }
    return false;
}

bool ParameterModel::insertRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent)

    this->beginInsertRows(QModelIndex(), position, position + rows - 1);
    for (int row = 0; row < rows; ++row) {
        auto pe = QSharedPointer<ParameterEntry>::create();
        pe->setCategory(default_category);
        this->parameters.insert(position, pe);
    }
    this->endInsertRows();
    return true;
}

bool ParameterModel::removeRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent)

    if (position < 0 || position + rows > parameters.size()) {
        return false;
    }
    this->beginRemoveRows(QModelIndex(), position, position + rows - 1);
    for (int row = 0; row < rows; ++row) {
        this->parameters.removeAt(position);
    }
    this->endRemoveRows();
    return true;
}


