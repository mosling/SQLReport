#ifndef PARAMETERDIALOG_H
#define PARAMETERDIALOG_H

#include "QtCore/qsortfilterproxymodel.h"
#include "parametermodel.h"
#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class ParameterDialog; }
QT_END_NAMESPACE

class ParameterDialog : public QDialog
{
    Q_OBJECT

public:
    ParameterDialog(ParameterModel *model, QWidget *parent = nullptr);
    ~ParameterDialog();

    void setInfoLabel(const QString &query, const QString &db);

private slots:
    void on_btnAdd_clicked();
    void on_btnRemove_clicked();

    void filterChanged();

    void on_checkBox_stateChanged(int arg1);

    void on_pbAddValues_clicked();

private:
    Ui::ParameterDialog *ui;

    QSortFilterProxyModel *proxyModel;
    ParameterModel *model;
};
#endif // PARAMETERDIALOG_H
