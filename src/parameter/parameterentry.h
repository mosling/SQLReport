#ifndef PARAMTERENTRY_H
#define PARAMTERENTRY_H

#include <QObject>

class ParameterEntry : public QObject {

    Q_OBJECT

public:
    enum ParameterCategory {Local, Query, Database};
    Q_ENUM(ParameterCategory)
    static QStringList getCategories();
    static QString getCategoryName(const ParameterCategory c);

    ParameterEntry(const ParameterCategory &category = ParameterCategory::Query,
                   const QString &name = "",
                   const QString &value = "");

    virtual ~ParameterEntry();

    ParameterEntry& operator= (ParameterEntry rhs);
    bool operator== (ParameterEntry &rhs);

    ParameterCategory getCategory() { return category; }
    QString getCategoryString() { return getCategoryName(category); }

    QString getName() { return name; }
    QString getValue() { return value; }

    void setCategory(const ParameterCategory &c) { category = c; }
    void setCategory(const QString &s);

    void setName(const QString &str) { name = str; }
    void setValue(const QString &str) { value = str; }

signals:

private:
    ParameterCategory category;
    QString name;
    QString value;

};

#endif // PARAMTERENTRY_H
