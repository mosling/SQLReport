#include "parameterentry.h"

#include <QVariant>
#include <QMetaEnum>


ParameterEntry::ParameterEntry(const ParameterCategory &category, const QString &name,
                               const QString &value)
    :category(category)
    ,name(name)
    ,value(value)
{
}

ParameterEntry::~ParameterEntry()
{
}

ParameterEntry &ParameterEntry::operator=(ParameterEntry rhs)
{
    this->setCategory(rhs.getCategory());
    this->setName(rhs.name);
    this->setValue(rhs.value);

    return *this;
}

bool ParameterEntry::operator==(ParameterEntry &rhs)
{
    if ( this == &rhs)
    {
        return true;
    }

    return this->category == rhs.category && this->name == rhs.name;
}

QStringList ParameterEntry::getCategories()
{
    QMetaEnum e = QMetaEnum::fromType<ParameterCategory>();
    QStringList categoryList;

    for (int k = 0; k < e.keyCount(); k++)
    {
        auto c = (ParameterCategory) e.value(k);
        categoryList.push_back(getCategoryName(c));
    }

    return categoryList;
}

QString ParameterEntry::getCategoryName(const ParameterCategory c)
{
    int castValue = static_cast<int>(c);
    return QMetaEnum::fromType<ParameterCategory>().valueToKey(castValue);
}

void ParameterEntry::setCategory(const QString &s)
{
    bool bOk;
    int i = QMetaEnum::fromType<ParameterCategory>().keyToValue(s.toLatin1(), &bOk);
    if (i < 0)
    {
        category = ParameterCategory::Query;
    }
    else
    {
        category = static_cast<const ParameterCategory>(i);
    }
}
