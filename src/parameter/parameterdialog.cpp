#include "parameterdialog.h"
#include "parametercategorydelegate.h"
#include "ui_parameterdialog.h"

#include <QSortFilterProxyModel>
#include <QSettings>

static inline QColor textColor(const QPalette &palette)
{
    return palette.color(QPalette::Active, QPalette::Text);
}

static void setTextColor(QWidget *w, const QColor &c)
{
    auto palette = w->palette();
    if (textColor(palette) != c) {
        palette.setColor(QPalette::Active, QPalette::Text, c);
        w->setPalette(palette);
    }
}

ParameterDialog::ParameterDialog(ParameterModel *model, QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::ParameterDialog)
    , proxyModel(new QSortFilterProxyModel)
    , model(model)
{
    ui->setupUi(this);

    ui->widgetAddValue->hide();

    // set signal and slot for "Buttons"
    connect(ui->butOk, SIGNAL(clicked()), this, SLOT(accept()));
    connect(ui->butCancel, SIGNAL(clicked()), this, SLOT(reject()));

    proxyModel->setSourceModel(model);
    proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    proxyModel->setFilterKeyColumn(1);
    proxyModel->sort(0, Qt::AscendingOrder);

    ui->tableView->setModel(proxyModel);
    ui->tableView->setSortingEnabled(true);

    ParameterCategoryDelegate *delegate = new ParameterCategoryDelegate();
    ui->tableView->setItemDelegateForColumn(0, delegate);

    connect(ui->leFilter, &QLineEdit::textChanged,
            this, &ParameterDialog::filterChanged);

    QSettings rc;
    this->restoreGeometry(rc.value("parameterdialog/geometry").toByteArray());
    ui->tableView->horizontalHeader()->restoreState(rc.value("parameterdialog/columns").toByteArray());
}

ParameterDialog::~ParameterDialog()
{
    QSettings rc;
    rc.beginGroup("parameterdialog");
    rc.setValue("geometry", this->saveGeometry());
    rc.setValue("columns", ui->tableView->horizontalHeader()->saveState());
    rc.endGroup();

    delete ui;
    delete proxyModel;
}

void ParameterDialog::setInfoLabel(const QString &query, const QString &db)
{
    ui->labelInfo->setText(tr("Query '%1' connected with '%2'").arg(query,db));
}

void ParameterDialog::on_btnAdd_clicked()
{   
    auto const indices = this->ui->tableView->selectionModel()->selectedIndexes();

    // without selection add at the end of the list
    model->setDefaultCategory(ParameterEntry::getCategoryName(ParameterEntry::Query));
    int position = model->rowCount();
    if (indices.size() > 0) {
        // otherwise add before the lowest select row
        position = indices.front().row();
        auto src_index = proxyModel->mapToSource(indices.front());
        model->setDefaultCategory(model->data(src_index, Qt::DisplayRole).toString());
    }
    this->proxyModel->insertRow(position, QModelIndex());

}


void ParameterDialog::on_btnRemove_clicked()
{
    auto const indices = this->ui->tableView->selectionModel()->selectedIndexes();

    int from = INT_MAX;
    int to = INT_MIN;

    for (auto &i : indices) {
        if (i.row() < from) from = i.row();
        if (i.row() > to) to = i.row();
    }
    if (from <= to)
    {
        this->proxyModel->removeRows(from, to - from + 1, QModelIndex());
    }
}

void ParameterDialog::filterChanged()
{
    QString pattern = QRegularExpression::escape(ui->leFilter->text());

    QRegularExpression::PatternOptions options = QRegularExpression::NoPatternOption;
    options |= QRegularExpression::CaseInsensitiveOption;
    QRegularExpression regularExpression(pattern, options);

    if (regularExpression.isValid()) {
        ui->leFilter->setToolTip(QString());
        proxyModel->setFilterRegularExpression(regularExpression);
        setTextColor(ui->leFilter, textColor(style()->standardPalette()));
    } else {
        ui->leFilter->setToolTip(regularExpression.errorString());
        proxyModel->setFilterRegularExpression(QRegularExpression());
        setTextColor(ui->leFilter, Qt::red);
    }
}

void ParameterDialog::on_checkBox_stateChanged(int arg1)
{
    proxyModel->setFilterKeyColumn(arg1 == Qt::Checked ? 2 : 1);
}


void ParameterDialog::on_pbAddValues_clicked()
{
    auto v = ui->leDbValues->text();
    this->model->importCategory(ParameterEntry::Database, v, true);
}
