#include "parametercategorydelegate.h"
#include "parameterentry.h"

#include <QComboBox>

ParameterCategoryDelegate::ParameterCategoryDelegate(QObject *parent)
    : QStyledItemDelegate{parent}
{

}

QWidget *ParameterCategoryDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option)
    Q_UNUSED(index)

    QComboBox *editor = new QComboBox(parent);
    editor->setEditable(false);
    editor->setFrame(false);
    auto l = ParameterEntry::getCategories();
    foreach (auto e, l) {
        editor->addItem(e);
    }
    return editor;
}

void ParameterCategoryDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    auto value = index.model()->data(index, Qt::EditRole).toString();
    static_cast<QComboBox*>(editor)->setCurrentText(value);
}

void ParameterCategoryDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    auto value = static_cast<QComboBox*>(editor)->currentText();
    model->setData(index, value, Qt::EditRole);
}

void ParameterCategoryDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                                     const QModelIndex &index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}

