#include "query_executor.h"
#include "QtCore/qforeach.h"
#include "common.h"
#include "resultsetlist.h"
#include "dbencoding.h"
#include "resultsetsingle.h"
#include "resultsetsql.h"

#include <QRegularExpression>
#include <QInputDialog>
#include <QUrl>
#include <QHashIterator>
#include <QtQml/QJSValue>
#include <QJSValueIterator>
#include <QTime>
#include <QtQml/QJSValue>

QueryExecutor::QueryExecutor(QObject *parentObj)
    : QObject(parentObj),
    logger(LogMessage::instance()),
      mQSE(nullptr),
      userParameter(),
      replacements(),
      treeReplacements(),
      cumulationMap(),
      queriesMap(),
      templatesMap(),
      sqlFileName(""),
      templateFileName(""),
      databaseType(""),
      scriptEngine(),
      fileOut(),
      streamOut(),
      uniqueId(0),
      firstQueryResult(false),
      currentTemplateBlockName(""),
      templateCall("\\#\\{([^\\}]*)\\}"),
      singleVariable("\\$([?]*[a-zA-Z_]+[a-zA-Z0-9_]*)"),
      modificationVariable("\\$\\{([^\\}]*)\\}"),
      paramAssign(":="),
      sqlQueryCount(0),
      lastTabulator(0),
      currentDbConnection(nullptr),
      activeTemplates(),
      indentation(0),
      stopPressed(false)
{
}

QueryExecutor::~QueryExecutor()
{
    try
    {
        clearStructures();
        mQSE = nullptr;
    }
    catch (...)
    {
        // catch all exception
    }
}

void QueryExecutor::clearStructures()
{
    databaseType = "";
    userParameter.clear();
    replacements.clear();
    queriesMap.clear();
    templatesMap.clear();
    activeTemplates.clear();
    indentation = 0;
}

//!
//! \brief QueryExecutor::readFileToList
//! \param category for ouput purpose only
//! \param basePath used to inlcude other files
//! \param filename the filename to read
//! \param [out] lines returns the file as QStringList
//! \param [out] file_list returns the list of included files and used for recursion detection
//! \return
//!
bool QueryExecutor::readFileToList(const QString &category, const QString &basePath,
                                   const QString &filename, QStringList &lines,
                                   QStringList &file_list)
{
    QFile inputFile(filename);
    if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        logger.errorMsg(tr("can't open %1 file '%2'").arg(category, filename));
        return false;
    }
    else
    {
        QTextStream streamIn(&inputFile);
        int lineNr = 0;
        while ( !streamIn.atEnd())
        {
            lineNr++;
            QString line = streamIn.readLine();
            lines.append(line);

            if (line.startsWith("::<"))
            {
                QString fnp = line.mid(3).trimmed();
                QString fnr = replaceLine(fnp, lineNr);
                QString fn = Common::findFile(basePath, fnr);

                if(fn.isEmpty())
                {
                    logger.errorMsg(tr("can't find include file '%1'").arg(fnr));
                }
                else if (file_list.contains(fn))
                {
                    logger.errorMsg(tr("cyclic include found at %1 < %2")
                                    .arg(file_list.join(" < "), fn));
                }
                else
                {
                    file_list.push_front(fn);
                    readFileToList(QString("%1@line %2 include").arg(category).arg(lineNr),
                                   basePath, fn, lines, file_list);
                    file_list.removeFirst();
                    line.append("::>");
                }
            }
        }
    }

    return true;
}

QByteArray QueryExecutor::getParameter(const QString &name, bool &bOk)
{
    if (replacements.contains(name.toUpper()))
    {
        bOk = true;
        return replacements[name.toUpper()];
    }

    if (name.at(0) == '?')
    {
        QString n = name.mid(1);
        if (userParameter.contains(n))
        {
            bOk = true;
            return userParameter[n].toUtf8();
        }
    }

    bOk = false;
    return "";
}

void QueryExecutor::setArgumentsToResultSet(QStringList &argument_list, ResultSet *resultSet)
{
    if (argument_list.size() > 0)
    {
        for (quint32 i=0; i < argument_list.size(); ++i)
        {
            QString argname = tr("ARG_%1").arg(i+1);  // let us start with 1
            resultSet->saveParameter(replacements, argname);
            replacements[argname] = argument_list.at(i).toUtf8();
        }
    }
}

//! It is possible to ask the user for a input value, this
//! value is asked once and reused in all other cases.
QString QueryExecutor::replaceLineUserInput(QStringList &varList)
{

    if (varList.size() > 0)
    {
        QString tmpName = varList[0];
        QString tmpDescr = "";

        if (varList.size() > 1)
        {
            // Attention, if this is explizit set to empty, the dialog is hidden and the variable
            // is set to empty (i.e. ${?foo,} is like an optional variable
            tmpDescr = varList[1];
            if (!tmpDescr.isEmpty())
            {
                // is set add the variable name for better understanding
                tmpDescr = QString("%1 '%2'").arg(varList[1], tmpName);
            }
        }
        else
        {
            // if no description given
            tmpDescr = QString("Please input value for the user variable '%1'.").arg(varList[0]);
        }

        tmpName = tmpName.mid(1);
        if (!userParameter.contains(tmpName))
        {
            // tmpDescr is set, if we need to ask the user otherwise it is empty
            if (!tmpDescr.isEmpty())
            {
                // ask the user for the value
                bool ok;
                QString text = QInputDialog::getText(NULL, tmpName, tmpDescr,
                                                     QLineEdit::Normal, "", &ok);
                if (!ok)
                {
                    this->stopPressed = true;
                    userParameter[tmpName] = "";
                }
                else
                {
                    userParameter[tmpName] = text;
                }
            }
            else
            {
                userParameter[tmpName] = "";
                logger.warnMsg(QString("Optional User Parameter '%1' is empty.").arg(tmpName));
            }
        }

        if (varList.size() > 1)
        {
            // the user variable contains modifications like ${?Name,Username,CAPITALIZE}
            if (checkForModifier(varList))
            {
                // if the user forgot the double comma for the modifier
                logger.warnMsg(tr("it looks you forgot the empty the description part (two commas) %1,,%2 -- execute anyway")
                                   .arg(varList.at(0), varList.at(1)));
            }
            else
            {
                varList.removeAt(1);
            }

            return replaceLineVariable(userParameter[tmpName].toUtf8(), varList);
        }
        else
        {
            return userParameter[tmpName];
        }
    }

    return "";
}


bool QueryExecutor::checkForModifier(const QStringList &varList)
{
    static const QStringList modefier_list = {
        "UPPERCASE", "UPPER", "DECODE", "XML", "LOWERCASE", "LOWER", "CAPITALIZE",
        "TRIM", "RTF", "IFEMPTY", "HEX", "BASE64", "BOOL", "BOOLEAN", "SIMPLE",
        "TREEMODE", "FMT", "CUMULATE", "DATE", "ISODATE", "ISODATEMS", "TIMESTAMP",
        "TIMESTAMPMS"
    };

    if (varList.size() > 1)
    {
        QString vCmd = varList.at(1).trimmed().toUpper();
        foreach(QString m, modefier_list)
        {
            if (m == vCmd) return true;
        }
        if (vCmd.contains("%1"))
        {
            return true;
        }
    }

    return false;
}

//! Replace the existing text (vStr) with the modified version by
//! executing the varList expression which contains the array
//! [varname,modifier,parameter1,...]
QString QueryExecutor::replaceLineVariable(const QByteArray &vStr,
                                        const QStringList &varList)
{
    quint32 vlCnt = varList.size();

    if (vlCnt > 0)
    {
        QString tmpName = varList.at(0);
        if (vlCnt > 1)
        {
            QString vCmd = varList.at(1).trimmed().toUpper();

            if ("UPPERCASE" == vCmd || "UPPER" == vCmd)
            {
               return QString(vStr).toUpper();
            }
            else if ("DECODE" == vCmd)
            {
                QString encoding = vlCnt > 2 ? varList.at(2) : "";
                encoding = encoding.trimmed().toUpper();
                return QString(DbEncoding::decode(vStr, encoding));
            }
            else if ("XML" == vCmd)
            {
                auto tmpStr = QByteArray(vStr);
                Common::quoteXmlString(tmpStr);
                return QString(tmpStr);
            }
            else if ("LOWERCASE" == vCmd || "LOWER" == vCmd)
            {
                return QString(vStr).toLower();
            }
            else if ("CAPITALIZE" == vCmd)
            {
                return QString("%1%2").arg(vStr.mid(0,1).toUpper(),vStr.mid(1).toLower());
            }
            else if ("TRIM" == vCmd)
            {
                return vStr.trimmed();
            }
            else if ("RTF" == vCmd)
            {
                if (vStr.startsWith("{\\rtf"))
                {
                    QString resultType = varList.size() > 2 ? varList.at(2) : "html";
                    return Common::convertRtf(vStr, resultType, true, mQSE->getOutputXml());
                }
                else
                {
                    logger.debugMsg(tr("No RTF String found -- use given string"));
                    return vStr;
                }
            }
            else if ("IFEMPTY" == vCmd)
            {
                if ( 0 == vStr.size())
                {
                    // suche den ersten Wert der nicht leer ist
                    for (qint32 i = 2; i < varList.size(); ++i)
                    {
                        bool bOk;
                        QString  tmpName2 = getParameter(varList.at(i), bOk);
                        // parameter found and not empty
                        if ( bOk && !tmpName2.isEmpty())
                        {
                            return tmpName2;
                        }

                        if (!bOk)
                        {
                            // handle unknwon user variables
                            if (varList.at(i).startsWith("?"))
                            {
                                auto tmpL = QStringList({
                                                         varList.at(i),
                                                         tr("Please input value for the user variable")
                                });
                                return replaceLineUserInput(tmpL);
                            }
                            else
                            {
                                // no parameter found suggest constant string
                                return varList.at(i);
                            }
                       }
                    }
                }
                else
                {
                    return vStr;
                }
            }
            else if ("HEX" == vCmd)
            {
                // hexadecimal output
                bool bOk = false;
                int i = vStr.toInt(&bOk);
                if (bOk)
                {
                    return QString("%1").arg(i,0,16);
                }
                else
                {
                    return vStr.toHex();
                }
            }
            else if ("BASE64" == vCmd)
            {
                QString blockWidth = vlCnt > 2 ? varList.at(2) : "";
                QString encoding = vlCnt > 3 ? varList.at(3) : "";
                QByteArray tmpStr = DbEncoding::decode(vStr, encoding.trimmed().toUpper());
                QByteArray b64 = tmpStr.toBase64();
                QString result;

                quint32 width = 0;
                if (!blockWidth.isEmpty())
                {
                    bool bOk = false;
                    width = blockWidth.toInt(&bOk);
                    if (!bOk) {
                        width = 0;
                    }
                }

                if (width > 0)
                {
                    quint32 pos = 0;
                    quint32 len = b64.length();
                    while (pos+width < len)
                    {
                        result += b64.mid(pos, width);
                        pos += width;
                        if (pos < len)
                        {
                            result += "\n";
                        }
                    }
                    if (pos < len)
                    {
                        result += b64.mid(pos, len-pos);
                    }
                    if (width == len)
                    {
                        result += "\n";
                    }
                }
                else
                {
                    result += b64;
                }

                return result;

            }
            else if ("BOOL" == vCmd || "BOOLEAN" == vCmd)
            {
                // using this parameters all optional
                // 2: true value output
                // 3: false value output
                // 4: true represantation (default in (1,ja,true,yes)
                QString bStr = vStr.trimmed();
                QString trueValue = vlCnt > 2 ? varList.at(2) : "true";
                QString falseValue = vlCnt > 3 ? varList.at(3) : "false";
                bool isTrue = false;
                if (vlCnt > 4)
                {
                    isTrue = bStr == varList.at(4);
                }
                else
                {
                    bStr = bStr.toUpper(); // toupper works for ascii values
                    isTrue = bStr == "1" || bStr == "JA" || bStr == "YES" || bStr == "TRUE";
                }
                return isTrue ? trueValue : falseValue;
            }
            else if ("SIMPLE" == vCmd)
            {
                return vStr.simplified();
            }
            else if ("TREEMODE" == vCmd)
            {
                // create an ouput for TREEMODE elements if
                // * the first time it is used (not in treeReplacements)
                // * the value has changed to the stored value (differs from treeReplacements
                if (!treeReplacements.contains(tmpName) || treeReplacements[tmpName] != vStr)
                {
                    treeReplacements[tmpName] = vStr;
                    qsizetype lidx = treeLevelList.indexOf(tmpName);

                    if (lidx == -1)
                    {
                        // append a new level to the list
                        treeLevelList.append(tmpName);
                    }
                    else if ((lidx + 1) < treeLevelList.size())
                    {
                        // remove all deeper levels, to start new if a higher level changes
                        updateTreeStorage(treeLevelList.at(lidx + 1));
                    }

                    return QString(vStr);
                }
                else if (varList.size() > 2)
                {
                    // sonst optionalen Standardwert einsetzen
                    return varList.at(2);
                }
                else
                {
                    // oder leer lassen
                    return "";
                }
            }
            else if ("FMT" == vCmd)
            {
                // Ausgabe formatieren und alle Folgezeilen mit dem
                // angegebenen StartOfLine versehen
                qint32 tw = 78;
                bool bOk = varList.size() > 2 ? tw = varList.at(2).toInt(&bOk) : true;
                if (!bOk) tw = 78;
                QString sol(varList.size() > 3 ? varList.at(3) : "");

                QStringList l = Common::splitString(vStr, tw, sol);
                int ls = l.size();
                QString result;
                for (int i = 0; i < ls; ++i)
                {
                    result += l.at(i);
                    if (i < (ls-1)) result += "\n";
                }
                return result;
            }
            else if ("CUMULATE" == vCmd)
            {
                bool bOk = false;
                quint32 number = vStr.toUInt(&bOk);
                if (bOk)
                {
                    quint32 c = number;

                    if (cumulationMap.contains(tmpName))
                    {
                        c += cumulationMap[tmpName];
                    }
                    cumulationMap[tmpName] = c;
                    return QString("%1").arg(c);
                }
            }
            else if ("DATE" == vCmd || "ISODATE" == vCmd
                     || "ISODATEMS" == vCmd || "TIMESTAMP" == vCmd || "TIMESTAMPMS" == vCmd)
            {

                if (vStr.isEmpty())
                {
                    // do nothing
                    return "";
                }

                QString dateString = QString(vStr); // convert for usage to internal UTF
                QDateTime dt;
                QString pattern = "missing";
                quint32 outIdx = 2;

                if (vCmd == "DATE" && varList.size() >= 4)
                {
                    dt = QDateTime::fromString(dateString, varList.at(2));
                    pattern = varList.at(2);
                    outIdx = 3;
                }
                else if (varList.size() >= 3)
                {
                    if ("ISODATE" == vCmd)
                    {
                        dt = QDateTime::fromString(dateString, Qt::ISODate);
                        pattern = "ISO DATE";
                    }
                    else if ("ISODATEMS" == vCmd)
                    {
                        dt = QDateTime::fromString(dateString, Qt::ISODateWithMs);
                        pattern = "ISO DATE WITH MS";
                    }
                    else if ("TIMESTAMP" == vCmd || "TIMESTAMPMS" == vCmd)
                    {
                        bool bOk;
                        quint64 ts = dateString.toLongLong(&bOk);
                        if (bOk)
                        {
                            if ("TIMESTAMP" == vCmd)
                            {
                                dt = QDateTime::fromSecsSinceEpoch(ts);
                                pattern = "Timestamp";
                            }
                            else
                            {
                                dt = QDateTime::fromMSecsSinceEpoch(ts);
                                pattern = "Timestamp Milliseconds";
                            }
                        }
                        else
                        {
                            logger.errorMsg(tr("can't convert '%1' to a timestamp number").arg(dateString));
                            pattern = "numerical timestamp";
                        }
                    }
                }
                else
                {
                    if (vCmd == "DATE")
                    {
                        logger.warnMsg("DATE modifier wrong parameter should be '<date format given>,<date format output>[,<locale>]'");
                    }
                    else
                    {
                        logger.warnMsg(tr("%1 modifier need an ouput date format").arg(vCmd));
                    }
                    return "";
                }


                if (dt.isValid())
                {
                    QString localeStr = varList.size() > outIdx+1 ? varList.at(outIdx+1) : "";
                    if (localeStr.isEmpty())
                    {
                        return dt.toString(varList.at(outIdx));
                    }
                    else
                    {
                        QLocale locale((QLocale(localeStr)));
                        return locale.toString(dt, varList.at(outIdx));
                    }
                }
                else
                {
                    logger.warnMsg(tr("the string '%1' dosen't match the date format '%2' for '%3'")
                                   .arg(vStr, pattern, vCmd));
                }
            }
            else if (varList.at(1).contains("%1"))
            {
                // es wurde ein zweiter Teil angegeben, dieser wird
                // als Format verwendet, wenn ein Wert existiert
                if (!vStr.isEmpty())
                {
                    return QString(varList.at(1)).arg(vStr);
                }
            }
            else
            {
                logger.errorMsg(tr("Not supported variable conversion '%1'.").arg(varList.at(1)));
            }
        }     
    }

    // no other value return the string itself
    return vStr;

}

QString QueryExecutor::replaceLineGlobal(const QStringList &varList, int currentLineLength, int lineCnt)
{
    Q_UNUSED(lineCnt)

    if (varList.size() > 0)
    {
        bool bOk;

        QString varStr = varList.join(",");
        QString tmpName = varList.at(0).trimmed().toUpper();
        qsizetype cntVars = varList.size();

        if ("__MSG" == tmpName || "__LOG" == tmpName || "__WARN" == tmpName || "__ERROR" == tmpName)
        {
            if (cntVars > 1)
            {
                QString message = replaceString(varList.at(1), "");
                if ("__MSG" == tmpName)
                {
                    logger.warnMsg("__MSG is deprecated please replace with __LOG");
                    logger.infoMsg(message);
                }
                else if ("__LOG" == tmpName)
                {
                    logger.infoMsg(message);
                }
                else if ("__WARN" == tmpName)
                {
                    logger.warnMsg(message);
                }
                else
                {
                    logger.errorMsg(message);
                }
            }
            return "";
        }
        else if ("__INFO" == tmpName)
        {
            if (cntVars == 2)
            {
                QString llname = varList.at(1);
                bool hasll = lookup_list.hasMapEntry(llname);
                if (hasll)
                {
                    qint32 e = lookup_list.mapSize(llname);
                    logger.infoMsg(tr("lookup list '%1' exists with %2 %3.").arg(llname).arg(e).arg(e==1?"entry":"entries"));
                }
                else
                {
                    logger.infoMsg(tr("no lookup list with name '%1' found").arg(llname));
                }
                return "";
            }
        }
        else if ("__INSERT" == tmpName || "__GET" == tmpName)
        {
            bool isInsert = tmpName == "__INSERT";

            if (isInsert && cntVars < 4)
            {
                // check if we have enough parameters to insert a value
                logger.errorMsg(tr("to few parameters expected '__insert,<listname>,<key>,<value>' but given '%1'").arg(varStr));
                return "";
            }

            if (!isInsert && cntVars < 3)
            {
                // check if we have enough parameters to access a value
                logger.errorMsg(tr("to few parameters expected '__get,<listname>,<key>' but given '%1'").arg(varStr));
                return "";
            }

            QString llname = varList.at(1);
            bool hasll = lookup_list.hasMapEntry(llname);
            if (!isInsert && !hasll)
            {
                // if we access the list, this list must be exists
                logger.infoMsg(tr("lookup list with name '%1' doesn't exists").arg(llname));
                return "";
            }

            bool bOk = false;
            QString llKey = getParameter(varList.at(2), bOk);
            if (!bOk)
            {
                // check if the given parameter with the key exists
                logger.errorMsg(tr("missing lookup key parameter '%1'").arg(varList.at(2)));
                return "";
            }

            if (isInsert)
            {
                // add new value to new or existing list
                 QString llValueName = varList.at(3).toUpper();
                if (replacements.contains(llValueName))
                {
                    lookup_list.insert_value(llname, llKey, replacements[llValueName] );

                }
                else
                {
                    logger.errorMsg(tr("missing lookup value parameter '%1'.").arg(varList.at(3)));
                    return "";
                }
            }
            else
            {
                // get access to the value and use given format
                // 0: __get, 1: listname, 2: key, 3: optional output if found, 4: optional output if not found
                QString outFound = varList.size() > 3 ? varList.at(3) : "%1";
                QString outNotFound = varList.size() > 4 ? varList.at(4) : "%1";

                if (lookup_list.hasMapKey(llname, llKey))
                {
                       return outFound.contains("%1") ? outFound.arg(lookup_list.getValue(llname,llKey)) : outFound;
                }
                else
                {
                    return outNotFound.contains("%1") ? outNotFound.arg(llKey) : outNotFound;
                }
            }
        }
        else if ("__LSEP" == tmpName)
        {
            if (!firstQueryResult)
            {
                return varList.size() > 1 ? varList.at(1) : ",";
            }
        }
        else if ("__DATE" == tmpName)
        {
            QString tmpDateFormat("yyyy-MM-dd");
            if (varList.size() > 1)
            {
                tmpDateFormat = varList.at(1);
            }
            return getDate(tmpDateFormat);
        }
        else if (tmpName.startsWith("__COUNTER"))
        {
            uniqueId++;
            if (tmpName == "__COUNTER")
            {
                return QString("%1").arg(uniqueId);
            }
            else if (tmpName == "__COUNTERH")
            {
                QString hexadecimal;
                hexadecimal.setNum(uniqueId,16);
                return hexadecimal;
            }
            else
            {
                logger.errorMsg(tr("wrong '%1' only __COUNTER or __COUNTERH is supported").arg(tmpName));
            }
        }
        else if (tmpName.startsWith("__LINECNT"))
        {
            logger.warnMsg(tr("global variable %1 was removed! Replace with __COUNTER.").arg(tmpName));
        }
        else if ("__INDENT" == tmpName)
        {
            if (varList.size() > 1)
            {
                if (varList.at(1) == "0")
                {
                    indentation = 0;
                }
                else
                {
                    qint32 i = varList.at(1).toInt(&bOk);
                    if (!bOk) i = 0;
                    indentation = indentation + i;
                }
            }
        }
        else if ("__TAB" == tmpName)
        {
            int tab = 0;
            if (varList.size() > 1)
            {
                tab = varList.at(1).toInt(&bOk);
                if (!bOk) tab = 0;
                lastTabulator = tab;
            }
            else
            {
                tab = lastTabulator;
            }

            if (tab != 0 && currentLineLength < tab)
            {
                // app spaces to reach the next tabstop
                return QString(tab-currentLineLength, ' ');
            }
        }
        else if ("__LF" == tmpName)
        {
            writeToStream("\n"); // to trigger the optional indentation
        }
        else if ("__CLEAR" == tmpName)
        {
            if (varList.size() > 1)
            {
                cumulationMap.remove(varList.at(1));
            }
        }
        else if ("__TREE_RESET" == tmpName)
        {
            updateTreeStorage(varList.size() > 1 ? varList.at(1) : nullptr);
        }
        else
        {
            logger.errorMsg(tr("unsupported global command '%1' found").arg(tmpName));
        }
    }

    return "";
}

void QueryExecutor::showDbError(QString vErrStr)
{
    logger.errorMsg(vErrStr);

    if ( currentDbConnection != nullptr )
    {
        currentDbConnection->lastDbError(true);
    }
}

//! Erzeugen des Namens der Ausgabedatei. Dabei wird die alte
//! Version mit explizitem Zeitstempel weiterhin unterstützt.
//! Der erzeugte Name wird in lastOutputFile gespeichert.
void QueryExecutor::createOutputFileName(const QString &basePath)
{
    if (nullptr != mQSE)
    {
        QString mOutFileName = mQSE->getOutputFile();
        mOutFileName = replaceLine(mOutFileName, 0);

        if (mOutFileName.isEmpty())
        {
            mOutFileName = "output.txt";
            mQSE->setOutputFile(mOutFileName);
        }

        if (!(mOutFileName.startsWith("/") || mOutFileName.at(1)==':') )
        {
            // find a relative path, prepend with basePath
            mOutFileName = basePath + "/" + mOutFileName;
        }

        if (mQSE->getWithTimestamp())
        {
            QFileInfo outInfo(mOutFileName);

            QString bn = outInfo.baseName();
            QString p  = outInfo.dir().absolutePath();
            QString sf = outInfo.suffix();

            QDateTime mNow = QDateTime::currentDateTime();
            mOutFileName = p + "/" + mNow.toString("yyyy-MM-dd")
                    +"-"+bn+"-"+mNow.toString("hhmm")+"."+sf;
        }

        mQSE->setLastOutputFile(mOutFileName);
    }
}

//! Das QuerySet kann ausgewählt werden und alle Dateien 
//! müssen sich im selben Verzeichnis befinden.
//! Die Namen wurden schon während der Erfassung bereinigt.
void QueryExecutor::createInputFileNames(const QString &basePath)
{
    if (nullptr != mQSE)
    {
        sqlFileName = mQSE->getSqlFile().isEmpty() ? QString("") : basePath + "/" + mQSE->getSqlFile();
        templateFileName = basePath + "/" + mQSE->getTemplateFile();
    }
    else
    {
        sqlFileName = "";
        templateFileName = "";
    }
}


//! A method to add the sql query or a template to the internal map.
void QueryExecutor::addMapEntry(const QString &category, const QString &name, const QStringList &lines)
{
    QString localName = name;

    if (localName.toLower().trimmed() == "javascript")
    {
        // normalize the special block
        if (localName != Common::jsName())
        {
            logger.warnMsg(tr("please use '%1' as identifier for the Javascript block in %2 file ('%3' was used)")
                               .arg(Common::jsName(), category, localName));
        }
        localName = Common::jsName();
    }

    bool isJs = localName == Common::jsName();

    if (category == "SQL")
    {
        QString sqlLine = lines.join(isJs ? "\n" : " ");
        if (localName.contains("."))
        {
            // check if the system knows the database driver
            auto parts = localName.split(".");
            if (!Common::availableDrivers().contains(parts[1]))
            {
                logger.warnMsg(tr("the database specific SQL for %1 has no driver avilable in the system")
                               .arg(localName));
            }
        }

        if (queriesMap.contains(localName))
        {
            logger.warnMsg(tr("the SQL '%1' was added before and is ignored (definition existst multiple times)").arg(localName));
        }
        else
        {
            if (logger.isDebug())
            {
                logger.debugMsg(QString("adding SQL-Query '%1'").arg(localName));
            }
            queriesMap[localName] = sqlLine;
        }
    }
    else if (category == "Template")
    {
        if (templatesMap.contains(localName))
        {
            logger.warnMsg(tr("the template '%1' was added before and is ignored"
                              " (definition existst multiple times)").arg(localName));
        }
        else
        {
            if (logger.isDebug())
            {
                logger.debugMsg(tr("adding Template-Block '%1'").arg(localName));
            }

            if (lines.size() == 0)
            {
                // add empty line without newline for templates without lines
                templatesMap[localName].append("\\");
            }
            else
            {
                templatesMap[localName].append(lines);
            }
        }
    }
    else
    {
        logger.errorMsg(tr("application error unknown map type category '%1'").arg(category));
    }
}

//! Add the name of the template the counter is used to detect recursive calls of templates.
quint32 QueryExecutor::addActiveTemplate(QString templateName)
{
    activeTemplates[templateName] += 1;
    return activeTemplates[templateName];
}

void QueryExecutor::removeActiveTemplate(QString templateName)
{
    if (activeTemplates.contains(templateName))
    {
        activeTemplates[templateName] -= 1;
    }
}

QString QueryExecutor::showReplacements(QHash<QString, QByteArray> &variables,
                                     const QString &format)
{
    bool first = true;
    QString result;
    QStringList l;

    foreach (const QString &str, variables.keys())
    {
        QString value = variables.value(str);
        if ("json" == format.toLower())
        {
            Common::quoteJsonString(value);
        }
        else if ("encode64" == format.toLower())
        {
            value = variables.value(str).toBase64();
        }
        else
        {
            logger.errorMsg(QString("Format '%1' for {*} not supported (must be json (default) or encode64).").arg(format));
            return "";
        }

        l << QString("\"%2\": \"%3\"").arg(str, value);
    }

    l.sort();
    result += "{\n";
    foreach (const QString str, l)
    {
        if (!first)
        {
            result += ",\n";
        }
        else
        {
            first = false;
        }
        result += QString("%1%2").arg(getIndentationString(4), str.trimmed());
    }
    result += QString("\n%1}").arg(getIndentationString(0));

    return result;
}

QString QueryExecutor::getIndentationString(quint32 addIndent)
{
    return QString(indentation + addIndent, ' ');
}

void QueryExecutor::writeToStream(const QString &str)
{
    static bool needIndent = false;

    if (indentation > 0 && needIndent)
    {
        streamOut << getIndentationString(0);
    }

    streamOut << str;
    needIndent = str.endsWith("\n");
}

bool QueryExecutor::parseInputFile(const QString &category, const QString &basePath,
                                   const QString &fileName, bool preserve_empty_lines)
{
    bool bRet;
    QStringList fileList;
    QStringList blockNames;
    QList<QPair<QString, quint32> > lineCounter;

    QStringList stringList; // need for recursive call of readFileToList
    lineCounter.append(qMakePair(fileName, 0));
    if ((bRet = readFileToList(category, basePath, fileName, stringList, fileList)))
    {
        QStringList block_lines;
        QString block_name = "";
        quint32 currentLineCounter = 0;
        quint32 empty_lines = 0;
        foreach (QString line, stringList)
        {
            lineCounter[currentLineCounter].second++;
            if (line.startsWith("::<"))
            {
                // start of includeded lines
                currentLineCounter++;
                if (lineCounter.length() == currentLineCounter )
                {
                    lineCounter.append(qMakePair(line.mid(3).trimmed(), 0));
                }
                continue;
            }

            if (line.startsWith("::>"))
            {
                // end of included lines
                currentLineCounter = currentLineCounter > 0 ? currentLineCounter - 1 : 0;
                continue;
            }

            if (line.startsWith("::#"))
            {
                continue;
            }

            if (preserve_empty_lines && line.length() == 0)
            {
                empty_lines++;
                continue;
            }

            if (line.startsWith("::"))
            {
                if (!block_name.isEmpty())
                {
                    // add the last block and ignore tailing empty lines
                    addMapEntry(category, block_name, block_lines);
                }

                // start new block
                empty_lines = 0;
                block_lines.clear();
                block_name = line.mid(2).trimmed();

                if (blockNames.contains(block_name))
                {
                    logger.warnMsg(tr("Overwrite %1 '%2' at line %3 in %4")
                                   .arg(category, block_name)
                                   .arg(lineCounter[currentLineCounter].second)
                                   .arg(lineCounter[currentLineCounter].first));
                }
            }
            else
            {
                if ( "" == block_name)
                {
                    logger.errorMsg(tr("Missing block for %1 at line %2 in %3")
                                    .arg(category)
                                    .arg(lineCounter[currentLineCounter].second)
                                    .arg(lineCounter[currentLineCounter].first));
                }
                else
                {
                    if (preserve_empty_lines && empty_lines > 0)
                    {
                        for (quint32 i = 0; i < empty_lines; ++i)
                        {
                            block_lines.append("");
                        }
                        // reset empty lines
                        empty_lines = 0;
                    }
                    block_lines.append(line);
                }
            }
        }

        // add the last block
        if (!block_name.isEmpty())
        {
            addMapEntry(category, block_name, block_lines);
        }
    }

    return bRet;
}

//! This is the start method to fill the internal structures
//! to create the output.
//! This is also the place where the output stream is opened.
//! TODO: refactoring required
bool QueryExecutor::executeFileInputOutput(const QString &basePath)
{
    bool bRet = true;

    createInputFileNames(basePath);

    // open and read the SQL-statements if existing
    if (!sqlFileName.isEmpty())
    {
        bRet = parseInputFile("SQL", basePath, sqlFileName, false);
    }

    if (bRet)
    {
        bRet = parseInputFile("Template", basePath, templateFileName, true);
    }

    if (bRet && (templatesMap.contains(Common::jsName()) || queriesMap.contains(Common::jsName())) )
    {
        QString jsCode = "";
        if (templatesMap.contains(Common::jsName()))
        {
            jsCode = templatesMap[Common::jsName()].join('\n');
        }
        if (queriesMap.contains(Common::jsName()))
        {
            jsCode = jsCode + "\n// -- javascript from queries--\n\n" + queriesMap[Common::jsName()];
        }

        QJSValue result = scriptEngine.evaluate(jsCode);
        if (result.isError())
        {
            QStringList codeList = jsCode.split("\n");
            qsizetype qline = codeList.length() + 1;
            qsizetype lineCounter = 0 ;
            foreach (QString s, codeList)
            {
                lineCounter++;
                if (s.contains("-- javascript from queries--"))
                {
                    qline = lineCounter;
                }
            }
            qsizetype errLine = result.property("lineNumber").toInt();

            logger.errorMsg(tr("javascript error '%1' at line %2 in %3 file")
                            .arg(result.toString())
                            .arg(errLine)
                            .arg(qline < errLine ? "SQL" : "Template"));

            for (qsizetype l=errLine-1; l<errLine+2; ++l)
            {
                if (l > 0 && l <= codeList.length())
                {
                    logger.errorMsg(tr("%1: %2").arg(l, 3).arg(codeList.at(l-1)));
                }
            }

            return false;
        }
        else if (logger.isDebug())
        {
            QJSValueIterator it(scriptEngine.globalObject());
            while (it.hasNext()) {
                it.next();
                logger.debugMsg(QString("ADD JS property %1").arg(it.name()));
            }
        }
    }

    if (nullptr != mQSE)
    {
        // open the output file, create missing path
        createOutputFileName(basePath);

        fileOut.setFileName(mQSE->getLastOutputFile());
        QFileInfo fileOutInfo(fileOut);
        if (!fileOutInfo.absoluteDir().exists())
        {
            logger.infoMsg(tr("create path %1")
                           .arg(fileOutInfo.absoluteDir().absolutePath()));
            fileOutInfo.absoluteDir().mkpath(fileOutInfo.absoluteDir().absolutePath());
        }

        if (!fileOut.open((mQSE->getAppendOutput() ? QIODevice::Append : QIODevice::WriteOnly) | QIODevice::Text))
        {
            logger.errorMsg(tr("Can't open file '%1'").arg(mQSE->getLastOutputFile()));
            bRet = false;
        }
        else
        {
            streamOut.setDevice(&fileOut);
            QString tmpEnc = "UTF-8";
            if (mQSE->getOutputLatin1())
            {
                streamOut.setEncoding(QStringEncoder::Encoding::Latin1);
                tmpEnc = "Latin1/ISO-8859-1";
			}
            else
            {
                streamOut.setEncoding(QStringEncoder::Encoding::Utf8);
            }
            logger.infoMsg(tr("generated file (%1) '%2'").arg(tmpEnc,mQSE->getLastOutputFile()));
		}
	}
	else
	{
        logger.errorMsg("no active query set (please create one)");
        bRet = false;
    }

    return bRet;
}

//! All parameters starting with ? (i.e. ${?username}) can be used
//! as command line parameters. The inputDefines holds a number of the
//! paramerters with the syntax.
//! parameter1:=Wert|parameter2:=Wert2|...
void QueryExecutor::setUserParameter(const QString &parameterList)
{
    QStringList pList = parameterList.split(QLatin1Char('|'), Qt::SkipEmptyParts);
    foreach (QString pValue, pList)
    {
        QStringList pvList = pValue.split(paramAssign);
        if (pvList.size() > 0)
        {
            QString param = pvList.at(0);
            QString value = (pvList.size() > 1) ? pvList.at(1) : "";
            bool overwrite = userParameter.contains(param) && value != userParameter[param];

            if (logger.isDebug() && !overwrite)
            {
                logger.infoMsg(tr("add parameter '%1' with value '%2'")
                               .arg(param, value));
            }

            if (overwrite)
            {
                logger.infoMsg(tr("overwrite parameter %1 := '%2' with new value '%3'")
                               .arg(param, userParameter[param], value));
            }

            userParameter[param] = value;
        }
    }
}

//!
//! \brief QueryExecutor::replaceEvaluation
//!
//! The eval and the optional decode arguments are removed before simple replace the
//! function and compute the result unsing javascript engine
//!
//! \param tmpList the list of parts, the last part is EVAL, before are two optional
//! arguments to decode the used variables in the function DECODE,<encoding-name>
//! \param aLineCnt the line number
//! \return the computed QString
//!
QString QueryExecutor::replaceEvaluation(QStringList &tmpList, int aLineCnt)
{
    if ("EVAL" != tmpList.last().toUpper())
    {
        logger.warnMsg(tr("sqlReport interprets <b>'%1'</b> as <b>'eval'</b>, please remove the surrounding whitspaces")
                           .arg(tmpList.at(tmpList.size()-1)));
    }
    tmpList.removeLast(); // remove EVAL

    QString with_encoding = "";

    if (tmpList.size() > 2)
    {
        // check if we have an optional encoding given
        if ( "DECODE" == tmpList.at(tmpList.size()-2).trimmed().toUpper())
        {
            with_encoding = tmpList.at(tmpList.size()-1).trimmed().toUpper();
            tmpList.removeLast(); // remove encoding string
            tmpList.removeLast(); // remove DECODE
        }
    }

    QString tmpName = tmpList.join(',');
    QString expression = replaceString(tmpName, with_encoding);

    QJSValue objSigValue = scriptEngine.newQObject(&lookup_list);
    scriptEngine.globalObject().setProperty("lookup", objSigValue);

    QJSValue cjsSigValue = scriptEngine.newQObject(&common_javascript);
    scriptEngine.globalObject().setProperty("cjs", cjsSigValue);

    QJSValue expResult = scriptEngine.evaluate(expression);

    if (!expResult.isError())
    {
        return expResult.toString();
    }
    else
    {
        logger.errorMsg(tr("error '%1' at line %2 evaluate script /%3/ at line %4")
                            .arg(expResult.toString())
                            .arg(expResult.property("lineNumber").toInt())
                            .arg(expression)
                            .arg(aLineCnt)
                        );
    }

    return "";
}

//!
//! \brief QueryExecutor::replaceString
//! Used to replace the variable name like $FOO or $?BAR only, no modifier allowed.
//!
//! \param aString the string containing optional variabled
//! \param aLineCnt the line count
//! \param encoding can be used to convert database string to from a given encoding to QString (UTF16)
//! \return
//!
QString QueryExecutor::replaceString(const QString &aString, QString encoding)
{
    QString result = "";
    long long lpos=0;

    QRegularExpressionMatchIterator i = singleVariable.globalMatch(aString);
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();
        QStringList tmpList;
        tmpList.append(match.captured(1));

        // add the static part of the line and move the pointer
        long long pos = match.capturedStart();
        result += aString.mid(lpos,pos-lpos);
        lpos = pos + match.capturedLength();

        if (tmpList.at(0).startsWith("?"))
        {
            result += replaceLineUserInput(tmpList);
        }
        else if (replacements.contains(tmpList.at(0).toUpper()))
        {
            if (!encoding.isEmpty())
            {                
                result += QString(DbEncoding::decode(replacements[tmpList.at(0).toUpper()],
                                                     encoding.trimmed().toUpper()));
            }
            else
            {
                result += replacements[tmpList.at(0).toUpper()];
            }
        }
        else
        {
            logger.errorMsg(tr("unknwon parameter '%1' used at '%2'").arg(tmpList.at(0), aString));
        }
    }
    // add the last part to the result, without match this is the hole string
    result += aString.mid(lpos);

    return result;
}

//! This methods replaces the all variables with the current value.
//! All variables has the syntax ${...} and we have normal, global and
//! user input variables.
//! Another special case is the flag sqlBinding, if this set all variables
//! replaced by :varname
//! If the simpleFormat is true we replace a expression line. In expressions every
//! variable starts with a dollar sign and follow the same naming conventions
//! as the normal replace name method call.
QString QueryExecutor::replaceLine(const QString &aLine, int aLineCnt)
{
    QString result = "";
    QString tmpPipe = "_";
    long long lpos=0;

    QRegularExpressionMatchIterator i = modificationVariable.globalMatch(aLine);
    while (i.hasNext())
    {
        QRegularExpressionMatch match = i.next();

        QString tmpExpression = match.captured(1);

        // add the static part of the line and move the pointer
        long long pos = match.capturedStart();
        result += aLine.mid(lpos,pos-lpos);
        lpos = pos + match.capturedLength();

        // change from single instruction to pipelined instructions where
        // each command is separated by a pip character
        // this ia backward compatible because in this case we have one command only
        QStringList instructionList = tmpExpression.split('|', Qt::KeepEmptyParts);

        if (instructionList.size() > 1 and replacements.contains(tmpPipe))
        {
            logger.errorMsg(tr("using pipelining at line %2 with an existing variable '$%1', please rename this").arg(tmpPipe).arg(aLineCnt));
            return "";
        }

        QString resultStr = "";
        // iterate overall instruction and get the output from the i-th step as input for
        // the (i+1)-th step exception instruction 0 which is handled as before, accessing the
        // variable
        // We use the implicite variable $_ to get the result to the next step, the $_ can also
        // be used in eval statements
        foreach (QString instruction, instructionList)
        {
            // each instruction is an normal command as before
            QStringList tmpList = instruction.split(',', Qt::KeepEmptyParts);
            bool isEval = tmpList.size() > 1 && "EVAL" == tmpList.last().trimmed().toUpper();

            if (!isEval && replacements.contains(tmpPipe))
            {
                // if we have an evaluation string, we need no extra input value
                // add the tmpPipe as variable name for replacements
                tmpList.prepend(tmpPipe);
            }
            QString tmpName = tmpList.at(0);

            // correct lower case variables works in the first step only
            if (!tmpName.startsWith('?'))
            {
                tmpName=tmpName.toUpper();
            }

            // first look for an expression evaluated by the script engine
            // expression,EVAL
            if (isEval)
            {
                resultStr = replaceEvaluation(tmpList, aLineCnt);
            }
            else if (tmpName == "*")
            {
                QString format = tmpList.value(1, "json");
                resultStr = showReplacements(replacements, format);
            }
            // else check if we have a user variable
            else if (tmpName.startsWith("?"))
            {
                resultStr = replaceLineUserInput(tmpList);
            }
            // else check if the variable exists in the replacement list (columns from SQL)
            else if (replacements.contains(tmpName))
            {
                resultStr = replaceLineVariable(replacements[tmpName], tmpList);
            }
            // check if we have a global substitution
            else if (tmpName.startsWith("__"))
            {
                resultStr = replaceLineGlobal(tmpList, result.length(), aLineCnt);
            }
            else
            {
                if (!mQSE->getEmptyNotFound())
                {
                    resultStr = "['" + tmpName + "' is unknown]";
                }
                else
                {
                    // otherwise call the line variable replacement with empty string
                    resultStr = replaceLineVariable("", tmpList);
                }
                logger.errorMsg(QString("unknown variable name <b>'%1'</b> at line %2")
                                    .arg(tmpName)
                                    .arg(aLineCnt));
            }

            replacements[tmpPipe] = resultStr.toUtf8();
        }

        result += replacements[tmpPipe];
        // and remove the temporary tmpPipe from the replacements
        replacements.remove(tmpPipe);
    }

    // add the last part to the result, without match this is the hole line
    result += aLine.mid(lpos);

    return result;
}

QString QueryExecutor::getDate(const QString &aFormat) const
{
    QDateTime mNow = QDateTime::currentDateTime();
    return QLocale::system().toString( mNow, aFormat);
}

//* executes the given string and returns
//* - 0 found a syntactical error or return value isn't a boolean
//* - 1 the condition is false
//* - 2 the condition is true
quint32 QueryExecutor::executeScriptCondition(const QString &templateName, const QString &condition, int lineCnt)
{
    QString expression = replaceString(condition, "");
    QJSValue expResult = scriptEngine.evaluate(expression).toString();
    if (!expResult.isError())
    {
        QString resultStr = expResult.toString();
        if (resultStr == "false" || resultStr == "true")
        {
            bool b = resultStr == "true";
            if ( !b )
            {
                if (logger.isDebug())
                {
                    logger.debugMsg(QString("Suppress output for template '%1' because '%2' is false.")
                                    .arg(templateName, condition));
                }
                return 1;
            }
        }
        else
        {
            logger.errorMsg(QString("The IF clause must return a boolean (false/true) value but is '%1'")
                            .arg(expResult.toString()));
            return 0;
        }
    }
    else
    {
        logger.errorMsg(tr("error '%1' at line %2 evaluate script /%3/ at template line %4")
                            .arg(expResult.toString())
                            .arg(expResult.property("lineNumber").toInt())
                            .arg(expression)
                            .arg(lineCnt));

        return 0;
    }

    return 2;
}

void QueryExecutor::updateTreeStorage(const QString &levelName)
{
    if (levelName == nullptr)
    {
        // if nullptr cleanup tree storage
        treeReplacements.clear();
        treeLevelList.clear();
    }
    else
    {
        // otherwise check if levelName exists and remove all entries behind this
        // name in the treeLevelList
        qsizetype lidx = treeLevelList.indexOf(levelName);
        if (lidx > -1)
        {
            for (qsizetype i = lidx; i < treeLevelList.size(); ++i )
            {
                treeReplacements.remove(treeLevelList.at(i));
            }
            treeLevelList.erase(treeLevelList.begin() + lidx, treeLevelList.end());
        }
        else
        {
            logger.warnMsg(QString("__TREE_RESET: no tree entry named '%1'").arg(levelName));
        }
    }
}

bool QueryExecutor::executeBatchLine(
    QuerySet &batchQuerySet,
    DbConnectionSet &batchDatabaseSet,
    DbConnection *batchDb,
    QString queryPath,
    const QString &baseInput, QString commandLine,
    quint32 cntCommands, quint32 lineNr, quint32 queryNr)
{
    QStringList qList = commandLine.split("!!", Qt::SkipEmptyParts);
    QString queryInput = "";

    if (qList.size() > 0) {
        bool useBatchDb = false;
        QString queryName = qList.at(0);
        for (qsizetype i = 1; i < qList.size(); ++i) {
            queryInput = queryInput + "|" + qList.at(i);
        }

        if (queryName.startsWith("USEBATCHDB_", Qt::CaseInsensitive)) {
            useBatchDb = true;
            queryName.remove(0, 11);
        }

        if (batchQuerySet.contains(queryName)) {
            QuerySetEntry *tmpQuery = batchQuerySet.getByName(queryName);
            DbConnection *dbcon =
                useBatchDb ? batchDb : batchDatabaseSet.getByName(tmpQuery->getDbName());

            QString dbStr =
                dbcon == nullptr ? QString("no database given") : dbcon->getName();
            logger.setQuerysetName(queryName);
            logger.infoMsg(
                QString(
                    "--[ %1/%2 @ %3 ]---------------------------------------------")
                    .arg(queryNr)
                    .arg(cntCommands)
                    .arg(dbStr));

            logger.setQuerysetName(tmpQuery->getName());
            QString queryParameter =
                tmpQuery->getQueryParameter() + "|" + baseInput + queryInput;
            return this->createOutput(tmpQuery, dbcon, queryPath,
                                            queryParameter);
        } else {
            logger.errorMsg(
                tr("Unknown QuerySet '%1' at line %2").arg(queryName).arg(lineNr));
        }
    }

    // no commands found, not a problem at all
    return true;
}

//! This method expands all lines of the given template list.
//! \return true if the calling method has to add a linefeed
void QueryExecutor::replaceTemplate(const QStringList &aTemplLines, int aLineCnt)
{
    QString tmpName, result;

    auto vLineNum = aTemplLines.size();
    bool subTemplateResult = true;

    for (int i = 0; i < vLineNum && subTemplateResult; ++i)
    {
        QString vStr = aTemplLines.at(i);

        QRegularExpressionMatchIterator expit = templateCall.globalMatch(vStr);
        qint64 lpos = 0;
        while (expit.hasNext())
        {
            QRegularExpressionMatch match = expit.next();
            auto pos = match.capturedStart();

            tmpName = match.captured(1);
            result = vStr.mid(lpos,pos-lpos);
            writeToStream(replaceLine(result, aLineCnt));
            lpos = pos + match.capturedLength();
            // now add the subtemplate
            subTemplateResult = outputTemplate(tmpName);
        }

        QString tmpStr = vStr.mid(lpos);
        result = replaceLine(tmpStr, aLineCnt);

        if (result.endsWith("\\"))
        {
            // remove the last backslash sign
            result = result.mid(0,result.length()-1);
            writeToStream(result);
        }
        else
        {
            writeToStream(result);
            writeToStream("\n");
        }
    }
}

//! This is the heart of the executor. This method controls the SQL
//! query execution, the output generating and is called recursiv
//! to execute inner templates.
//! All SQL results stored as QString in the hash mReplacements. Same
//! column names hides the outer names and will be restored when leaving the
//! method.
bool QueryExecutor::outputTemplate(const QString &templateStr)
{
    QString listSeperator("");      // used with the ,list modifier
    int lineCnt = 0;                //
    bool bRet = true;
    QString lastTemplateName = currentTemplateBlockName;

    if (stopPressed)
    {
        return false;
    }

    if (mQSE->getMaxRecursion() > 0)
    {
        quint32 level = addActiveTemplate(templateStr);
        if (level > mQSE->getMaxRecursion())
        {
            logger.warnMsg(QString("stop executing template '::%1' because max recursion (%2) reached")
                           .arg(templateStr).arg(mQSE->getMaxRecursion()));
            removeActiveTemplate(templateStr);
            return true;
        }
    }

    // split the given aTemplate into parts separated by comma
    QStringList argument_list;
    QStringList ll = templateStr.split(',', Qt::KeepEmptyParts);
    QString aTemplate = ll.at(0).trimmed();
    QString outputModifier = ll.size()>1 ? ll.at(1).trimmed().toUpper() : "";

    // first check if we have arguments
    if ("(" == outputModifier)
    {
        QString arg;
        bool combine_args = false;
        quint32 arg_i = 2;
        while (arg_i < ll.size())
        {
            QString tmp_arg = ll.at(arg_i);
            if (")" != tmp_arg)
            {
                if (combine_args)
                {
                    arg = arg + "," + tmp_arg;
                    if (tmp_arg.endsWith("\""))
                    {
                        combine_args = false;
                        arg = arg.removeLast();
                        argument_list.append(arg);
                    }
                }
                else if (tmp_arg.startsWith("\""))
                {
                    // if argument starts with a double quote we suggest a quoted string
                    // with multiple split elements
                    arg = tmp_arg.removeFirst();
                    if (arg.endsWith("\""))
                    {
                        // if it end with a double quote we have a quoted string without comma
                        arg = arg.removeLast();
                        argument_list.append(arg);
                    }
                    else
                    {
                        combine_args = true;
                    }
                }
                else
                {
                    // add argument
                    argument_list.append(tmp_arg);
                }
            }
            else
            {
                arg_i++;
                break;
            }
            arg_i++;
        }
        if (combine_args)
        {
            logger.errorMsg(tr("no ending double quote in argument list of ##%1##").arg(ll.join(',')));
        }
        // replace variables in arguments
        for (quint32 i = 0; i < argument_list.size(); ++i)
        {
            argument_list[i] = replaceString(argument_list.at(i), "");

        }
        // remove arguments and set new ouput modifier
        ll.remove(1, arg_i - 1);
        outputModifier = ll.size()>1 ? ll.at(1).trimmed().toUpper() : "";
    }

    // use the javascript engine to check if we can output this template
    // variant 1: template,IF,condition
    // variant 2: template,ELSE,alternate template,condition
    bool isElseModifier = "ELSE" == outputModifier;
    if ("IF" == outputModifier or isElseModifier)
    {
        // rebuild the condition in case that the condition contains a comma
        quint32 startCondition = isElseModifier ? 3 : 2;
        QString expCondition = ll.at(startCondition++);
        while (startCondition < ll.size())
        {
            expCondition = QString("%1,%2").arg(expCondition,ll.at(startCondition++));
        }

        quint32 condition = executeScriptCondition(aTemplate, expCondition, lineCnt);
        if (condition == 0)
        {
            return false;
        }
        if (condition == 1)
        {
            if (isElseModifier)
            {
                // false with else statement, overwite the aTemplate
                aTemplate = ll.at(2).trimmed();
            }
            else
            {
                return true;
            }
        }
    }

    // check the calling template string for more informations
    if ("LIST" == outputModifier)
    {
        if (ll.size() > 2)
        {
            ll.removeFirst(); // the template name
            ll.removeFirst(); // the output modifier i.e. LIST
            listSeperator = ll.join(',');
        }
        else
        {
            listSeperator = ",";
        }
    }

    // exists a template with this name
    if (templatesMap.contains(aTemplate))
    {
        currentTemplateBlockName = aTemplate;
        logger.setContext(currentTemplateBlockName);
        const auto templLines = templatesMap[aTemplate];

        // exists a query with the template name ?
        // or up to the time we can handle saved result set we can reuse a
        // SQL query by writing his name and add a different output template
        // using a dot (::ARTICLE.NAMES)

        QString queryTemplate = aTemplate;
        if (aTemplate.contains('.'))
        {
            queryTemplate = aTemplate.split('.').at(0);
        }

        // check if there is a DB specific SQL
        QString queryTemplateDb = QString("%1.%2").arg(queryTemplate, databaseType);
        if (queriesMap.contains(queryTemplateDb))
        {
            queryTemplate = queryTemplateDb;
        }

        ResultSet *resultSet = nullptr;

        if ("SPLIT" == outputModifier)
        {
            QString list_name = ll.size() > 2 ? ll.at(2) : "";
            QString sep = ll.size() > 3 && ll.at(3).size() > 0 ? ll.at(3) : ",";
            QString fname = ll.size() > 4 ? ll.at(4).toUpper() : "LISTVALUE";
            QString decode = ll.size() > 5 ? ll.at(5).toUpper() : "";
            QString src_encoding = ll.size() > 6 ? ll.at(6).trimmed().toUpper() : "";

            if (list_name.isEmpty())
            {
                logger.errorMsg(tr("no parameter '%1' found for SPLIT").arg(list_name));
            }
            else
            {
                // update parameter if decode is first given option
                if ( sep.toUpper() == "DECODE")
                {
                    sep = ",";
                    src_encoding = (fname == "LISTVALUE") ? "" : fname;
                    fname = "LISTVALUE";
                    decode = "DECODE";
                }
                bool foundParam = false;
                QByteArray listStr = getParameter(list_name, foundParam);
                if (!foundParam)
                {
                    logger.errorMsg(tr("parameter '%1' doesn't exists at current replacements").arg(list_name));
                }
                else if (listStr.isEmpty())
                {
                    logger.warnMsg(tr("ignore empty list '%1'").arg(list_name));
                }
                else
                {
                    if ("DECODE" == decode)
                    {
                        listStr = DbEncoding::decode(listStr, src_encoding);
                    }
                    logger.debugMsg(tr("output template '%1' from list '%2' split by '%3'").arg(aTemplate, list_name, sep));
                    resultSet = new ResultSetList(listStr, sep, fname);
                }
            }
        }
        else if (queriesMap.contains(queryTemplate))
        {
            logger.debugMsg(tr("output template '%1' using query '%2'").arg(aTemplate, queryTemplate));
            QString sqlQuery;

            sqlQueryCount++;
            if (argument_list.size() > 0)
            {
                auto args = new ResultSetSingle();
                setArgumentsToResultSet(argument_list, args);
                sqlQuery = replaceLine(queriesMap[queryTemplate], lineCnt);
                args->finishResultSet(replacements);
                delete args;
            }
            else
            {
                sqlQuery = replaceLine(queriesMap[queryTemplate], lineCnt);
            }

            resultSet = new ResultSetSql(currentDbConnection->getName(), sqlQuery);
        }
        else
        {
            // a standalone template with an empty result set
            resultSet = new ResultSetSingle();
        }

        if (resultSet != nullptr)
        {
            setArgumentsToResultSet(argument_list, resultSet);

            if (resultSet->isValid())
            {
                firstQueryResult = true;

                while (resultSet->updateParameter(replacements, mQSE))
                {
                    QCoreApplication::processEvents();
                    if (stopPressed)
                    {
                        break;
                    }

                    // add a optional list seperator
                    if (!firstQueryResult && !listSeperator.isEmpty())
                    {
                        writeToStream(listSeperator);
                    }

                    if (!resultSet->wasEmptyRow())
                    {
                        replaceTemplate(templLines, lineCnt);
                        lineCnt++;
                        if (lineCnt % 1000 == 0)
                        {
                            logger.infoMsg(tr("[%1]").arg(lineCnt));
                        }
                    }
                    firstQueryResult = false;
                }

                if (!stopPressed && resultSet->wasEmptySet())
                {
                    QString emptyTemplate = QString("%1_EMPTY").arg(aTemplate);
                    if (templatesMap.contains(emptyTemplate))
                    {
                        outputTemplate(emptyTemplate);
                    }
                }
            }
            else
            {
                QString errText = resultSet->lastError();
                writeToStream(errText);
                logger.errorMsg(errText);
            }

            resultSet->finishResultSet(replacements);
            delete resultSet;
            resultSet = nullptr;
        }
    }
    else
    {
            logger.errorMsg(tr("template %1 isn't defined").arg(aTemplate));
    }

    currentTemplateBlockName = lastTemplateName;
    logger.setContext(currentTemplateBlockName);

    if (mQSE->getMaxRecursion() > 0)
    {
        removeActiveTemplate(templateStr);
    }

    return bRet;
}


bool QueryExecutor::createOutput(QuerySetEntry *aQSE,
                                 DbConnection *dbc,
                                 const QString &basePath,
                                 const QString &parameterList)
{
    bool b = true;
    stopPressed = false;
    mQSE = aQSE;
    QElapsedTimer t;
    bool needSql = !aQSE->getSqlFile().isEmpty();

    logger.setContext("");
    QString parameterTempList = parameterList;
    clearStructures();                              // remove the internal structure

    if (needSql && dbc == nullptr)
    {
        logger.errorMsg(tr("stop execution -->  missing database for an existing SQL file"));
        return false;
    }

    if (dbc)
    {
        // get database parameter, pratical usage show that often the database is used as
        // target system and define some paths, which are needed to define the output
        // so we add the parameter without check if a SQL template exists
        parameterTempList = QString("%1|%2").arg(dbc->getDatabaseParameter(), parameterTempList);
    }

    if (needSql)
    {
        currentDbConnection = dbc;
        b = dbc->connectDatabase(basePath);         // connect to database, get encoding
        databaseType = dbc->getDbType();
        aQSE->setDatabaseEncoding(dbc->getDbEncoding());
    }

    setUserParameter(parameterTempList);            // set the input parameters

    sqlQueryCount = 0;
    b = b && executeFileInputOutput(basePath);      // read the sql and the template file into the internal structure

    t.start();                                      // measure the time to create the output only
    b = b && outputTemplate("MAIN");				// start process with the MAIN template
    b = b && !stopPressed;

    if (stopPressed)
    {
        logger.setContext("user interaction");
        logger.errorMsg("current query execution stopped");
    }

    streamOut.flush();
    fileOut.close();								// flush and close the output file

    // close the database connection
    if (needSql)
    {
        dbc->closeDatabase();				        // close the database connection
        currentDbConnection = nullptr;
    }

    logger.infoMsg(tr("summary time: %1; executing %2 %3")
                       .arg(Common::formatMilliSeconds(t.elapsed()))
                       .arg(sqlQueryCount)
                       .arg(sqlQueryCount == 1 ? tr("query") : tr("queries"))
                   );

    if (!b)
    {
        logger.errorMsg(tr("found some errors during execution"));
    }

    return b;
}

bool QueryExecutor::createBatchOuput(
    QuerySet &batchQuerySet,
    DbConnectionSet &batchDatabaseSet,
    QuerySetEntry *aQSE,
    const QString &localParamter)
{
    if (aQSE == nullptr)
    {
        logger.errorMsg("given query is nullprt");
        return false;
    }

    QElapsedTimer batchTime;
    QString queryPath = QFileInfo(batchQuerySet.getQuerySetFileName()).absolutePath();
    QString parameterList = aQSE->getQueryParameter() + "|" + localParamter;

    batchTime.start();
    DbConnection *initialDatabase = batchDatabaseSet.getByName(aQSE->getDbName());
    bool vRes = this->createOutput(aQSE, initialDatabase, queryPath, parameterList);

    if (!vRes)
    {
        logger.errorMsg(tr("can't execute batch query '%1' using database '%2'")
                            .arg(aQSE->getName(), aQSE->getDbName()));
        return false;
    }

    // Batch command, uses the created file to execute each line independently
    if (vRes)
    {
        QFile batchFile(aQSE->getLastOutputFile());
        if (!batchFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            logger.errorMsg(tr("Batch Execution File '%1' doesn't exists")
                                .arg(aQSE->getLastOutputFile()));
            return false;
        }

        QString batchCommandsString = QString(batchFile.readAll());
        QStringList batchCommand = batchCommandsString.split('\n');
        quint32 cntCommands = 0;
        foreach (QString str, batchCommand) {
            if (str.startsWith("!!")) {
                cntCommands++;
            }
        }

        quint32 lineNr = 0;
        quint32 queryNr = 0;

        foreach (QString line, batchCommand) {
            line = line.trimmed();
            lineNr++;
            if (line.startsWith("!!")) {
                queryNr++;
                if (!executeBatchLine(batchQuerySet, batchDatabaseSet,
                                      initialDatabase, queryPath,
                                      localParamter, line,
                                      cntCommands, lineNr, queryNr)) {
                    break;
                }
            }
        }

        // remove the before created batch file and reset the logging query name
        batchFile.remove();
        logger.setQuerysetName("");
        logger.infoMsg(tr("batch execution time: %1")
                           .arg(Common::formatMilliSeconds(batchTime.elapsed())));
    }

    return true;
}


QString QueryExecutor::expandString(const QString &str,
                                    const QHash<QString, QByteArray> &parameter,
                                    bool simpleFormat)
{
    QHash<QString, QByteArray> replaces;

    QHash<QString, QByteArray>::const_iterator i = parameter.constBegin();
    while (i != parameter.constEnd()) {
        QString key_name = i.key().toUpper();
        if (replacements.contains(key_name))
        {
            replaces.insert(key_name, replacements[i.key()]);
        }
        replacements[key_name] = i.value();
        ++i;
    }

    QString newStr = simpleFormat ? replaceString(str, "") : replaceLine(str, 0);

    i = parameter.constBegin();
    while (i != parameter.constEnd()) {
        QString key_name = i.key().toUpper();
        if (replaces.contains(key_name))
        {
            replacements[key_name] = replaces[i.key()];
        }
        else
        {
            replacements.remove(key_name);
        }
        ++i;
    }
    return newStr;
}

