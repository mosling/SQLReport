::TREE
select h1.name as pn, h2.name as cn from hobby_tree 
JOIN hobby h1 ON parent = h1.id 
JOIN hobby h2 ON child = h2.id
ORDER BY h1.name, h2.name