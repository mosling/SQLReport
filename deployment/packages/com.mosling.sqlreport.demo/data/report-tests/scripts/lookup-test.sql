::hobby_cache
select id, name
from hobby

::person_cache
select id, name
from person

::hobbies
select id_person, id_hobby
from person_hobby

::abbreviation_data
select *
from abbreviation
