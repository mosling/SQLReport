function toHex (s, sep) {
  arr = s.split(sep)
    arr.forEach(function(part, index) {
    i = parseInt(this[index])
    this[index] = "0x" + i.toString(16)
  }, arr);

  return arr.join(" ")
}